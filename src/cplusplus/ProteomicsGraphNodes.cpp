#include "ProteomicsGraphNodes.hpp"

std::string PeptideNode::to_unique_peptide_sequence(std::string pepSeq) {
  // if included, trim off the cleavage events
  if ( pepSeq[1] == '.' )
    pepSeq = pepSeq.substr(2, pepSeq.size()-2 );
  if ( pepSeq[pepSeq.size()-1] == '.' )
    pepSeq = pepSeq.substr(0, pepSeq.size()-2 );

  // uppercase and replace replace isoleucine with leucine
  std::transform(pepSeq.begin(), pepSeq.end(), pepSeq.begin(), toupper);
  std::replace( pepSeq.begin(), pepSeq.end(), 'I', 'L');

  return pepSeq;
}
