#ifndef _PREFIXSUFFIXTARGETDECOY_H
#define _PREFIXSUFFIXTARGETDECOY_H

#include <string>

#include "SetTargetDecoy.hpp"
#include "StringOperations.hpp"

class PrefixSuffixTargetDecoy : public SetTargetDecoy<std::string>  {
public:
    // cannot simply use decoy prefix and suffix, because other nodes
    // in the graph (e.g. spectra nodes) will be marked as targets
    // (b/c they don't match the decoy prefix / suffix), and will be
    // marked during mark_targets_and_dependents (in
    // SingleReplicateCutoutAnalysis).
  PrefixSuffixTargetDecoy(const std::string & pref_param, const std::string & suff_param, const std::set<std::string> & keys_to_consider):
    decoy_pref(pref_param), decoy_suff(suff_param) {
    
    for (const std::string & key : keys_to_consider)
      if (key_is_decoy(key))
	SetTargetDecoy<std::string>::decoys.insert(key);
      else
	SetTargetDecoy<std::string>::targets.insert(key);
  }

  bool key_is_target(const std::string & key) const {
    return ! is_decoy(key);
  }

  bool key_is_decoy(const std::string & key) const {
    return starts_with(key, decoy_pref) && ends_with(key, decoy_suff);
  }

private:
  std::string decoy_pref, decoy_suff;
};

#endif
