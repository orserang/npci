#ifndef _KEYCOMPARABLE_H
#define _KEYCOMPARABLE_H

template <typename K>
class KeyComparable
{
public:
  typedef K KeyType;
  KeyComparable(const K & key_param)
  {
    key = key_param;
  }
  const K & get_key() const
  {
    return key;
  }
private:
  K key;
};

template <typename K>
bool operator <(const KeyComparable<K> & lhs, const KeyComparable<K> & rhs) { return lhs.get_key() < rhs.get_key(); }

template <typename K>
struct KeyComparablePointerCompare
{
  bool operator()(const KeyComparable<K>* left, const KeyComparable<K>* right) const { return *left < *right; }
};

template <typename K>
struct PointerCompare
{
  bool operator()(const K* left, const K* right) const { return *left < *right; }
};


#endif
