template <typename DOUBLEVAL, std::size_t N>
NumericGridFunction<DOUBLEVAL, N>::NumericGridFunction(const NumericRange<N> & nr_param, const std::vector<DOUBLEVAL> & values_param):
  numeric_range_(nr_param), values_(values_param) {
  //  fixme: ideally should check that numeric_range.size() ==
  //  values.size(); however, this is non-trivial because you
  //  currently need to iterate through NumericRange to get its size
  //  (computing it analytically may result in numerical hiccups at
  //  the boundaries).
}

template <typename DOUBLEVAL, std::size_t N>
const NumericGridFunction<DOUBLEVAL, N> & NumericGridFunction<DOUBLEVAL, N>::operator +=(const NumericGridFunction<DOUBLEVAL, N> & rhs) {
  return vector_operation<plus_eq<DOUBLEVAL> >(rhs);
}

template <typename DOUBLEVAL, std::size_t N>
const NumericGridFunction<DOUBLEVAL, N> & NumericGridFunction<DOUBLEVAL, N>::operator -=(const NumericGridFunction<DOUBLEVAL, N> & rhs) {
  return vector_operation<minus_eq<DOUBLEVAL> >(rhs);
}

template <typename DOUBLEVAL, std::size_t N>
void NumericGridFunction<DOUBLEVAL, N>::print() const {
  typename NumericRange<N>::const_iterator iter = numeric_range_.begin();
  for (DOUBLEVAL x : values_) {
      std::cout << *iter << " : " << x << std::endl;
      ++iter;
    }
  std::cout << std::endl;
}

template <typename DOUBLEVAL, std::size_t N>
DOUBLEVAL NumericGridFunction<DOUBLEVAL, N>::definite_integral() const
{
  DOUBLEVAL result(0.0);
  for ( DOUBLEVAL f: values_ )
    result += f;
  
  DOUBLEVAL delta_prod = numeric_range_.template delta_product<DOUBLEVAL>();
  return result * delta_prod;
}

