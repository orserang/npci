#ifndef _PROTEOMICSGRAPHGROUPER_H
#define _PROTEOMICSGRAPHGROUPER_H

#include "ProteomicsGraph.hpp"
#include "Registry.hpp"
#include "Singleton.hpp"
#include "NPCIExceptions.hpp"

template <typename PROTEOMICSGRAPH>
class ProteomicsGraphGrouper {
public:
  void group_proteins(PROTEOMICSGRAPH & pg) const;
  virtual std::list<std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> > get_protein_groups(const PROTEOMICSGRAPH & pg) const = 0;
  void group_proteins_from_defined_sets(PROTEOMICSGRAPH & pg, const std::list<std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> > & protein_group_ptrs) const;

protected:
  void clear_groups(PROTEOMICSGRAPH & pg) const;
  void check_valid_grouping(const PROTEOMICSGRAPH & pg) const;
};

#include "ProteomicsGraphGrouper.cpp"

#endif
