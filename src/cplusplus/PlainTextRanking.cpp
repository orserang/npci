template <typename K>
PlainTextRanking<K>::PlainTextRanking(const std::string & filename):
  Ranking<K>(filename)
{
  std::ifstream fin(filename);
  std::string line_string;
  std::set<K> observed_accessions;
  // read the file and create the map of score to  set
  while ( getline(fin, line_string) )
    {
      std::istringstream ist(line_string);
      double score;
      ist >> score;
      K accession;
      while (ist >> accession)
	{
	  // strip the following strings to allow old Fido format rankings:
	  if ( accession == "{" or accession == "," or accession == "}" )
	    continue;
	  // if the accession has already been seen, throw an exception

	  // uses the highest posterior for a  mentioned multiple times
	  // std::cout << "ranking: " << accession << std::endl;
	  // if ( observed_accessions.count(accession) > 0 )
	  //   {
	  // 	std::cerr << "Warning: " << accession << " has been read multiple times in ranking; using first (best) position" << std::endl;
	  // 	continue;
	  //   }

	  // throws an error for a  mentioned multiple times
	  if ( observed_accessions.count(accession) > 0 )
	    throw FormatException(std::string(" " + accession + " appears multiple times in ranking"));

	  // add the accession to observed_s
	  observed_accessions.insert(accession);

	  Ranking<K>::score_to_scored_object_set[score].insert(accession);
	}
    }
  Ranking<K>::create_best_sets_first_from_map();
}
