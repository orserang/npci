#ifndef _PROTEINIDGRAPHLOADER_H
#define _PROTEINIDGRAPHLOADER_H

#include "ProteinIDGraph.hpp"
#include "Registry.hpp"
#include "FileAndFormat.hpp"

class ProteinIDGraphLoader
{
protected:
  virtual void load_from_file(ProteinIDGraph & pg, const FileAndFormat & faf) const = 0;
public:
  void load_and_verify(ProteinIDGraph & pg, const FileAndFormat & faf);
};

#endif
