#include "ProteinPilotProteinIDGraphLoader.hpp"

void ProteinPilotProteinIDGraphLoader::load_from_file(ProteinIDGraph & pg, const FileAndFormat & faf) const {
  // note: loads from the file peptides.txt
  TabSeparatedFile tsf(faf.get_file(), std::vector<std::string>{"Accessions", "Sequence", "Conf", "Spectrum"});

  int num_spectra = 0;
  for (const std::vector<std::string> & values_at_row : tsf.get_query_value_rows()) {
    std::string peptide_sequence = values_at_row[1];
    double score = from_string<double>(values_at_row[2]);

    std::string spectrum_id = values_at_row[3];

    PeptideNode pep_node(peptide_sequence);
    //    PSMNode psm_node("spectrum_" + to_string(num_spectra), std::array<double, 1>{{score}});
    PSMNode psm_node("spectrum_" + spectrum_id, std::array<double, 1>{{score}});
    pg.add_node( pep_node );
    pg.add_node( psm_node );
    pg.add_edge_between_keys(pep_node.get_key(), psm_node.get_key());
    ++num_spectra;

    // remove spaces so that ';' is the delimeter between proteins
    std::string all_protein_accessions = values_at_row[0];
    all_protein_accessions.erase(std::remove(all_protein_accessions.begin(), all_protein_accessions.end(), ' '), all_protein_accessions.end());
    
    for (const std::string & protein_accession : split(all_protein_accessions, ';') ) {
      ProteinNode prot_node(protein_accession);
      pg.add_node( prot_node );
      pg.add_edge_between_keys(prot_node.get_key(), pep_node.get_key());
    }
  }
}


