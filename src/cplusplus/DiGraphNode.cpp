template <typename K, typename NodeType>
void DiGraphNode<K, NodeType>::add_edge_to_ptr(DiGraphNode<K, NodeType>::NodePtr succ)
{
  add_successor_ptr(succ);
  // need to cast this as subtype NodeType*, because NodeType should
  // only have pointers to and from NodeType
  succ->add_predecessor_ptr( & dynamic_cast<NodeType &>(*this) );
}

template <typename K, typename NodeType>
void DiGraphNode<K, NodeType>::remove_edge_to_ptr(DiGraphNode<K, NodeType>::NodePtr succ)
{
  remove_successor_ptr(succ);
  // need to cast this as subtype NodeType*, because NodeType should
  // only have pointers to and from NodeType
  succ->remove_predecessor_ptr( & dynamic_cast<NodeType &>(*this) );
}

template <typename K, typename NodeType>
const NodeType & DiGraphNode<K, NodeType>::operator=(const DiGraphNode<K, NodeType> & rhs)
{
  KeyComparable<K>::operator=(rhs);
  return dynamic_cast<const NodeType &>(*this);
}
