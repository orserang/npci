#ifndef _NUMERICRANGE_H
#define _NUMERICRANGE_H

#include <array>
#include <limits>
#include <iostream>
#include <iterator>

#include "Functional.hpp"
#include "StandardExceptions.hpp"

template <typename> class NumericRangeIterator;

template <std::size_t N>
class NumericRange {
public:
  template <typename> friend class NumericRangeIterator;

  typedef NumericRangeIterator<NumericRange<N> > const_iterator;
  
  static const std::size_t Size = N;
  
  NumericRange() {
    _lower.fill(std::numeric_limits<double>::quiet_NaN());
    _upper.fill(std::numeric_limits<double>::quiet_NaN());
    _delta.fill(std::numeric_limits<double>::quiet_NaN());
  }

  NumericRange(const std::array<double, N> & lower, const std::array<double, N> & upper, const std::array<double, N> & delta):
    _lower(lower), _upper(upper), _delta(delta) {}
  
  const_iterator begin() const;
  const_iterator end() const;

  template <typename DOUBLEVAL>
  DOUBLEVAL volume() const {
    DOUBLEVAL my_volume(1.0);
    for (std::size_t k = 0; k<N; ++k)
      my_volume *= DOUBLEVAL(_upper[k] - _lower[k]);
    return my_volume;
  }

  // to get delta1 * delta2 * delta3 ... (useful for computing integrals)
  // note: this could be cached
  template <typename DOUBLEVAL>
  DOUBLEVAL delta_product() const {
  DOUBLEVAL delta_prod(1.0);
  for (double delta_val : _delta )
    delta_prod *= DOUBLEVAL(delta_val);
  return delta_prod;
  }

  bool operator ==(const NumericRange<N> & rhs) const {
    return _lower == rhs._lower && _upper == rhs._upper && _delta == rhs._delta;
  }

  const std::array<double, N> & get_delta() const {
    return _delta;
  }

  const std::array<double, N> & get_lower() const {
    return _lower;
  }

  const std::array<double, N> & get_upper() const {
    return _upper;
  }

  template <template <typename, typename> class Container, typename Allocator>
  static NumericRange<N> create_from_collection_of_points(const Container<std::array<double, N>, Allocator> & container, std::size_t buckets) {
    std::array<double, N> lower = get_lower_from_collection(container);
    std::array<double, N> upper = get_upper_from_collection(container);
    
    return NumericRange<N>::create_from_lower_and_upper_bounds(lower, upper, buckets);
  }

  static NumericRange<N> create_from_lower_and_upper_bounds(const std::array<double, N> & lower, const std::array<double, N> & upper, std::size_t buckets) {
    return NumericRange<N>(lower, upper, get_delta_from_lower_and_upper(lower, upper, buckets));
  }

  void print() const {
    std::cout << "lower: " << _lower << " upper: " << _upper << " delta: " << _delta << std::endl;
  }

private:
  static std::array<double, N> get_delta_from_lower_and_upper(const std::array<double, N> & lower, const std::array<double, N> & upper, std::size_t buckets) {
    std::array<double, N> result;
    for (std::size_t k=0; k<N; ++k) {
      if (upper[k] < lower[k])
	throw SizeMismatchException("Lower limit of NumericRange must not exceed upper limit");
      result[k] = (upper[k] - lower[k]) / buckets;
    }
    return result;
  }

  std::array<double, N> _lower, _upper, _delta;
}; // class NumericRange

template <typename T>
class NumericRangeIterator: public std::iterator< std::array<double, T::Size>, std::forward_iterator_tag > {
public:
  template <std::size_t> friend class NumericRange;

  NumericRangeIterator(): _range_ptr(NULL), _state() {}

  NumericRangeIterator& operator++() {
    this->advance();
    if ( ! in_range() )
      *this = NumericRangeIterator();
    return *this;
  }

  NumericRangeIterator operator++(int) {
    NumericRangeIterator tmp(*this);
    ++*this;
    return tmp;
  }

  std::array<double, T::Size> const& operator*() const {
    return _state;
  }

  std::array<double, T::Size> const* operator->() const {
    return &_state;
  }

  bool operator==(const NumericRangeIterator<T> & rhs) const {
    return _range_ptr == rhs._range_ptr && _state == rhs._state;
  }

  bool operator!=(const NumericRangeIterator<T> & rhs) const {
    return !(*this == rhs);
  }

private:
  NumericRangeIterator(const T& t, const std::array<double, T::Size> & s):
    _range_ptr(&t), _state(s) {}

  bool in_range(std::size_t index_to_advance = T::Size-1) const {
    return _range_ptr != NULL && ( _state[ index_to_advance ] - _range_ptr->_upper[ index_to_advance ] ) < _range_ptr->_delta[ index_to_advance ];
  }
  
  void advance(std::size_t index_to_advance = 0) {
    _state[ index_to_advance ] += _range_ptr->_delta[ index_to_advance ];
    if ( ! in_range(index_to_advance) ) {
      if (index_to_advance < T::Size-1) {
	// restart index_to_advance
	_state[index_to_advance] = _range_ptr->_lower[index_to_advance];
	
	// carry
	++index_to_advance;
	advance(index_to_advance);
      }
    }
  }

  const T* _range_ptr;
  std::array<double, T::Size> _state;
}; // class NumericRangeIterator


template <std::size_t N>
typename NumericRange<N>::const_iterator NumericRange<N>::begin() const {
  return NumericRange<N>::const_iterator(*this, _lower);
}

template <std::size_t N>
typename NumericRange<N>::const_iterator NumericRange<N>::end() const {
  return NumericRange<N>::const_iterator();
}

// fixme: duplicated code: could be solved with min / max functors
// passed to a single "extremum" function (would also need to know
// whether to use inf or -inf as initial value)
template <template <typename, typename> class Container, typename Allocator, std::size_t N>
std::array<double, N> get_lower_from_collection(const Container<std::array<double, N>, Allocator> & container) {
  std::array<double, N> result;
  for (std::size_t k=0; k<N; ++k) {
    double min_val = std::numeric_limits<double>::infinity();
    for (const std::array<double, N> & arr : container)
      min_val = std::min(min_val, arr[k]);
    result[k] = min_val;
  }
  return result;
}

template <template <typename, typename> class Container, typename Allocator, std::size_t N>
std::array<double, N> get_upper_from_collection(const Container<std::array<double, N>, Allocator> & container) {
  std::array<double, N> result;
  for (std::size_t k=0; k<N; ++k) {
    double max_val = -std::numeric_limits<double>::infinity();
    for (const std::array<double, N> & arr : container)
      max_val = std::max(max_val, arr[k]);
    result[k] = max_val;
  }
  return result;
}

#endif
