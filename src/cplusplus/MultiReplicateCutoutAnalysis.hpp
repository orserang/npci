#ifndef _MULTIREPLICATECUTOUTANALYSIS_H
#define _MULTIREPLICATECUTOUTANALYSIS_H

// for computing the product over several single replicates (and
// ensuring their parameters are compatible)

#include "SingleReplicateCutoutAnalysis.hpp"
#include "Functional.hpp"

// forward declaration
template <typename> class MultiReplicateCutoutAnalysisReport;

template <typename K>
class MultiReplicateCutoutAnalysis : public BaseCutoutAnalysis<K> {
  friend class MultiReplicateCutoutAnalysisReport<K>;
public:
  // construct from several BaseCutoutAnalysis<K>, thus preventing <N> from being used
  MultiReplicateCutoutAnalysis(std::list<std::shared_ptr<BaseCutoutAnalysis<K> > > all_replicate_analyses_param, const std::string & label_param):
    all_replicate_analyses_(all_replicate_analyses_param)
  {
    if (all_replicate_analyses_.size() == 0)
      throw OutOfBoundsException("MultiReplicateCutoutAnalysis requires non-empty list of replicates");

    // cannot use BaseCutoutAnalysis<K> initializer because you first
    // need to ensure the list is not empty (immediately above)
    std::shared_ptr<BaseCutoutAnalysis<K> > front = all_replicate_analyses_.front();
    BaseCutoutAnalysis<K>::td_ptr_ = front->td_ptr_;
    BaseCutoutAnalysis<K>::gr_ptr_list_ = front->gr_ptr_list_;

    BaseCutoutAnalysis<K>::label_ = label_param;

    // verify that all replicates use the same parameter pointers
    for (std::shared_ptr<BaseCutoutAnalysis<K> > base_ptr : all_replicate_analyses_)
      if (base_ptr->td_ptr_ != BaseCutoutAnalysis<K>::td_ptr_ || base_ptr->gr_ptr_list_ != BaseCutoutAnalysis<K>::gr_ptr_list_)
	throw IncompatibleParametersExecption("The parameters of all provided replicate cutout analyses must be the same");
  }

  // compute the product over the likelihoods from replicate analyses
  // (computes the product using the cached likelihoods from each
  // replicate)
  virtual std::list<std::vector<LogDouble> > get_all_likelihoods() const {
    std::list<std::vector<LogDouble> > result;
    for (std::shared_ptr<BaseCutoutAnalysis<K> > single_analysis : all_replicate_analyses_) {
      const std::list<std::vector<LogDouble> > & like = single_analysis->get_all_likelihoods();
      if (result.size() == 0)
	result = like;
      else {
	for ( auto iter_pair = std::make_pair(result.begin(), like.cbegin());
	     iter_pair.first != result.end() && iter_pair.second != like.cend();
	     ++iter_pair.first, ++iter_pair.second )
	  std::transform(iter_pair.first->begin(), iter_pair.first->end(), iter_pair.second->cbegin(), iter_pair.first->begin(), times_eq<LogDouble>() );
      }
    }

    // normalize the result (use arbitrary template parameter)
    return LikelihoodSimilarity<1>::normalized_likelihoods(result);
  }

  struct IncompatibleParametersExecption : public StandardException {
    IncompatibleParametersExecption(const std::string & str) throw():
      StandardException(str) { }
  };

  const std::list<std::shared_ptr<BaseCutoutAnalysis<K> > > & get_all_replicate_analyses() {
    return all_replicate_analyses_;
  }

  BaseReport generate_report() const;

private:
  std::list<std::shared_ptr<BaseCutoutAnalysis<K> > > all_replicate_analyses_;
};

#include "MultiReplicateCutoutAnalysisReport.hpp"

template <typename K> 
BaseReport MultiReplicateCutoutAnalysis<K>::generate_report() const {
  return MultiReplicateCutoutAnalysisReport<K>(*this);
}

#endif

