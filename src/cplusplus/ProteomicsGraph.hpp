#ifndef _PROTEOMICSGRAPH_H
#define _PROTEOMICSGRAPH_H

#include "ProteomicsGraphNodes.hpp"
#include "CutoutGraph.hpp"
#include "SetOperations.hpp"
#include "StringOperations.hpp"
#include "StandardExceptions.hpp"

#include <limits>
#include <string>
#include <fstream>
#include <algorithm>
#include <memory>

template <class EVIDENCENODE>
class ProteomicsGraph : public CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION> {
public:
  typedef MarkableDiGraphNode<ProteomicsKey> BaseNode;
  typedef MarkableDiGraphNode<ProteomicsKey>* BaseNodePtr;

  typedef ProteinGroupNode* ProteinGroupNodePtr;
  typedef ProteinNode* ProteinNodePtr;
  typedef PeptideNode* PeptideNodePtr;
  typedef EVIDENCENODE* EVIDENCENODEPtr;

  typedef std::set<ProteinNode> ProteinGroup;
  typedef std::set<ProteinGroup> ProteinGroupSet;

protected:
  std::set<ProteinGroupNodePtr> protein_group_node_ptrs;
  std::set<ProteinNodePtr> protein_node_ptrs;
  std::set<PeptideNodePtr> peptide_node_ptrs;
  std::set<EVIDENCENODEPtr> evidence_node_ptrs;

public:
  ProteomicsGraph() {}
  ProteomicsGraph(const ProteomicsGraph & rhs) { copy_from_rhs(rhs); }

  const ProteomicsGraph & operator =(const ProteomicsGraph & rhs);
  virtual void copy_from_rhs(const ProteomicsGraph & rhs);

  const std::set<ProteinGroupNodePtr> & get_protein_group_node_ptr_set() const { return protein_group_node_ptrs; }
  const std::set<ProteinNodePtr> & get_protein_node_ptr_set() const { return protein_node_ptrs; }
  const std::set<PeptideNodePtr> & get_peptide_node_ptr_set() const { return peptide_node_ptrs; }
  const std::set<EVIDENCENODEPtr> & get_evidence_node_ptr_set() const { return evidence_node_ptrs; }

  std::list<std::set<ProteomicsKey> > get_grouped_keys() const;

  // modifiers: digraph related
  virtual void clear();

  void add_node(const ProteinGroupNode & protein_group_node);
  void add_node(const ProteinNode & protein_node);
  void add_node(const PeptideNode & peptide_node);
  void add_node(const EVIDENCENODE & evidence_node);

  void remove_node_ptr(ProteinGroupNodePtr protein_group_node_ptr);
  void remove_node_ptr(ProteinNodePtr protein_node_ptr);
  void remove_node_ptr(PeptideNodePtr peptide_node_ptr);
  void remove_node_ptr(EVIDENCENODEPtr evidence_node_ptr);
};

#include "ProteomicsGraph.cpp"

#endif

