#ifndef _MARKABLEDIGRAPH_HPP
#define _MARKABLEDIGRAPH_HPP

#include "DiGraph.hpp"
#include "MarkableDiGraphNode.hpp"

// Note: this graph should be acyclic for use here
template <typename K>
class MarkableDiGraph : public DiGraph<K, MarkableDiGraphNode<K> >
{
public:
// note: it might be useful to define a function to push / pop all
// markings, enabling external routines that change markings to call
// one another without unexpected errors (this may lower the
// efficiency of the tasks at hand)

  typedef MarkableDiGraphNode<K> Node;
  typedef MarkableDiGraphNode<K>* NodePtr;

  void mark_node_and_dependent_ptrs_from_ptr_set(const std::set<NodePtr> & node_ptr_set) {
    // assumes nodes are only marked along with their dependents,
    // i.e. the successors of the greatest predecessor. this permits
    // early termination of the routine when marked nodes are hit. 
    
    // first find the greatest predecessor set:
    std::set<NodePtr> ancestor_node_ptr_set = greatest_predecessor_node_ptr_set(node_ptr_set);

    mark_node_and_descendent_ptrs_from_ptr_set(ancestor_node_ptr_set);
  }

  // note: this can be performed more efficiently by marking nodes;
  // however, this would require marking all ancestors, potentially
  // muddling the functionality of this class (nodes must be marked
  // with all descendents). marking dependents is allowed, because
  // they are all descendents of some node set.
  std::set<NodePtr> greatest_predecessor_node_ptr_set(const std::set<NodePtr> & node_ptr_set) const {
    if (node_ptr_set.size() == 0)
      return std::set<NodePtr>();

    std::set<NodePtr> predecessor_node_ptr_set;
    for (const NodePtr node_ptr : node_ptr_set)
      for (const NodePtr p : node_ptr->get_predecessor_ptrs())
	predecessor_node_ptr_set.insert(p);

    // note: the following line assumes graph is acyclic; cyclic graph
    // may lead to infinite recursion
    std::set<NodePtr> ancestor_node_ptr_set = greatest_predecessor_node_ptr_set(predecessor_node_ptr_set);

    // add ptrs to nodes that have nod predecessors themselves
    for (const NodePtr node_ptr : node_ptr_set)
      if (node_ptr->get_predecessor_ptrs().size() == 0)
	ancestor_node_ptr_set.insert(node_ptr);

    return ancestor_node_ptr_set;
  }

  void mark_node_and_descendent_ptrs_from_ptr_set(const std::set<NodePtr> & node_ptr_set) {
    for (NodePtr node_ptr : node_ptr_set)
      mark_node_and_descendent_ptrs(node_ptr);
  }
  void mark_node_and_descendent_ptrs(NodePtr node_ptr) {
    node_ptr->mark();
    for (NodePtr p : node_ptr->get_successor_ptrs()) {
      // assumes nodes are only marked along with their descendents
      // (permits early termination of the routine when marked nodes
      // are hit)
      if ( ! p->is_marked() )
	mark_node_and_descendent_ptrs(p);
    }
  }

  void unmark_all() {
    for (NodePtr node_ptr : *this)
      node_ptr->unmark();
  }

  std::set<NodePtr> unmarked_node_and_descendent_ptrs_from_ptr_set(const std::set<NodePtr> & node_ptr_set) const {
    std::set<NodePtr> result;
    for (NodePtr node_ptr : node_ptr_set)
      insert_all( result, unmarked_node_and_descendent_ptrs(node_ptr) );
    return result;
  }


  std::set<NodePtr> unmarked_node_and_dependent_ptrs_from_ptr_set(const std::set<NodePtr> & node_ptr_set) const {
    std::set<NodePtr> ancestor_node_ptr_set = greatest_predecessor_node_ptr_set(node_ptr_set);
    return unmarked_node_and_descendent_ptrs_from_ptr_set(ancestor_node_ptr_set);
  }

  std::set<NodePtr> unmarked_node_and_descendent_ptrs(NodePtr node_ptr) const {
    std::set<NodePtr> result;

    // assumes nodes are only marked along with their descendents
    // (permits early termination of the routine when marked nodes
    // are hit)
    if (node_ptr->is_marked())
      return result;

    result.insert( node_ptr );

    for (NodePtr node_ptr2 : node_ptr->get_successor_ptrs())
      // assumes nodes are only marked along with their descendents
      // (permits early termination of the routine when marked nodes
      // are hit)
      if ( ! node_ptr2->is_marked() )
	insert_all( result, unmarked_node_and_descendent_ptrs(node_ptr2) );
    return result;
  }
};

#endif
