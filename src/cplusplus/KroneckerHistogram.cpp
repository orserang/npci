template <typename T>
KroneckerHistogram<T>::KroneckerHistogram(const std::vector<T> & values):
  n_(0) {
  add_values(values);
}

template <typename T>
void KroneckerHistogram<T>::add_values(const std::vector<T> & values)
{
  for (const T val : values)
    add_value(val);
}

template <typename T>
void KroneckerHistogram<T>::remove_values(const std::vector<T> & values)
{
  for (const T val : values)
    remove_value(val);
}

template <typename T>
void KroneckerHistogram<T>::add_value(T val, int count_change)
{
  n_ += count_change;

  // if the object isn't present, add it with zero count so that
  // addition will result in a 1 count
  if ( std::unordered_map<T, int >::count(val) == 0 )
    std::unordered_map<T, int >::operator[](val) = 0;
  
  int & count = std::unordered_map<T, int >::operator[](val);
  count += count_change;

  if ( count == 0 )
    std::unordered_map<T, int>::erase(val);
}

template <typename T>
void KroneckerHistogram<T>::remove_value(T val, int count_change)
{
  n_ -= count_change;

  // if the object isn't present, add it with zero count so that
  // subtraction will result in a -1 count
  if ( std::unordered_map<T, int >::count(val) == 0 )
    std::unordered_map<T, int >::operator[](val) = 0;

  int & count = std::unordered_map<T, int >::operator[](val);
  count -= count_change;

  if ( count == 0 )
    std::unordered_map<T, int>::erase(val);
}

template <typename T>
void KroneckerHistogram<T>::print() const
{
  std::cout << "[";
  for ( const std::pair<T, int> & val_and_count : *this )
    std::cout << "(" << val_and_count.first << ", " << val_and_count.second << ")" << ", ";
  std::cout << "] ";
  auto vals = values(*this);
  std::cout << std::accumulate(vals.begin(), vals.end(), 0) << " ";
  std::cout << n_ << std::endl;
}
