template <typename PROTEOMICSGRAPH>
std::list<std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> > StandardProteomicsGraphGrouper<PROTEOMICSGRAPH>::get_protein_groups(const PROTEOMICSGRAPH & pg) const {
  std::list<std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> > result;
  
  // build a map of peptide sets to proteins
  typedef std::set<typename PROTEOMICSGRAPH::BaseNodePtr> SuccessorSetType;
  typedef std::map<SuccessorSetType, std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> > peptide_set_to_protein_ptr_map;
  peptide_set_to_protein_ptr_map peptide_sets_to_proteins;
  const std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> & prots = pg.get_protein_node_ptr_set();
  for (const typename PROTEOMICSGRAPH::ProteinNodePtr protein_node_ptr : prots )
    {
      const SuccessorSetType & peps = protein_node_ptr->get_successor_ptrs();
      peptide_sets_to_proteins[peps].insert( protein_node_ptr );
    }
  for (const std::pair<SuccessorSetType, std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> > & peptides_and_proteins : peptide_sets_to_proteins)
    result.push_back(peptides_and_proteins.second);

  return result;
}

