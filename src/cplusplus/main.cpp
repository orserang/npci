#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <numeric>
 
#include "PlainTextSetTargetDecoy.hpp"
#include "PlainTextRanking.hpp"
#include "GroupRanking.hpp"
#include "ProteinIDGraphFactory.hpp"
#include "ProteomicsGraphGrouperRegistry.hpp"

#include "SingleReplicateCutoutAnalysis.hpp"
#include "MultiReplicateCutoutAnalysis.hpp"

#include "IIDSmoothedLikelihoodSimilarity.hpp"
#include "DirichletSmoothedLikelihoodSimilarity.hpp"
#include "KullbackLeiblerSmoothedLikelihoodSimilarity.hpp"
#include "KolmogorovSmirnovLikelihoodSimilarity.hpp"

int main(int argc, char**argv)
{
  if (argc >=4 ) {

    std::cout.precision(10);

    std::cerr << "Loading Graph" << std::endl;
    // create the graph by tring all file types and choosing the one
    // that doesn't throw an exception or return an empty graph. also,
    // use trivial grouping (each protein has its own group). the
    // grouping string could also be "identical_connectivity", which
    // would perform standard protein grouping.
    std::shared_ptr<ProteinIDGraph> pg_ptr = ProteinIDGraphFactory<ProteinIDGraph>::create_ungrouped_proteomics_graph(FileAndFormat(argv[1]));
    Singleton<ProteomicsGraphGrouperRegistry<ProteinIDGraph> >::get_instance().group_proteins_from_name(*pg_ptr, "identical_connectivity");

    std::cerr << "Loading TargetDecoy" << std::endl;
    std::shared_ptr<TargetDecoy<std::string> > td_ptr( new PlainTextSetTargetDecoy<std::string>(argv[2]) );

    std::cerr << "Loading and Grouping Rankings" << std::endl;

    // convert the ranking to a grouped ranking (as in Serang 2012
    // JPR, grouping is a matter of what hypotheses are investigated,
    // and thus always used-- protein-level inference simply means
    // each protein has its own group).
    std::list<std::shared_ptr<GroupRanking<std::string> > > g_rk_ptr_lst;

    const int downsample_factor = 1;
    const int upper_index_limit = 1000000000000;

    for (int i=3; i<argc; ++i) {
      PlainTextRanking<std::string> rk(argv[i]);
      std::shared_ptr<GroupRanking<std::string> > g_rk_ptr(new GroupRanking<std::string>(rk.downsampled(downsample_factor, upper_index_limit), pg_ptr->get_grouped_keys()));

      g_rk_ptr_lst.push_back( g_rk_ptr );
    }

    // you can create the SingleReplicateCutoutAnalysis with any of
    // these divergence measures: Dirichlet, IID likelihood, KL
    // divergence, KS test

    // 100 specifies 100 buckets for numeric integration, 10 specifies
    // that it is the minimum number of data points that will still be
    // considered (don't bother comparing samples with only 9 PSMs
    // remaining, since the estimate of the distribution will be
    // terrible).

    std::shared_ptr<LikelihoodSimilarity<1> > similarity_engine( new DirichletSmoothedLikelihoodSimilarity<1>(100, 10, true) );
    //    std::shared_ptr<LikelihoodSimilarity<1> > similarity_engine( new IIDSmoothedLikelihoodSimilarity<1>(100, 10, true) );
    //    std::shared_ptr<LikelihoodSimilarity<1> > similarity_engine( new KullbackLeiblerSmoothedLikelihoodSimilarity<1>(100, 10, true) );
    //    std::shared_ptr<LikelihoodSimilarity<1> > similarity_engine( new KolmogorovSmirnovLikelihoodSimilarity<1>(10) );

    std::cerr << "Performing CutoutAnalysis" << std::endl;
    // construct a SingleReplicateCutoutAnalysis using ProteinIDGraph,
    // the TargetDecoy object, all of the GroupedRankings, the
    // similarity engine, and a label (for the report).

    std::shared_ptr<BaseCutoutAnalysis<ProteomicsKey> > repA_ptr( new SingleReplicateCutoutAnalysis<ProteomicsKey, 1>(pg_ptr, td_ptr, g_rk_ptr_lst, similarity_engine, "Replicate 1") );

    // add the SingleReplicateCutoutAnalysis to a multi replicate
    // analysis (consisting of conditionally independent experimental
    // results from technical replicates or different types of
    // experiments (e.g. you could add one
    // SingleReplicateCutoutAnalysis analyzing proteomics, one
    // analyzing RNASeq, etc. and use them to aggregate scores over
    // different types of inference. 
    std::list<std::shared_ptr<BaseCutoutAnalysis<ProteomicsKey> > > single_rep_list{repA_ptr};
    MultiReplicateCutoutAnalysis<ProteomicsKey> mca(single_rep_list, "Aggregate");

    std::cerr << "Generating and Writing Report" << std::endl;
    std::cout << mca.generate_report() << std::endl;
  }
  else
    std::cerr << "usage:\tnpCI <graph file> <plain text target decoy> <plain text ranking> [<plain text ranking>...]" << std::endl;
}

