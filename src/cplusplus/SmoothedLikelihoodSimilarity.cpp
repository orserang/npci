template <std::size_t N>
const std::vector<double> BaseSmoothedLikelihoodSimilarity<N>::sigma_range_ = {0.005, 0.01, 0.075, 0.1, 0.125, 0.15, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 1.0, 2.0, 4.0, 10.0, 100.0};

// fixme: debug (faster)
//template <std::size_t N>
//const std::vector<double> BaseSmoothedLikelihoodSimilarity<N>::sigma_range_ = {0.05};
//const std::vector<double> BaseSmoothedLikelihoodSimilarity<N>::sigma_range_ = {0.1};

template <std::size_t N>
BaseReport BaseSmoothedLikelihoodSimilarity<N>::generate_report() const {
  return SmoothedLikelihoodSimilarityReport<N>(*this);
}

// to be placed outside the class so that the specialized class
// SmoothedLikelihoodSimilarity<1> is available before it is used
template <std::size_t N>
void SmoothedLikelihoodSimilarity<N>::init_best_sigmas() {
  std::cerr << "\tfinding best sigmas (on all cross sections)" << std::endl;
  for (std::size_t k=0; k<N; ++k) {
    std::list<std::array<KroneckerHistogramCollection<1>, 2> > khc_for_column;
    for (const std::array<KroneckerHistogramCollection<N>, 2> & khc_pair : BaseSmoothedLikelihoodSimilarity<N>::kronicker_collection_lists_)
      khc_for_column.push_back( std::array<KroneckerHistogramCollection<1>, 2>{{ khc_pair[0].get_histogram_collection_using_column(k), khc_pair[1].get_histogram_collection_using_column(k) }} );

    std::shared_ptr<SmoothedLikelihoodSimilarity<1> > sls_for_column_ptr = clone_without_data();
    // the following will also call init_best_sigmas:
    sls_for_column_ptr->init_data(khc_for_column);

    BaseSmoothedLikelihoodSimilarity<N>::best_sigmas_[k] = sls_for_column_ptr->get_best_sigmas()[0];
    BaseSmoothedLikelihoodSimilarity<N>::data_scaling_factors_[k] = sls_for_column_ptr->data_scaling_factors_[0];
    BaseSmoothedLikelihoodSimilarity<N>::negative_scores_and_sigmas_[k] = sls_for_column_ptr->get_negative_scores_and_sigmas()[0];
  }

  std::cerr << "sigma has been estimated " << BaseSmoothedLikelihoodSimilarity<N>::best_sigmas_ << std::endl;
}

