#ifndef _FUNCTIONAL_H
#define _FUNCTIONAL_H

template <typename T>
struct plus {
  auto operator()(const T& x, const T& y) const -> decltype(x+y) {
    return x + y;
  }
};

template <typename T>
struct minus {
  auto operator()(const T& x, const T& y) const -> decltype(x-y) {
    return x - y;
  }
};

template <typename T>
struct times {
  auto operator()(const T& x, const T& y) const -> decltype(x*y) {
    return x * y;
  }
};

template <typename T>
struct divide {
  auto operator()(const T& x, const T& y) const -> decltype(x/y) {
    return x / y;
  }
};

template <typename T>
struct plus_eq {
  T operator()(T& x, const T& y) const {
    return x += y;
  }
};

template <typename T>
struct minus_eq {
  T operator()(T& x, const T& y) const {
    return x -= y;
  }
};

template <typename T>
struct times_eq {
  T operator()(T& x, const T& y) const {
    return x *= y;
  }
};

template <typename T>
struct divide_eq {
  T operator()(T& x, const T& y) const {
    return x /= y;
  }
};

template <typename T>
struct min_functor {
  T operator()(const T& x, const T& y) const {
    return min(x, y);
  }
};

template <typename T>
struct max_functor {
  T operator()(const T& x, const T& y) const {
    return max(x, y);
  }
};

#endif

