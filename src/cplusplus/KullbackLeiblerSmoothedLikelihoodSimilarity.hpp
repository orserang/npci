#ifndef _KULLBACKLEIBLERSMOOTHEDLIKELIHOODSIMILARITY_H
#define _KULLBACKLEIBLERSMOOTHEDLIKELIHOODSIMILARITY_H

#include "SmoothedLikelihoodSimilarity.hpp"

template <std::size_t N>
class KullbackLeiblerSmoothedLikelihoodSimilarity : public SmoothedLikelihoodSimilarity<N> {
public:
  KullbackLeiblerSmoothedLikelihoodSimilarity(const std::list< std::array<KroneckerHistogramCollection<N>, 2> > & kronicker_collection_lists, int num_bins, int minimum_data_param, bool save_distributions):
    SmoothedLikelihoodSimilarity<N>(kronicker_collection_lists, num_bins, minimum_data_param, save_distributions) {
    this->init_best_sigmas();
  }

  KullbackLeiblerSmoothedLikelihoodSimilarity(int num_bins_param, int minimum_data_param, bool save_distributions):
    SmoothedLikelihoodSimilarity<N>(num_bins_param, minimum_data_param, save_distributions) {
    // if no data is provided, don't bother initializing sigma (it
    // won't find any non NaN non inf values and will throw an
    // exception)
  }

  // if the data is reinitialized, estimate the best sigmas again
  virtual void init_data(const std::list< std::array<KroneckerHistogramCollection<N>, 2> > & kronicker_collection_lists_param) {
    SmoothedLikelihoodSimilarity<N>::init_data(kronicker_collection_lists_param);
  }

  virtual std::shared_ptr<SmoothedLikelihoodSimilarity<1> > clone_without_data() const {
    return std::shared_ptr<SmoothedLikelihoodSimilarity<1> >( new KullbackLeiblerSmoothedLikelihoodSimilarity<1>(SmoothedLikelihoodSimilarity<N>::num_bins_, SmoothedLikelihoodSimilarity<N>::minimum_data_, SmoothedLikelihoodSimilarity<N>::save_distributions_) );
  }

protected:

  virtual LogDouble smoothed_likelihood_function(const KroneckerHistogram<std::array<double, N> > & lhs_hist, const SmoothedHistogram<LogDouble, N> & lhs_hist_smoothed, const KroneckerHistogram<std::array<double, N> > & rhs_hist, const SmoothedHistogram<LogDouble, N> & rhs_hist_smoothed, const std::array<double, N> & sigma) const {
    if ( ! (lhs_hist_smoothed.get_numeric_range() == rhs_hist_smoothed.get_numeric_range()) )
      throw SizeMismatchException("The numeric ranges integrated by the DirichletLikelihoodFunctor must be identical");

    const NumericRange<N> & nr = lhs_hist_smoothed.get_numeric_range();
    LogDouble delta_prod = nr.template delta_product<LogDouble>();

    // treat the total number of bits (symmetric KL + bits from
    // parameters) as a log probability (should be the probability
    // given an otherwise uniform prior)
    LogDouble result = exp(-(directed_kl_divergence(lhs_hist_smoothed, rhs_hist_smoothed, delta_prod) + directed_kl_divergence(rhs_hist_smoothed, lhs_hist_smoothed, delta_prod))/LogDouble(2.0)) * sigma_prior(sigma);
    
    return result;
  }
private:
  // the sigma prior is the information in sigma (due to
  // precision). It should be proportional to the log of each sigma
  // parameter (standard minimum description length approach).
  LogDouble sigma_prior(const std::array<double, N> & sigma) const {
    LogDouble result(1.0);
    for (double sigma_val : sigma)
      result *= LogDouble(1.0) / LogDouble(sigma_val);
    return result;
  }
  LogDouble directed_kl_divergence(const SmoothedHistogram<LogDouble, N> & bg, const SmoothedHistogram<LogDouble, N> & fg, LogDouble delta_prod) const {
    LogDouble result(0.0);
    for (auto iter_pair = std::make_pair(bg.cbegin(), fg.cbegin()); iter_pair != std::make_pair(bg.cend(), fg.cend()); ++iter_pair.first, ++iter_pair.second ) {
      LogDouble bg_hist_val = *iter_pair.first;
      LogDouble fg_hist_val = *iter_pair.second;
      LogDouble term = directed_kl_term(bg_hist_val, bg.get_n(), fg_hist_val, fg.get_n());
      result += term;
    }

    // scale by the bucket size (to turn sum into integral)
    return result * delta_prod;
  }

  LogDouble directed_kl_term(LogDouble bg_hist_val, double bg_n, LogDouble fg_hist_val, double fg_n) const {
    // for terms where both base and exponent approach zero, the
    // product is not affected (LogDouble will ignore
    // these). Effectively, these values are not in the
    // support). Hence, for readability, only consider values with
    // nonzero fg_hist_val (the if statement is not necessary
    // because LogDouble already handles it)
    
    if ( fg_hist_val == LogDouble(0.0) )
      return LogDouble(0.0);
    
    // rather than rescale the histograms to make the PDF, simply
    // divide each histogram value by n (since the integral was n,
    // it will become 1 by linearity)
    LogDouble bg_pdf_val = bg_hist_val / LogDouble(bg_n);
    LogDouble fg_pdf_val = fg_hist_val / LogDouble(fg_n);
    
    // the exponentiated term a log(a/b) becomes (a/b)^a

    // modified for greater precision:
    //    LogDouble term = fg_pdf_val * LogDouble(log( double(fg_pdf_val / bg_pdf_val) ));
    LogDouble term = fg_pdf_val * LogDouble( (fg_pdf_val / bg_pdf_val).get_log_absolute_value() );

    if ( LogDouble::is_nan(term) )
      throw NumericalStabilityException("Encountered nan in KullbackLeiblerLikelihoodFunctor");

    return term;
  }
};

#endif
