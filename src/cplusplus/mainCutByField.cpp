#include "TabSeparatedFile.hpp"

int main(int argc, char ** argv) {
  if (argc < 3) {
    std::cerr << "usage: TSV fname field1 [field2, ...]" << std::endl;
    exit(1);
  }
  
  std::vector<std::string> fields;
  for (int k=2; k<argc; ++k)
    fields.push_back(argv[k]);

  TabSeparatedFile dsf(argv[1], fields);

  for (const std::vector<std::string> & row : dsf.get_query_value_rows()) {
    std::size_t ind = 0;
    for (const std::string & str : row) {
      std::cout << str;
      if (ind != row.size() - 1)
	std::cout << "\t";
      ++ind;
    }
    std::cout << std::endl;
  }

  return 0;
}
