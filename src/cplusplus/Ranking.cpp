template <typename K>
void Ranking<K>::create_best_sets_first_from_map()
{
  // put the ranked protein sets into
  // best_sets_of_proteins_first (highest scoring first)
  for (typename DoubleToObjectSet::const_reverse_iterator score_and_scored_object_set = score_to_scored_object_set.rbegin(); score_and_scored_object_set != score_to_scored_object_set.rend(); ++score_and_scored_object_set)
    best_sets_first.push_back( score_and_scored_object_set->second );
}
