#include "ProteinIDGraphLoader.hpp"

void ProteinIDGraphLoader::load_and_verify(ProteinIDGraph & pg, const FileAndFormat & faf)
{
  pg.clear();
  load_from_file(pg, faf);
  if ( pg.size() == 0 )
    throw FormatException("Empty graph");

  // look for nan scores
  for (const PSMNode* psm_node_ptr : pg.get_evidence_node_ptr_set())
    for (double val : psm_node_ptr->get_evidence())
      if (isnan(val))
	throw FormatException("Graph contains uninitialized PSM scores: " + (*psm_node_ptr->get_predecessor_ptrs().begin())->get_key());
}

