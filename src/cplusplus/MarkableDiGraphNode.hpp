#ifndef _MARKABLEDIGRAPHNODE_HPP
#define _MARKABLEDIGRAPHNODE_HPP

#include "DiGraphNode.hpp"
#include "MarkableMixin.hpp"

template <typename K>
class MarkableDiGraphNode : public MarkableMixin, public DiGraphNode<K, MarkableDiGraphNode<K> > {
public:
  typedef K KeyType;

  MarkableDiGraphNode() {}
  MarkableDiGraphNode(const K & key_param):
    DiGraphNode<K, MarkableDiGraphNode<K> >(key_param) {}
  virtual ~MarkableDiGraphNode() {}

  void print() const {
    if (is_marked())
      std::cout << "*MK*";
    DiGraphNode<K, MarkableDiGraphNode<K> >::print();
  }

  virtual MarkableDiGraphNode<K>*clone() const {
    return new MarkableDiGraphNode<K>(*this);
  }
};

#endif
