#ifndef _DIGRAPH_H
#define _DIGRAPH_H

#include <vector>
#include <set>
#include <list>
#include <map>
#include <unordered_map>
#include <iostream>

#include "DiGraphNode.hpp"
#include "StringOperations.hpp"
#include "StandardExceptions.hpp"
#include "PythonMapOperations.hpp"

// note: this class gives access to raw pointers; this lets the graph
// operate very efficiently once it's constructed (it doesn't need to
// perform lookup by key). the pointers should not be deleted
// externally (they cannot be declared const, because the user may
// want to alter node members). the pointers could be replaced with
// wrappers (e.g. iterators).

template <typename K, typename NodeType >
class DiGraph {
public:
  typedef NodeType Node;
  // this would use shared_ptr (owns the memory)
  typedef NodeType* NodePtr;

  typedef typename std::set<NodePtr>::const_iterator const_iterator;
  const_iterator begin() const { return node_ptr_set.cbegin(); }
  const_iterator end() const { return node_ptr_set.cend(); }

  // constructors:
  DiGraph() {}
  DiGraph(const DiGraph<K, NodeType> & rhs) { copy_from_rhs(); }
  // virtual destructor because it is designed to be inherited from
  virtual ~DiGraph() { clear(); }

  // modifiers:
  virtual void clear();
  const DiGraph<K, NodeType> & operator =(const DiGraph<K, NodeType> & rhs);
  virtual void copy_from_rhs(const DiGraph<K, NodeType> & rhs);

  virtual void add_node(const Node & n);
  // passed by reference because the argument is set to NULL after
  // being freed
  virtual void remove_node_ptr(NodePtr node_ptr);
  virtual void remove_key(const K & key);

  void add_edge_between_keys(const K & from_key, const K & to_key);
  void remove_edge_between_keys(const K & from_key, const K & to_key);

  // should only be performed on nodes that are already in the graph
  void add_edge(NodePtr from, NodePtr to);
  void remove_edge(NodePtr from, NodePtr to);

  // accessors:
  size_t size() const { return key_to_node_ptr.size(); }
  bool contains_node_ptr(const NodePtr node_ptr) const { return node_ptr_set.count( node_ptr ) > 0; }
  bool contains_key(const K & key_param) const { return key_to_node_ptr.count( key_param ) > 0; }

  NodePtr operator [](const K & key_param) const;
  std::set<NodePtr> operator [](const std::set<K> & key_set) const;
  const typename std::unordered_map<K, NodePtr> & get_key_to_node_ptr() const { return key_to_node_ptr; }

  // primarily for debugging
  void print() const;
  void print_keys() const;

  class NodeNotFoundException : public StandardException {
  public:
    NodeNotFoundException(const std::string & str) throw():
      StandardException(str) { }
  };

  // if A --> B, then B should --> A
  void make_edges_symmetric();
  void moralize();
  void triangulate();

protected:
  std::unordered_map<K, NodePtr> key_to_node_ptr;
  std::set<NodePtr> node_ptr_set;
  virtual void copy_edges_from_rhs(const DiGraph<K, NodeType> & rhs);

private:
  void check_node_ptr_is_in_graph(NodePtr node_ptr) const {
    if ( ! contains_node_ptr(node_ptr) ) {
      throw NodeNotFoundException( "key: " + to_string(node_ptr->get_key()) + " ptr: " + to_string(node_ptr) );
    }
  }
  void check_key_is_in_graph(const K & key) const {
    if ( ! contains_key(key) )
      throw NodeNotFoundException( "key: " + to_string(key) );
  }
};

#include "DiGraph.cpp"

#endif
