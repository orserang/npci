#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <fstream>

#include "PlainTextSetTargetDecoy.hpp"
#include "PlainTextRanking.hpp"
#include "GroupRanking.hpp"
#include "ProteinIDGraphFactory.hpp"
#include "NPCIExceptions.hpp"

#include "SingleReplicateCutoutAnalysis.hpp"
#include "MultiReplicateCutoutAnalysis.hpp"

#include "DirichletSmoothedLikelihoodSimilarity.hpp"
#include "KullbackLeiblerSmoothedLikelihoodSimilarity.hpp"
#include "KolmogorovSmirnovLikelihoodSimilarity.hpp"
#include "IIDSmoothedLikelihoodSimilarity.hpp"

std::list<std::vector<std::array<double, 1> > > load_file(const std::string & fname) {
  std::ifstream fin(fname);
  std::list<std::vector<std::array<double, 1> > > all_lines;
  std::string buff;
  while (getline(fin, buff)) {
    std::vector<std::array<double, 1> > line;
    for (const std::string & str : split_whitespace(buff))
      line.push_back( std::array<double, 1>{{ from_string<double>(str) }} );

    // do not include empty lines
    if (line.size() > 0)
      all_lines.push_back(line);
  }
  
  return all_lines;
}
std::shared_ptr<LikelihoodSimilarity<1> > create_likelihood_similarity(const std::string & method) {
  std::shared_ptr<LikelihoodSimilarity<1> > similarity_engine;
  const int num_bins = 100;

  if ( method == "dirichlet" ) 
    similarity_engine = std::shared_ptr<LikelihoodSimilarity<1> >(new DirichletSmoothedLikelihoodSimilarity<1>(num_bins, 10, true));
  else if ( method == "kl" )
    similarity_engine = std::shared_ptr<LikelihoodSimilarity<1> >(new KullbackLeiblerSmoothedLikelihoodSimilarity<1>(num_bins, 10, true));
  else if ( method == "iid" )
    similarity_engine = std::shared_ptr<LikelihoodSimilarity<1> >(new IIDSmoothedLikelihoodSimilarity<1>(100, 10, true));
  else if ( method == "ks" )
    similarity_engine = std::shared_ptr<LikelihoodSimilarity<1> >(new KolmogorovSmirnovLikelihoodSimilarity<1>(10));
  else {
    std::cerr << "usage: method must be one of the following: { dirichlet , kl , iid , ks }" << std::endl;
    exit(1);
  }
  return similarity_engine;
}

int main(int argc, char**argv)
{
  if (argc == 4) {
    std::list<std::vector<std::array<double, 1> > > dataA = load_file(argv[1]);
    std::list<std::vector<std::array<double, 1> > > dataB = load_file(argv[2]);

    if ( dataA.size() != dataB.size() )
      throw IncompatibleFilesException("The files must contain the same number of (nonempty) lines");

    KroneckerHistogramCollection<1> khcA(dataA), khcB(dataB);

    const std::string & method = argv[3];
    std::shared_ptr<LikelihoodSimilarity<1> > similarity_engine = create_likelihood_similarity(method);

    std::cout << similarity_engine << std::endl;
  }
  else
    std::cerr << "usage: StatsTest <data point file> <data point file> { dirichlet , kl , iid , ks }" << std::endl;
}

