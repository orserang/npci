#ifndef _PROTEINIDGRAPH_H
#define _PROTEINIDGRAPH_H

#include "ProteomicsGraph.hpp"

class PSMNode : public EvidenceCutoutGraphNode<ProteomicsKey, 1> {
public:
  static constexpr int unknown_charge_state = -1;

  int charge_state;

  PSMNode(const ProteomicsKey & string_param, std::array<double, 1> score_param = std::array<double, 1>{{std::numeric_limits<double>::quiet_NaN()}}, int charge_state_param = unknown_charge_state):
    EvidenceCutoutGraphNode<ProteomicsKey, 1>(string_param, score_param),
    charge_state(charge_state_param) { }

  virtual void print() const {
    std::cout << KeyComparable<ProteomicsKey>::get_key() << " ";
    std::cout << get_evidence() << " ";
    std::cout << charge_state << std::endl;
  }

  virtual PSMNode * clone() const {
    return new PSMNode(*this);
  }
};

class ProteinIDGraph : public ProteomicsGraph<PSMNode> {
public:
  ProteinIDGraph() {}
  ProteinIDGraph(const ProteinIDGraph & rhs) { copy_from_rhs(rhs); }
};

#endif

