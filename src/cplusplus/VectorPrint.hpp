#ifndef _VECTORPRINT_H
#define _VECTORPRINT_H

#include <iostream>
#include <vector>

template <typename T>
std::ostream & operator <<(std::ostream & os, const std::vector<T> & vec) {
  //  os << "{ ";
  unsigned int i;
  for (i=0; i<vec.size()-1; ++i)
    os << vec[i] << " ";
  os << vec[i];
  //  os << " }";
  return os;
}

#endif
