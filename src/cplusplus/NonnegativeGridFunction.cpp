#include "NonnegativeGridFunction.hpp"

template <typename DOUBLEVAL, std::size_t N>
NonnegativeGridFunction<DOUBLEVAL, N>::NonnegativeGridFunction(const NumericGridFunction<DOUBLEVAL, N> & ngf):
  NumericGridFunction<DOUBLEVAL, N>(ngf) {
  make_nonnegative();
}


template <typename DOUBLEVAL, std::size_t N>
void NonnegativeGridFunction<DOUBLEVAL, N>::make_nonnegative() {
  // threshold any negative or zero values to be very small

  // note: this allows numerical error to accumulate (many epsilon may
  // not be close to epsilon), but will prevent numerical problems
  // where distributions are ever zero. this could be specialized for
  // LogDouble for better performance.
  for (DOUBLEVAL & f: NonnegativeGridFunction::values_)
    if ( f <= DOUBLEVAL(0.0) )
      f = DOUBLEVAL(std::numeric_limits<double>::min());
}

template <typename DOUBLEVAL, std::size_t N>
const NonnegativeGridFunction<DOUBLEVAL, N> & NonnegativeGridFunction<DOUBLEVAL, N>::operator +=(const NumericGridFunction<DOUBLEVAL, N> & rhs) {
  NumericGridFunction<DOUBLEVAL, N>::operator +=(rhs);
  make_nonnegative();
  return *this;
}

template <typename DOUBLEVAL, std::size_t N>
const NonnegativeGridFunction<DOUBLEVAL, N> & NonnegativeGridFunction<DOUBLEVAL, N>::operator -=(const NumericGridFunction<DOUBLEVAL, N> & rhs) {
  NumericGridFunction<DOUBLEVAL, N>::operator -=(rhs);
  make_nonnegative();
  return *this;
}
