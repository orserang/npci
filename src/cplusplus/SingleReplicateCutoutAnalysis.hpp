#ifndef _SINGLEREPLICATECUTOUTANALYSIS_H
#define _SINGLEREPLICATECUTOUTANALYSIS_H

#include "CutoutGraph.hpp"
#include "BaseCutoutAnalysis.hpp"
#include "Subtype.hpp"
#include "KroneckerHistogramCollection.hpp"
#include "UnscaledNumericDensityCollection.hpp"
#include "IteratorUtilities.hpp"
#include "LikelihoodSimilarity.hpp"

// forward declaration
template <typename, std::size_t> class SingleReplicateCutoutAnalysisReport;

template <typename K, std::size_t N>
class SingleReplicateCutoutAnalysis : public BaseCutoutAnalysis<K> {
  friend class SingleReplicateCutoutAnalysisReport<K,N>;
public:
  SingleReplicateCutoutAnalysis(std::shared_ptr<CutoutGraph<K,N> > cg_ptr_param, std::shared_ptr<TargetDecoy<K> > & td_ptr_param, const std::list<std::shared_ptr<GroupRanking<K> > > & gr_ptr_list_param, std::shared_ptr<LikelihoodSimilarity<N> > likelihood_similarity_ptr_param, const std::string & label_param):
    BaseCutoutAnalysis<K>(td_ptr_param, gr_ptr_list_param, label_param),
    likelihood_similarity_ptr_(likelihood_similarity_ptr_param),
    label_(label_param),
    cg_ptr_(cg_ptr_param)
  {
    init_data_for_likelihood_similarity_ptr();
  }

  std::shared_ptr<LikelihoodSimilarity<N> > get_likelihood_similarity_ptr() const {
    return likelihood_similarity_ptr_;
  }

  const std::string & get_label() const {
    return label_;
  }

  BaseReport generate_report() const;

private:
  // functions for retrieving raw results:
  std::list<std::vector<LogDouble> > get_all_likelihoods() const {
    return likelihood_similarity_ptr_->get_all_likelihoods();
  }

  void init_data_for_likelihood_similarity_ptr() {
    std::list<std::array<KroneckerHistogramCollection<N>, 2> > target_and_decoy_collections;
    for (const std::shared_ptr<GroupRanking<K> > gr_ptr : BaseCutoutAnalysis<K>::gr_ptr_list_)
      target_and_decoy_collections.push_back(std::array<KroneckerHistogramCollection<N>, 2>{ {target_kronicker_collection(*gr_ptr), decoy_kronicker_collection(*gr_ptr)} });

    // use an init method to reset the data rather than cloning a new
    // object (same effect, but simpler)
    likelihood_similarity_ptr_->init_data(target_and_decoy_collections);
  }
  
  KroneckerHistogramCollection<N> target_kronicker_collection(const GroupRanking<K> & gr) {
    KroneckerHistogram<std::array<double, N> > kh_initial(target_scores());

    std::list<std::vector<std::array<double, N> > > neg_deltas = successive_eliminated_target_evidence(gr);
    std::list<KroneckerHistogram<std::array<double, N> > > kh_deltas;
    for (const std::vector<std::array<double, N> > & collection : neg_deltas)
      kh_deltas.push_back( -make_kronicker(collection) );

    return KroneckerHistogramCollection<N>(kh_initial, kh_deltas);
  }

  KroneckerHistogramCollection<N> decoy_kronicker_collection(const GroupRanking<K> & gr) {
    KroneckerHistogram<std::array<double, N> > kh_initial(decoy_only_scores());
    std::list<KroneckerHistogram<std::array<double, N> > > kh_deltas( gr.size(), KroneckerHistogram<std::array<double, N> >() );

    return KroneckerHistogramCollection<N>(kh_initial, kh_deltas);
  }
  
  std::vector<std::array<double, N> > target_scores() {
    // clear markings in *cg_ptr_
    cg_ptr_->unmark_all();
    mark_targets_and_dependents();
    return cg_ptr_->marked_evidence();
  }

  std::vector<std::array<double, N> > decoy_only_scores() {
    // clear markings in *cg_ptr_
    cg_ptr_->unmark_all();
    mark_targets_and_dependents();

    // return non marked evidence node scores
    return cg_ptr_->unmarked_evidence();
  }

  std::list<std::vector<std::array<double, N> > > successive_eliminated_target_evidence(const GroupRanking<K> & gr) {
    std::list<std::vector<std::array<double, N> > > result;

    // clear markings in *cg_ptr_
    cg_ptr_->unmark_all();

    for ( const std::set<std::set<K> > & group_key_set : gr.get_best_sets_first() ) {
      std::set<K> keys_at_threshold = flatten(group_key_set);

      /* note: using the following line (in place of the next) removes
	 decoys also, but requires that the targets start with
	 all_evidence instead of target_scores */
      //      std::set<K> target_keys = keys_at_threshold;
      std::set<K> target_keys = BaseCutoutAnalysis<K>::td_ptr_->target_matches(keys_at_threshold);

      std::set<typename MarkableDiGraphNode<K>::NodePtr> uncast_nodes;
      for (const K & key : target_keys)
	if ( cg_ptr_->contains_key(key) )
	  uncast_nodes.insert( (*cg_ptr_)[key] );
	else
	  std::cerr << "Warning: key \"" << key << "\" found in ranking, but not in graph" << std::endl;

      // append results with unmarked dependent evidence
      result.push_back( cg_ptr_->unmarked_dependent_evidence(uncast_nodes) );

      // mark dependents
      cg_ptr_->mark_node_and_dependent_ptrs_from_ptr_set(uncast_nodes);
    }
    return result;
  }

  void mark_targets_and_dependents() {
    // in all nodes in *cg_ptr_, if is target, mark dependents
    for (const std::pair<K, typename MarkableDiGraphNode<K>::NodePtr> & key_and_node_ptr: cg_ptr_->get_key_to_node_ptr()) {
      const typename MarkableDiGraphNode<K>::NodePtr node_ptr = key_and_node_ptr.second;
      if ( BaseCutoutAnalysis<K>::td_ptr_->is_target( key_and_node_ptr.first ) )
	cg_ptr_->mark_node_and_dependent_ptrs_from_ptr_set( std::set<typename MarkableDiGraphNode<K>::NodePtr>{node_ptr} );
    }
  }

  void mark_decoys_and_dependents() {
    // in all nodes in *cg_ptr_, if is target, mark dependents
    for (const std::pair<K, typename MarkableDiGraphNode<K>::NodePtr> & key_and_node_ptr: cg_ptr_->get_key_to_node_ptr()) {
      const typename MarkableDiGraphNode<K>::NodePtr node_ptr = key_and_node_ptr.second;
      if ( BaseCutoutAnalysis<K>::td_ptr_->is_decoy( key_and_node_ptr.first ) )
	cg_ptr_->mark_node_and_dependent_ptrs(node_ptr);
    }
  }

  std::shared_ptr<LikelihoodSimilarity<N> > likelihood_similarity_ptr_;
  std::string label_;
  std::shared_ptr<CutoutGraph<K,N> > cg_ptr_;
};

#include "SingleReplicateCutoutAnalysisReport.hpp"

template <typename K, std::size_t N>
BaseReport SingleReplicateCutoutAnalysis<K,N>::generate_report() const {
  return SingleReplicateCutoutAnalysisReport<K,N>(*this);
}

#endif
