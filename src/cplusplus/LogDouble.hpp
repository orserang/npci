#ifndef _LOGDOUBLE_H
#define _LOGDOUBLE_H

#include <math.h>
#include <limits>
#include <iostream>

#include "ComparableMixin.hpp"
#include "StandardExceptions.hpp"

template <typename T> int sgn(T val) {
    return (val > T(0)) - (val < T(0));
}

class LogDouble : public ComparableMixin<LogDouble> {
protected:
  signed char sign;
  double log_absolute_value;
  
  static double logaddexp(double log_a, double log_b);
  static double logaddexp_first_larger(double log_a, double log_b);
  static double logsubabsexp(double log_a, double log_b);
  static double logsubexp_first_larger(double log_a, double log_b);
public:
  LogDouble();
  explicit LogDouble(double x);

  static LogDouble create_from_log_absolute_value(double log_absolute_value_param);

  // +=, -=, *=, /=
  const LogDouble & operator +=(const LogDouble & rhs);
  const LogDouble & operator -=(const LogDouble & rhs);
  const LogDouble & operator *=(const LogDouble & rhs);
  const LogDouble & operator /=(const LogDouble & rhs);
  LogDouble operator -() const;

  explicit operator double() const { return sign * exp(log_absolute_value); }

  double get_log_absolute_value() const { return log_absolute_value; }
  double get_sign() const { return sign; }

  bool operator <(LogDouble rhs) const;
  bool operator ==(LogDouble rhs) const;
  //  bool operator !=(LogDouble rhs) const { return ! (*this == rhs); }

  static bool is_nan(LogDouble x) { return isnan(double(x)); }
  static bool is_inf(LogDouble x) { return isinf(x.get_log_absolute_value()); }

  friend LogDouble exp(LogDouble rhs);
  friend std::ostream & operator <<(std::ostream & os, LogDouble rhs);
  
  struct LogDoubleNumericException : public StandardException {
    LogDoubleNumericException(const std::string & str) throw():
      StandardException(str) { }
  };
};

LogDouble operator +(LogDouble lhs, LogDouble rhs);
LogDouble operator -(LogDouble lhs, LogDouble rhs);
LogDouble operator *(LogDouble lhs, LogDouble rhs);
LogDouble operator /(LogDouble lhs, LogDouble rhs);

LogDouble pow(LogDouble lhs, LogDouble rhs);

#endif
