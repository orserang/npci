#ifndef _SMOOTHEDLIKELIHOODSIMILARITYREPORT_H
#define _SMOOTHEDLIKELIHOODSIMILARITYREPORT_H

#include "LikelihoodSimilarityReport.hpp"
#include "LogDouble.hpp"
#include "SmoothedLikelihoodSimilarity.hpp"

// forward declaration
template <std::size_t N>
class SmoothedLikelihoodSimilarity;

// note: does not inherit from LikelihoodSimilarityReport (which would
// be an obvious choice). however, this report creates a tricky
// situation: 1) if you don't return reports (and write to them by
// reference), then there is the question of who creates the report
// (the most derived type would need to). 2) if you return reports,
// then SmoothedLikelihoodSimilarity needs to create a
// SmoothedLikelihoodSimilarityReport, but LikelihoodSimilarity (which
// has the shared report generation code for
// SmoothedLikelihoodSimilarityReport) also creates a report. I've
// chosen to implement option 2) and then copy the XML tree from
// LikelihoodSimilarityReport to
// SmoothedLikelihoodSimilarityReport. thus, if you inherit, both the
// SmoothedLikelihoodSimilarityReport and the
// LikelihoodSimilarityReport will define the "dimensions" attribute,
// resulting in two copies of the attribute after all data from the
// LikelihoodSimilarityReport is copied over.
template <std::size_t N>
class SmoothedLikelihoodSimilarityReport : public BaseReport {
public:
  SmoothedLikelihoodSimilarityReport(const BaseSmoothedLikelihoodSimilarity<N> & bsls):
    BaseReport("SmoothedLikelihoodSimilarityReport")
  {
    copy_all_from_report(bsls.LikelihoodSimilarity<N>::generate_report());
    
    // converts to string via << operator
    write_labeled_value("unnormalized_sigma", bsls.get_best_sigmas());
    write_labeled_value("normalized_sigma", bsls.get_best_sigmas() / bsls.data_scaling_factors_);

    write_labeled_value("normalized_sigma_range", bsls.sigma_range_);

    write_labeled_value("data_normalization_factor", bsls.data_scaling_factors_);

    write_labeled_value("bins_for_numeric_integration", bsls.num_bins_);
    
    write_scores_and_sigmas_from_negative(bsls.negative_scores_and_sigmas_);
    
    if (bsls.save_distributions_) {
      const double downsample_factor = 2.0;
      write_all_distributions(bsls, downsample_factor);
    }
  }
  
  void write_scores_and_sigmas_from_negative(const std::array<std::vector< std::pair<LogDouble, double> >, N>  & negative_scores_and_sigmas) {
    rapidxml::xml_node<> * scores_and_sigmas = BaseReport::create_node_ptr("sigma_estimation_results");
    
    // for each dimension, output the sigma choice
    for (std::size_t k=0; k<N; ++k) {
      rapidxml::xml_node<> * scores_and_sigmas_for_k = BaseReport::create_node_ptr("maximum_likelihood_for_sigma");
      scores_and_sigmas_for_k->append_attribute( BaseReport::create_attribute_ptr("dimension", k) );

      for (const std::pair<LogDouble, double> & point : negative_scores_and_sigmas[k]) {
	rapidxml::xml_node<> * score_and_sigma = BaseReport::create_node_ptr("single_dimension_sigma_likelihood_point");
	score_and_sigma->append_attribute( BaseReport::create_attribute_ptr("sigma", point.second) );
	score_and_sigma->append_attribute( BaseReport::create_attribute_ptr("unnormalized_log_likelihood", point.first.get_log_absolute_value()) );

	scores_and_sigmas_for_k->append_node(score_and_sigma);
      }
      scores_and_sigmas->append_node(scores_and_sigmas_for_k);
    }
    BaseReport::root_->append_node(scores_and_sigmas);
  }

  // moved outside of class declaration (to cpp) so that
  // SmoothedLikelihoodSimilarity<N> is available (prevents circular
  // include)
  void write_all_distributions(const BaseSmoothedLikelihoodSimilarity<N> & similarity_engine, double downsample_factor);

  rapidxml::xml_node<> *  create_distribution_pair_subtree(const PDF<LogDouble, N> & pdf0, const PDF<LogDouble, N> & pdf1, int index) {
    rapidxml::xml_node<> * distribution_pair = BaseReport::create_node_ptr("distribution_pair");
    distribution_pair->append_attribute( BaseReport::create_attribute_ptr("index", index) );

    rapidxml::xml_node<> * pdf_subtree0 = create_subtree_for_distribution(pdf0);
    pdf_subtree0->append_attribute( BaseReport::create_attribute_ptr("column_number", 0) );

    rapidxml::xml_node<> * pdf_subtree1 = create_subtree_for_distribution(pdf1);
    pdf_subtree1->append_attribute( BaseReport::create_attribute_ptr("column_number", 1) );

    distribution_pair->append_node( pdf_subtree0 );
    distribution_pair->append_node( pdf_subtree1 );
    return distribution_pair;
  }

private:
  rapidxml::xml_node<> * create_subtree_for_distribution_support(const NumericRange<N> & nr) {
    // note: for efficiency, stores values as text (i.e. element)
    // instead of making a node for each
    std::ostringstream ost;
    for (const std::array<double, N> x : nr)
      ost << x << std::endl;

    return BaseReport::create_node_ptr_and_text("pdf_support", ost.str());
  }
  rapidxml::xml_node<> * create_subtree_for_distribution(const PDF<LogDouble, N> & pdf) {
    // note: for efficiency, stores density as text (i.e. element)
    // instead of making a node for each. also, store without numeric
    // range inputs for each output (i.e. support). support for all
    // pdfs is output once above.
    std::ostringstream ost;
    for (LogDouble x : pdf.get_values())
      // fixme: debug
      ost << x.get_log_absolute_value() << std::endl;
    
    // note: this line can be used instead of the one above to output
    // the non-log scaled distribution
      //      ost << double(x) << std::endl;

    return BaseReport::create_node_ptr_and_text("log_pdf", ost.str());
  }
};

#include "SmoothedLikelihoodSimilarityReport.cpp"

#endif
