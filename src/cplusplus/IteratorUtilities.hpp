#ifndef _ITERATORUTILITIES_HPP
#define _ITERATORUTILITIES_HPP

#include <list>
#include <tuple>
#include <iterator>
#include <algorithm>

#include "StandardExceptions.hpp"
#include "SetOperations.hpp"

template <typename ...T>
std::list<std::tuple<T...> > minimum_size_zip(std::list<T>... lst)
{
  std::list<std::tuple<T...> >  result;
  for (int i = 0, e = std::min({lst.size()...}); i != e; i++) {
    result.emplace_back(std::move(lst.front())...);
#pragma GCC diagnostic ignored "-Wunused-variable"
    // suppress the warning; this variable needs to be created in
    // order to pop from the argument lists
    struct { int dummy; } pops[] = { {(lst.pop_front(), 0)}... };
#pragma GCC diagnostic pop
  }
  return result;
}

template <typename ...T>
std::list<std::tuple<T...> > equal_size_zip(std::list<T>... lst)
{
  size_t min_length = std::min({lst.size()...});
  size_t max_length = std::max({lst.size()...});
  if (min_length != max_length)
    throw SizeMismatchException("equal_size_zip requires all lists to be of equal size");

  return minimum_size_zip(lst...);
}

template <template <typename, typename> class ContainerA, typename AllocatorA, template <typename, typename> class ContainerB, typename AllocatorB, typename T>
ContainerA<T, AllocatorB> flatten(const ContainerA<ContainerB<T, AllocatorB>, AllocatorA> & container_of_containers) {
  ContainerA<T, AllocatorB> result;
  for (const ContainerB<T, AllocatorB> & container : container_of_containers)
    for (const T & value : container)
      result.push_back(value);
  return result;
}

template <template <typename, typename, typename> class ContainerA, typename CompareA, typename AllocatorA, template <typename, typename, typename> class ContainerB, typename CompareB, typename AllocatorB, typename T>
ContainerB<T, CompareB, AllocatorB> flatten(const ContainerA<ContainerB<T, CompareB, AllocatorB>, CompareA, AllocatorA> & container_of_containers) {
  ContainerB<T, CompareB, AllocatorB> result;
  for (const ContainerB<T, CompareB, AllocatorB> & container : container_of_containers)
    insert_all(result, container);
  return result;
}

/* fixme: complete and use for more concise code */

// template <typename T...>
// class zip_iter {
// public:
//   zip_iter(std::iterator<T>... iters):
//     iters_(iters) { }

//   const zip_iter & operator ++() {
//     int garbage[] = { (std::get<S...>(iters_).begin()..., 0) };
//   }
//   std::iterator<T>... iters_;
// };

// note: the following are to be improved for generality with other
// containers (e.g. vector). currently only works with std::list (I
// belive because of pop_front), but the compiler error message is
// uninformative because it reports a bug in GCC. 
/*
template <typename ...LIST>
std::list<std::tuple<typename LIST::value_type...> > minimum_size_zip(LIST... lst)
{
  std::list<std::tuple<typename LIST::value_type...> >  result;
  for (int i = 0, e = std::min({lst.size()...}); i != e; i++) {
    result.emplace_back(std::move(lst.front())...);
    struct { int dummy; } pops[] = { (lst.pop_front(), 0)... };
  }
  return result;
}

template <typename ...LIST>
std::list<std::tuple<typename LIST::value_type...> > equal_size_zip(LIST... lst)
{
  size_t min_length = std::min({lst.size()...});
  size_t max_length = std::max({lst.size()...});
  
  //  assert(min_length == max_length);
  if (min_length != max_length)
    throw SizeMismatchException("equal_size_zipped arguments must be the same size");

  return minimum_size_zip(lst...);
}
*/

#include "IteratorUtilities.cpp"

#endif
