#ifndef _PLAINTEXTPROTEINIDGRAPHLOADER_H
#define _PLAINTEXTPROTEINIDGRAPHLOADER_H

#include "ProteinIDGraphLoader.hpp"

class PlainTextProteinIDGraphLoader : public ProteinIDGraphLoader
{
protected:
  void read_and_add_charge_prior(ProteinIDGraph & pg, std::ifstream & fin) const;
  void add_protein_peptide_and_edge(ProteinIDGraph & pg, const ProteinNode & protein_node, const PeptideNode & peptide_node) const;
  void add_peptide_psm_and_edge(ProteinIDGraph & pg, const PeptideNode & peptide_node, const PSMNode & psm_node) const;
  virtual void load_from_file(ProteinIDGraph & pg, const FileAndFormat & faf) const;
};

#endif
