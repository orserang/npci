template <typename DOUBLEVAL, typename OBJECTVAL>
DOUBLEVAL GaussianKernelFunction<DOUBLEVAL, OBJECTVAL>::operator()(const OBJECTVAL & lhs, const OBJECTVAL & rhs) const
{
  DOUBLEVAL unscaled_exponent = -KernelFunction<DOUBLEVAL, OBJECTVAL>::squared_dist(lhs / sigma_, rhs / sigma_);
  DOUBLEVAL sigma_prod(1.0);
  for (double sigma_element : sigma_)
    sigma_prod *= DOUBLEVAL(sigma_element);
  // note: does not have an integral of 1.0 (i.e. not a standard
  // normal); losing the sigma in the demoninator 
  return exp(unscaled_exponent / DOUBLEVAL(2.0));

  // standard normal version 1:
  //  return exp(unscaled_exponent / DOUBLEVAL(2.0)) / (DOUBLEVAL(sqrt(2*M_PI)) * sigma_prod);
}
