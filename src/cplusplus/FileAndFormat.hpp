#ifndef _FILEANDFORMAT_H
#define _FILEANDFORMAT_H

#include <string>

class FileAndFormat {
private:
  std::string file, format;
public:
  FileAndFormat(const std::string & file_param = "", const std::string & format_param = "")
  {
    file = file_param;
    format = format_param;
  }
  const std::string & get_file() const { return file; }
  const std::string & get_format() const { return format; }
  bool has_format() const { return format != ""; }
};

class HasFileAndFormatMixin {
  void set_file_and_format(const FileAndFormat & faf_param) {
    faf = faf_param;
  }
  const FileAndFormat & get_file_and_format() const {
    return faf;
  }
private:
  FileAndFormat faf;
};

#endif
