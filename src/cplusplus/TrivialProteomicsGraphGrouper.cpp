template <typename PROTEOMICSGRAPH>
std::list<std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> > TrivialProteomicsGraphGrouper<PROTEOMICSGRAPH>::get_protein_groups(const PROTEOMICSGRAPH & pg) const {
  std::list<std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> > result;
  // add a unique group for each protein
  for (const typename PROTEOMICSGRAPH::ProteinNodePtr prot_node_ptr : pg.get_protein_node_ptr_set() )
    {
      typename std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> prot_ptr_singleton_set( { prot_node_ptr } );
      result.push_back(prot_ptr_singleton_set);
    }
  return result;
}

