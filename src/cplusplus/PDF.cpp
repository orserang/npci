template <typename DOUBLEVAL, std::size_t N>
PDF<DOUBLEVAL, N>::PDF(const NumericGridFunction<DOUBLEVAL, N> & ngf):
  NonnegativeGridFunction<DOUBLEVAL, N>(ngf) {
  // scale so that the definite_integral is 1.0
  DOUBLEVAL total = NumericGridFunction<DOUBLEVAL, N>::definite_integral();
  for (DOUBLEVAL & f: PDF::values_)
    f /= total;
}

