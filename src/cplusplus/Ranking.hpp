#ifndef _RANKING_H
#define _RANKING_H

#include <map>
#include <list>
#include <set>
#include "StringOperations.hpp"
#include "SetOperations.hpp"

template <typename K>
class Ranking
{
public:
  typedef K ScoredObjectType;
  // avoids a redundant definition of a lengthy type in GroupRanking
  typedef std::map<double, std::set<K> > DoubleToObjectSet;

  Ranking(const std::string & label_param = ""):
    label_(label_param)
  { }

  Ranking(const DoubleToObjectSet & score_to_scored_object_set_param, const std::string & label_param = ""):
    score_to_scored_object_set(score_to_scored_object_set_param),
    label_(label_param)
  {
    create_best_sets_first_from_map();
  }

  size_t size() const { return best_sets_first.size(); }
  const std::list<std::set<K> > & get_best_sets_first() const { return best_sets_first; }
  const DoubleToObjectSet & get_double_to_scored_object_set_map() const { return score_to_scored_object_set; }

  const std::string & get_label() const {
    return label_;
  }

  Ranking<K> downsampled(int downsample_factor, int upper_index_limit) {
    // construct and return new with downsampling
    DoubleToObjectSet downsampled_map;

    int count = 0;
    bool added_this_iteration = false;
    std::set<K> cumulative;
    double lowest_score_so_far;
    for (auto iter = score_to_scored_object_set.rbegin(); iter != score_to_scored_object_set.rend(); ++iter, ++count) {
      added_this_iteration = false;

      // setting upper_index_limit < 0 turns off the limit
      if (upper_index_limit > 0 && count > upper_index_limit)
	break;

      const std::pair<double, std::set<K> > & point = *iter;
      insert_all(cumulative, point.second);
      lowest_score_so_far = point.first;

      if (count % downsample_factor == 0) {
	downsampled_map[point.first] = cumulative;
	added_this_iteration = true;

	// clear cumulative
	cumulative.clear();
      }
    }

    // add any remaining
    if (!added_this_iteration && size() > 0)
      downsampled_map[lowest_score_so_far] = cumulative;

    return Ranking<K>(downsampled_map, label_ + " (downsampled factor=" + to_string(downsample_factor) + ", upper_index_limit=" + to_string(upper_index_limit) + ")");
  }

protected:
  std::list<std::set<K> > best_sets_first;
  DoubleToObjectSet score_to_scored_object_set;

  std::string label_;

  void create_best_sets_first_from_map();
};

#include "Ranking.cpp"

#endif
