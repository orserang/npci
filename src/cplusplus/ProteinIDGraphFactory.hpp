#ifndef _PROTEINIDGRAPHFACTORY_H
#define _PROTEINIDGRAPHFACTORY_H

#include "ProteinIDGraphLoaderRegistry.hpp"
#include "Singleton.hpp"
#include "FileAndFormat.hpp"

template <typename PROTEOMICS_GRAPH_TYPE>
class ProteinIDGraphFactory {
public:
  static std::unique_ptr<PROTEOMICS_GRAPH_TYPE> create_ungrouped_proteomics_graph(const FileAndFormat & faf);
};

#include "ProteinIDGraphFactory.cpp"

#endif
