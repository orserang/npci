template <typename K>
bool TargetDecoy<K>::contains_target(const std::set<K> & group) const {
  auto td_pair = number_decoy_and_target_matches( group );
  return td_pair.second > 0;
}

template <typename K>
bool TargetDecoy<K>::contains_decoy_and_no_targets(const std::set<K> & group) const {
  auto td_pair = number_decoy_and_target_matches( group );
  return td_pair.first > 0 && td_pair.second == 0;
}

template <typename K>
std::vector<std::pair<int, int> > TargetDecoy<K>::roc(const GroupRanking<K> & gr) const {
  std::vector<std::pair<int, int> > result{ std::make_pair(0, 0) };
  const std::list<std::set< std::set<K> > > & partial_group_ordering = gr.get_best_sets_first();
  int target_group_count = 0, decoy_group_count = 0;
  for ( const std::set< std::set<K> > & group_set : partial_group_ordering ) {
      std::pair<int,int> target_change_and_decoy_group_change = number_target_and_decoy_group_matches(group_set);
      target_group_count += target_change_and_decoy_group_change.first;
      decoy_group_count += target_change_and_decoy_group_change.second;
      
      // always push the cumulative point to the result, even if it
      // doesn't result in a change (to make sure indices align with
      // likelihood series indices)
      result.emplace_back( std::pair<int,int>(target_group_count, decoy_group_count) );
  }

  return result;
}

template <typename K>
std::vector<std::pair<double, int> > TargetDecoy<K>::fdr_vs_targets(const GroupRanking<K> & gr) const {
  std::vector<std::pair<double, int> > result;
  std::vector<std::pair<int,int> > standard_roc = roc(gr);
    
  for (const std::pair<int,int> & standard_roc_point: standard_roc)
    result.push_back( std::pair<double, int>(get_fdr_from_roc_point(standard_roc_point), standard_roc_point.first) );
  return result;
}

template <typename K>
std::vector<std::pair<double, int> > TargetDecoy<K>::q_value_vs_targets(const GroupRanking<K> & gr) const {
  std::vector<std::pair<double, int> > fdr_result = fdr_vs_targets(gr);
  std::vector<std::pair<double, int> > result;
    
  double min_fdr_for_superset = 1.0;
  for (auto iter = fdr_result.rbegin(); iter != fdr_result.rend(); ++iter ) {
    min_fdr_for_superset = std::min(min_fdr_for_superset, iter->first);
    result.push_back( std::pair<double, int>( min_fdr_for_superset, iter->second ) );
  }
  // reverse the result (it's been processed backwards)
  std::reverse(result.begin(), result.end());
 
  return result;
}

template <typename K>
double TargetDecoy<K>::get_fdr_from_roc_point(const std::pair<int, int> & target_and_decoy_count) {
  // return 0.0 as FDR when neither targets nor decoys are identified
  if ( target_and_decoy_count == std::make_pair(0,0) )
    return 0.0;
  return std::min(target_and_decoy_count.second / double(target_and_decoy_count.first), 1.0);
}

template <typename K>
std::pair<int,int> TargetDecoy<K>::number_target_and_decoy_matches(const std::set<K> & obj_set) const {
  return std::pair<int, int>( target_matches(obj_set).size(), decoy_matches(obj_set).size() );
}

template <typename K>
std::pair<int, int> TargetDecoy<K>::number_target_and_decoy_matches_from_single_group(const std::set<K> & group) const {
  std::pair<int,int> target_and_decoy_matches = number_target_and_decoy_matches(group);
  // when the group contains any targets, count as a single target
  if (target_and_decoy_matches.first > 0)
    return std::make_pair(1,0);
  // when the group contains no targets and some decoys, count as a single decoy
  else if (target_and_decoy_matches.second > 0)
    return std::make_pair(0,1);
  // when the group contains neither targets nor decoys, return (0,0)
  return std::make_pair(0, 0);
}

template <typename K>
std::pair<int, int> TargetDecoy<K>::number_target_and_decoy_group_matches(const std::set<std::set<K> > & group_set) const {
  int target_groups = 0, decoy_groups = 0;
  for (const std::set<K> & group : group_set) {
    std::pair<int, int> target_and_decoy_matches = number_target_and_decoy_matches_from_single_group(group);
    target_groups += target_and_decoy_matches.first;
    decoy_groups += target_and_decoy_matches.second;
    }
  return std::pair<int, int> (target_groups, decoy_groups);
}

template <typename K>
std::set<K> TargetDecoy<K>::target_matches(const std::set<K> & key_set) const {
  std::set<K> result;
  for (const K & key : key_set)
    if (is_target(key))
      result.insert(key);
  return result;
}

template <typename K>
std::set<K> TargetDecoy<K>::decoy_matches(const std::set<K> & key_set) const {
  std::set<K> result;
  for (const K & key : key_set)
    if (is_decoy(key))
      result.insert(key);
  return result;
}

