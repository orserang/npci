#ifndef _KRONICKERHISTOGRAMCOLLECTION_H
#define _KRONICKERHISTOGRAMCOLLECTION_H

#include <iterator>
#include <array>

#include "KroneckerHistogram.hpp"
#include "NumericRange.hpp"

// Note: This class exists in order to save space and not store all
// KroneckerHistogram objects for remaining distributions; however,
// when enough memory exists, it would be faster to simply fill a
// vector with the KroneckerHistogram objects and then use iterators
// from that vector. The vector would be more efficient because it
// would not need to compute the KroneckerHistogram series for every
// sigma value tried.

template <std::size_t N> class KroneckerHistogramCollection;

template <std::size_t N>
class KroneckerHistogramCollectionIterator : public std::iterator< KroneckerHistogram<std::array<double, N> >, std::forward_iterator_tag > {
public:
  
  template <std::size_t> friend class KroneckerHistogramCollection;
  
  KroneckerHistogramCollectionIterator & operator++() {
    if (*this != khc_ptr_->end()) {
      state_ += *iter_;
      ++iter_;
    }
    return *this;
  }

  KroneckerHistogramCollectionIterator & operator+=(int count) {
    for (int k=0; k<count; ++k)
      (*this)++;
    return *this;
  }
  
  KroneckerHistogramCollectionIterator operator++(int) {
    KroneckerHistogramCollectionIterator tmp(*this);
    ++*this;
    return tmp;
  }
  
  KroneckerHistogram<std::array<double, N> > const& operator*() const { return state_; }
  
  KroneckerHistogram<std::array<double, N> > const* operator->() const { return & state_; }
  
  bool operator ==(const KroneckerHistogramCollectionIterator & rhs) const {
    return khc_ptr_ == rhs.khc_ptr_ && iter_ == rhs.iter_;
  }
  bool operator !=(const KroneckerHistogramCollectionIterator & rhs) const {
    return ! (*this == rhs);
  }
private:
  KroneckerHistogramCollectionIterator(const KroneckerHistogramCollection<N> & khc, const typename std::list<KroneckerHistogram<std::array<double, N> > >::const_iterator & iter) :
    khc_ptr_( & khc ), iter_(iter), state_(khc.get_initial()) { }

  const KroneckerHistogramCollection<N> * khc_ptr_;
  typename std::list<KroneckerHistogram<std::array<double, N> > >::const_iterator iter_;
  KroneckerHistogram<std::array<double, N> > state_;
};

template <std::size_t N>
class KroneckerHistogramCollection {
public:
  KroneckerHistogramCollection() { }

  // efficient constructor (values given are described using sparsity)
  KroneckerHistogramCollection(const KroneckerHistogram<std::array<double, N> > & initial, const std::list<KroneckerHistogram<std::array<double, N> > > & deltas) :
    initial_(initial), deltas_(deltas) {
    // add an empty change to end of deltas_ so that the iterator goes
    // over n+1 items (i.e. include one where no changes have been applied)
    deltas_.push_back( KroneckerHistogram<std::array<double, N> >() );
  }  

  // inefficient constructor (values given do not exploit sparsity)
  KroneckerHistogramCollection(const std::list<std::vector<std::array<double, N> > > & collection) {
    if (collection.size() == 0)
      throw OutOfBoundsException("KroneckerHistogramCollection must contain at least one value (to specify initial)");

    initial_ = *collection.begin();

    KroneckerHistogram<std::array<double, N> > kh_old = initial_;
    for (auto iter = std::next(collection.cbegin()); iter != collection.cend(); ++iter) {
      KroneckerHistogram<std::array<double, N> > kh_new(*iter);
      deltas_.push_back(kh_new - kh_old);
      kh_old = kh_new;
    }
    
    // add an empty change to end of deltas_ so that the iterator goes
    // over n+1 items (i.e. include one where no changes have been applied)
    deltas_.push_back( KroneckerHistogram<std::array<double, N> >() );
  }

  template <std::size_t> friend class KroneckerHistogramCollectionIterator;
  typedef KroneckerHistogramCollectionIterator<N> const_iterator;

  std::size_t size() const {
    return deltas_.size();
  }
  
  // iterate along histograms produced by introducing the changes from deltas
  const_iterator begin() const;
  const_iterator end() const;

  const KroneckerHistogram<std::array<double, N> > & get_initial() const { return initial_; }
  const std::list<KroneckerHistogram<std::array<double, N> > > & get_deltas() const { return deltas_; }

  typename std::list<KroneckerHistogram<std::array<double, N> > >::const_iterator begin_deltas() const { return deltas_.cbegin(); }
  typename std::list<KroneckerHistogram<std::array<double, N> > >::const_iterator end_deltas() const { return deltas_.cend(); }
  
  KroneckerHistogramCollection<1> get_histogram_collection_using_column(std::size_t col) const {
    // don't use the KroneckerHistogramCollection constructor because
    // it will add an unwanted empty histogram to deltas_

    KroneckerHistogram<std::array<double, 1> > initial_for_column = get_histogram_using_column(initial_, col);
    std::list<KroneckerHistogram<std::array<double, 1> > > deltas_for_column;
    for (const KroneckerHistogram<std::array<double, N> > & kh : deltas_)
      deltas_for_column.push_back( get_histogram_using_column(kh, col) );

    return KroneckerHistogramCollection<1>(initial_for_column, deltas_for_column);
  }

private:
  // effectively marginalizes out the other columns
  KroneckerHistogram<std::array<double, 1> > get_histogram_using_column(const KroneckerHistogram<std::array<double, N> > & kh, std::size_t col) const {
    KroneckerHistogram<std::array<double, 1> > result;
    for (const std::pair<std::array<double, N>, int > & point_and_count : kh)
      // make the value from the column an array of size 1
      result.add_value( std::array<double, 1>{{ point_and_count.first[col] }}, point_and_count.second);
    return result;
  }

  KroneckerHistogram<std::array<double, N> > initial_;
  std::list<KroneckerHistogram<std::array<double, N> > > deltas_;
};
  
template <std::size_t N>
typename KroneckerHistogramCollection<N>::const_iterator KroneckerHistogramCollection<N>::begin() const {
  return KroneckerHistogramCollection<N>::const_iterator(*this, deltas_.begin());
}

template <std::size_t N>
typename KroneckerHistogramCollection<N>::const_iterator KroneckerHistogramCollection<N>::end() const {
  return KroneckerHistogramCollection<N>::const_iterator(*this, deltas_.end());
}

template <std::size_t N>
std::array<double, N> get_lower_from_kronicker_histogram_collection(const KroneckerHistogramCollection<N> & khc) {
  std::list<std::array<double, N> > lower_for_each;
  for (const KroneckerHistogram<std::array<double, N> > & kh : khc)
    lower_for_each.push_back( get_lower_from_collection( keys(kh) ) );
  return get_lower_from_collection(lower_for_each);
}

template <std::size_t N>
std::array<double, N> get_upper_from_kronicker_histogram_collection(const KroneckerHistogramCollection<N> & khc) {
  std::list<std::array<double, N> > upper_for_each;
  for (const KroneckerHistogram<std::array<double, N> > & kh : khc)
    upper_for_each.push_back( get_upper_from_collection( keys(kh) ) );
  return get_upper_from_collection(upper_for_each);
}

#endif

