#ifndef _PROTEINPILOTPROTEINIDGRAPHLOADER_H
#define _PROTEINPILOTPROTEINIDGRAPHLOADER_H

#include "ProteinIDGraphLoader.hpp"
#include "TabSeparatedFile.hpp"

class ProteinPilotProteinIDGraphLoader : public ProteinIDGraphLoader
{
protected:
  virtual void load_from_file(ProteinIDGraph & pg, const FileAndFormat & faf) const;
};

#endif
