#ifndef _BASEREPORT_H
#define _BASEREPORT_H

#include <string>

#include "Singleton.hpp"

#include "rapidxml-1.13/rapidxml.hpp"
#include "rapidxml-1.13/rapidxml_print.hpp"

class BaseReport {
public:
  BaseReport(const char * sub_report_name) {
    //    std::cout << sub_report_name << std::endl;
    root_ = create_node_ptr(sub_report_name);
  }

  virtual ~BaseReport() { }

  // commonly performed tasks
  template <typename T>
  void write_labeled_value(const std::string & name, const T & value) {
    root_->append_attribute(create_attribute_ptr(name, value));
  }

  void write_all_likelihoods(const std::list<std::vector<LogDouble> > & all_likelihoods) {
    rapidxml::xml_node<> *likelihood_subtree = create_node_ptr("likelihood_series_collection");

    int ranking_number = 0;
    for ( const std::vector<LogDouble> & likelihood_series : all_likelihoods ) {
      rapidxml::xml_node<> *likelihood_subtree_for_ranking = create_node_ptr("likelihood_series");
      
      likelihood_subtree_for_ranking->append_attribute( create_attribute_ptr("ranking_number", ranking_number ) );

      int max_likelihood_index = -1;
      for (std::size_t i = 0; i<likelihood_series.size(); ++i) {
	LogDouble likelihood_val  = likelihood_series[i];

	if (max_likelihood_index == -1 || likelihood_val > likelihood_series[max_likelihood_index])
	  max_likelihood_index = i;

	rapidxml::xml_node<> * point_ptr = create_node_ptr("point");
	point_ptr->append_attribute( create_attribute_ptr("index", i ) );
	point_ptr->append_attribute( create_attribute_ptr("likelihood", double(likelihood_val) ) );
	point_ptr->append_attribute( create_attribute_ptr("log_likelihood", likelihood_val.get_log_absolute_value() ) );
	likelihood_subtree_for_ranking->append_node(point_ptr);
      }

      // add max likelihood (and index)
      likelihood_subtree_for_ranking->append_attribute( create_attribute_ptr("maximum_likelihood_index", max_likelihood_index) );      
      likelihood_subtree_for_ranking->append_attribute( create_attribute_ptr("maximum_likelihood", double(likelihood_series[max_likelihood_index]) ) );

      likelihood_subtree->append_node(likelihood_subtree_for_ranking);
      ++ranking_number;
    }

    // show the index with the highest likelihood overall (?)

    root_->append_node(likelihood_subtree);
  }

  // to allow tree-like behavior
  void write_sub_report(const BaseReport & report) {
    root_->append_node(report.get_tree());
  }

  // copies in every node and attribute from the report
  void copy_all_from_report(const BaseReport & report) {
    
    rapidxml::xml_node<> * rhs_root = report.get_tree();

    // copy nodes
    for ( rapidxml::xml_node<> * child = rhs_root->first_node(); child != 0; child = child->next_sibling() ) {
      root_->append_node(clone_node_ptr(child));
    }

    // copy attributes
    for ( rapidxml::xml_attribute<> * attribute = rhs_root->first_attribute(); attribute != 0; attribute = attribute->next_attribute() )
      root_->append_attribute(create_attribute_ptr(attribute->name(), attribute->value()));
  }

  rapidxml::xml_node<> * get_tree() const {
    return root_;
  }

  friend std::ostream & operator <<(std::ostream & os, const BaseReport & report) {
    os << *report.root_;
    return os;
  }

protected:
  // general xml tree utilities: keep in .hpp file for simplicity
  // compiling (otherwise you'll need to deliberately call
  // .template. these functions allow automatic type deduction via
  // to_string, and allow int, double, etc. to be passed in easily.
  template <typename T>
  rapidxml::xml_attribute<> * create_attribute_ptr(const std::string & name, T val) {
    rapidxml::xml_document<> & doc = Singleton<rapidxml::xml_document<> >::get_instance();
    return doc.allocate_attribute( doc.allocate_string(name.c_str()), doc.allocate_string(to_string<T>(val).c_str()) );
  }

  rapidxml::xml_node<> * create_node_ptr(const char * str) {
    return Singleton<rapidxml::xml_document<> >::get_instance().allocate_node(rapidxml::node_element, str);
  }
  template <typename T>
  rapidxml::xml_node<> * create_node_ptr_and_text(const char * str, T val) {
    rapidxml::xml_document<> & doc = Singleton<rapidxml::xml_document<> >::get_instance();
    return Singleton<rapidxml::xml_document<> >::get_instance().allocate_node(rapidxml::node_element, str, doc.allocate_string(to_string<T>(val).c_str()) );
  }

  rapidxml::xml_node<> * clone_node_ptr(rapidxml::xml_node<> * n) {
    return Singleton<rapidxml::xml_document<> >::get_instance().clone_node(n);
  }

  rapidxml::xml_node<> * root_;
};

#endif
