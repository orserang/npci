template <typename T>
void Registry<T>::register_name(const std::string & name, std::shared_ptr<T> obj_ptr)
{
  if (name == "")
    throw RegistryException("Cannot register empty string \"\" as name");
  if ( contains(name) )
    throw RegistryException("Cannot re-register \"" + name + "\"");
  name_to_object_ptr[name] = obj_ptr;
}

template <typename T>
const std::shared_ptr<T> & Registry<T>::lookup_name(const std::string & name) const
{
  if ( not contains(name) )
    throw Registry<T>::RegistryException("Registry does not contain \"" + name + "\"");
  return name_to_object_ptr.find(name)->second;
}

