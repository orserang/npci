#ifndef _PROTEINIDGRAPHLOADERREGISTRY_H
#define _PROTEINIDGRAPHLOADERREGISTRY_H

#include "ProteinIDGraphLoader.hpp"
#include "Registry.hpp"
#include "FileAndFormat.hpp"

#include "PlainTextProteinIDGraphLoader.hpp"
#include "ProteinPilotProteinIDGraphLoader.hpp"

class ProteinIDGraphLoaderRegistry : public Registry<ProteinIDGraphLoader>
{
public:
  // enforce usage through singleton
  ProteinIDGraphLoaderRegistry();
  void load_from_file(ProteinIDGraph & pg, const FileAndFormat & faf) const;

  struct AmbiguousFormatException : public StandardException {
    AmbiguousFormatException(const std::string & str) throw():
      StandardException(str) { }
  };

private:

  void load_from_file_with_format(ProteinIDGraph & pg, const FileAndFormat & faf) const;
  const std::string & load_from_file_without_format(ProteinIDGraph & pg, const FileAndFormat & faf) const;
};

#endif
