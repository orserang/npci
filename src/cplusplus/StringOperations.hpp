#ifndef _STRINGOPERATIONS_H
#define _STRINGOPERATIONS_H

#include <string>
#include <vector>
#include <sstream>

#include "StandardExceptions.hpp"

template <typename T>
std::string to_string(T x)
{
  std::ostringstream sout;
  sout.precision(10);
  sout << x;
  return sout.str();
}

template <typename T>
T from_string(const std::string & str)
{
  std::istringstream sin(str);
  T x;
  sin >> x;
  if ( sin.fail() || not sin.eof() )
    throw FormatException(std::string(str + " was not formatted for expected type"));
  return x;
}

std::string operator +(const std::string & lhs, char*rhs);
std::string operator +(char*lhs, const std::string & rhs);

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
std::vector<std::string> split(const std::string &s, char delim);

std::vector<std::string> split_whitespace(const std::string & s);
std::string first_word(const std::string & s);

bool starts_with(const std::string & a, const std::string & prefix);
bool ends_with(const std::string & a, const std::string & suffix);

void replace(std::string & str, const std::string & from, const std::string & to);


#endif
