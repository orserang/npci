#include "PlainTextProteinIDGraphLoader.hpp"

void PlainTextProteinIDGraphLoader::read_and_add_charge_prior(ProteinIDGraph & pg, std::ifstream & fin) const
{
  // for prior probabilities
  int charge;
  double prior;
  fin >> charge >> prior;
  // here is where you'd make use of the charge prior
}

void PlainTextProteinIDGraphLoader::add_protein_peptide_and_edge(ProteinIDGraph & pg, const ProteinNode & protein_node, const PeptideNode & peptide_node) const
{
  pg.add_node(protein_node);
  pg.add_node(peptide_node);
  pg.add_edge_between_keys(protein_node.get_key(), peptide_node.get_key());
}

void PlainTextProteinIDGraphLoader::add_peptide_psm_and_edge(ProteinIDGraph & pg, const PeptideNode & peptide_node, const PSMNode & psm_node) const
{
  pg.add_node(peptide_node);
  
  if ( ! isnan(psm_node.get_evidence()[0]) ) {
    pg.add_node(psm_node);
    pg.add_edge_between_keys(peptide_node.get_key(), psm_node.get_key());
  }
}

void PlainTextProteinIDGraphLoader::load_from_file(ProteinIDGraph & pg, const FileAndFormat & faf) const
{
  // read using a finite state machine
  std::ifstream fin(faf.get_file());

  // the current instruction
  char instr;
  // the valid next instructions to read
  std::set<char> valid_next_states = {'d','e','#'};

  PeptideNode peptide_node(ProteomicsKey(""));
  PSMNode psm_node(ProteomicsKey(""));
  int num_spectra = 0;

  int line_number = 1;

  fin >> instr;
  do
    {
      if ( valid_next_states.count(instr) == 0 )
	{
	  std::string garbage;
	  getline(fin, garbage);
	  // change the endl character to space
	  garbage[garbage.size()-1] = ' ';

	  throw FormatException(faf.get_file() + std::string(" on line number ") + to_string(line_number) + std::string(" (line was \"") + to_string(instr) + std::string(" ") + garbage  + std::string("\")") );
	}
      if ( instr == 'd' )
	{
	  read_and_add_charge_prior(pg, fin);
	  // does not change, so no need to update
	  //	    std::set<char> valid_next_states = {'d','e','#'};
	}
      else if ( instr == 'e' )
	{
	  // if a new peptide block has started while waiting for a
	  // score, add an unscored psm node
	  if ( valid_next_states.count('p') > 0 )
	    {
	      add_peptide_psm_and_edge(pg, peptide_node, psm_node);
	    }
	    
	  // start a new psm node and a new peptide node
	  std::string peptide_seq;
	  fin >> peptide_seq;
	  peptide_node = PeptideNode(peptide_seq);
	  psm_node = PSMNode("spectrum_" + to_string(num_spectra));
	  num_spectra++;

	  valid_next_states = std::set<char>{'c','r','#'};
	}
      else if ( instr == 'c' )
	{
	  fin >> psm_node.charge_state;

	  valid_next_states = std::set<char>{'r','#'};
	}
      else if ( instr == 'r' )
	{
	  std::string protein_accession;
	  fin >> protein_accession;
	  ProteinNode protein_node(protein_accession);
	  add_protein_peptide_and_edge(pg, protein_node, peptide_node);

	  valid_next_states = std::set<char>{'e','r','p','#'};
	}
      else if ( instr == 'p' )
	{
	  double score;
	  fin >> score;
	  psm_node.set_evidence(std::array<double,1>{{score}});
	  add_peptide_psm_and_edge(pg, peptide_node, psm_node);
	  valid_next_states = std::set<char>{'e','#'};
	}
      else if ( instr == '#' )
	{
	  // comment line
	  std::string garbage;
	  getline(fin, garbage);
	}
      // this FSM should have no other cases, so no else block is
      // necessary

      line_number++;
    } while ( fin >> instr );

  if ( instr != 'p' )
    {
      // if the last entry isn't scored, add it
      add_peptide_psm_and_edge(pg, peptide_node, psm_node);
    }
}
