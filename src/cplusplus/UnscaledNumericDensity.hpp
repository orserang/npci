#ifndef _UNSCALEDNUMERICDENSITY_H
#define _UNSCALEDNUMERICDENSITY_H

#include "NumericGridFunction.hpp"
#include "KroneckerHistogram.hpp"
#include "GaussianKernelFunction.hpp"

// inherits from NumericGridFunction (not NonnegativeGridFunction) to
// allow acceptance of negative-count KroneckerHistograms (which may
// be used to update while iterating over a
// UnscaledNumericDensityCollection)
template <typename DOUBLEVAL, std::size_t N>
class UnscaledNumericDensity : public NumericGridFunction<DOUBLEVAL, N>
{
protected:
public:
  UnscaledNumericDensity() {}
  UnscaledNumericDensity(const KroneckerHistogram<std::array<double, N> > & kh, const NumericRange<N> & nr_param, const std::array<double, N> & sigma);

  // for creating via KroneckerHistogram (used by IIDLikelihoodFunctor
  // to compute probability of a KroneckerHistogram given
  // SmoothedHistogram)
  static UnscaledNumericDensity<DOUBLEVAL, N> create_unsmoothed(const KroneckerHistogram<std::array<double, N> > & kh, const NumericRange<N> & nr_param);
};

template <typename T, std::size_t N>
std::array<T, N> operator -(const std::array<T, N> & lhs, const std::array<T, N> & rhs);

template <typename T, std::size_t N>
std::array<T, N> operator +(const std::array<T, N> & lhs, const std::array<T, N> & rhs);

template <typename T, std::size_t N>
std::array<T, N> operator /(const std::array<T, N> & lhs, const std::array<T, N> & rhs);

template <typename T, std::size_t N>
T operator *(const std::array<T, N> & lhs, const std::array<T, N> & rhs);

#include "UnscaledNumericDensity.cpp"

#endif
