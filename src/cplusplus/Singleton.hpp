#ifndef _SINGLETON_H
#define _SINGLETON_H

template <typename T>
class Singleton {
private:
  Singleton() {}
public:
  static T & get_instance() {
    static T instance;
    return instance;
  }
};

#endif
