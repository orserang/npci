#ifndef _PROTEOMICSGRAPHNODES_H
#define _PROTEOMICSGRAPHNODES_H

#include <string>
#include <limits>
#include <algorithm>
#include <array>

#include "CutoutGraph.hpp"

// all nodes share a key type; this makes it simple to contain them in
// the same DiGraph
typedef std::string ProteomicsKey;
typedef NoEvidenceCutoutGraphNode<ProteomicsKey> NENode;

class ProteinGroupNode : public NENode {
public:
  ProteinGroupNode(const ProteomicsKey & str): NENode(str)  { }
  virtual NENode * clone() const {
    return new ProteinGroupNode(*this);
  }
};

class ProteinNode : public NENode {
public:
  ProteinNode(const ProteomicsKey & str): NENode(str)  { }
  virtual NENode * clone() const {
    return new ProteinNode(*this);
  }
};
  
class PeptideNode : public NENode {
public:
  PeptideNode(const std::string & original_peptide_seq_param):
    NENode( to_unique_peptide_sequence(original_peptide_seq_param) ),
    original_peptide_seq(original_peptide_seq_param)
  { }
  static std::string to_unique_peptide_sequence(std::string pepSeq);
  virtual NENode * clone() const {
    return new PeptideNode(*this);
  }
protected:
  std::string original_peptide_seq;
};

#endif
