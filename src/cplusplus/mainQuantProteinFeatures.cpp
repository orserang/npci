#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <numeric>
 
#include "PrefixSuffixTargetDecoy.hpp"
#include "PlainTextRanking.hpp"
#include "GroupRanking.hpp"

#include "ProteinQuantificationGraph.hpp"
#include "ProteomicsGraphGrouperRegistry.hpp"

#include "SingleReplicateCutoutAnalysis.hpp"
#include "MultiReplicateCutoutAnalysis.hpp"

#include "DirichletSmoothedLikelihoodSimilarity.hpp"
#include "IIDSmoothedLikelihoodSimilarity.hpp"
#include "KullbackLeiblerSmoothedLikelihoodSimilarity.hpp"
#include "KolmogorovSmirnovLikelihoodSimilarity.hpp"

int main(int argc, char**argv)
{
  if (argc == 2 ) {

    std::cout.precision(10);

    std::cerr << "Loading Graph" << std::endl;
    std::shared_ptr<TMTFeatureGraph > pqg_ptr( new TMTFeatureGraph(argv[1], ',', {{"Quant 3 Raw", "Quant 1 Raw"}}, {{"Quant 3 Raw", "Quant 6 Raw"}} ) );

    ProteomicsGraphGrouperRegistry<ProteinQuantificationGraph<3> > grouper;
    grouper.group_proteins_from_name(*pqg_ptr, "identical_connectivity");

    std::cerr << "Creating TargetDecoy" << std::endl;
    std::set<std::string> all_protein_keys;
    const auto & node_ptr_set = pqg_ptr->get_protein_node_ptr_set();
    for (const auto & node_ptr : node_ptr_set)
      all_protein_keys.insert(node_ptr->get_key());
    std::shared_ptr<TargetDecoy<std::string> > td_ptr( new PrefixSuffixTargetDecoy("", ":DCY", all_protein_keys) );

    std::cerr << "Dumping protein-level features" << std::endl;
    pqg_ptr->dump_protein_features();
  }
  else
    std::cerr << "usage:\tnpci-quant <TMT TSV file>" << std::endl;
}

