#ifndef _REGISTRY_H
#define _REGISTRY_H

#include <string>
#include <memory>
#include <map>

#include "StandardExceptions.hpp"

template <typename T>
class Registry
{
private:
  std::map<std::string, std::shared_ptr<T> > name_to_object_ptr;
public:
  void register_name(const std::string & name, std::shared_ptr<T> obj_ptr);
  const std::shared_ptr<T> & lookup_name(const std::string & name) const;
  bool contains(const std::string & name) const { return name_to_object_ptr.count(name) > 0; }
  const std::map<std::string, std::shared_ptr<T> > & get_name_to_object_ptr() const { return name_to_object_ptr; }

  struct RegistryException : public StandardException {
    RegistryException(const std::string & str) throw():
      StandardException(str) { }
  };
};

#include "Registry.cpp"

#endif
