#ifndef _NONNEGATIVEGRIDFUNCTION_H
#define _NONNEGATIVEGRIDFUNCTION_H

#include "NumericGridFunction.hpp"

template <typename DOUBLEVAL, std::size_t N>
class NonnegativeGridFunction : public NumericGridFunction<DOUBLEVAL, N>
{
protected:
public:
  NonnegativeGridFunction() {}
  NonnegativeGridFunction(const NumericGridFunction<DOUBLEVAL, N> & ngf);

  void make_nonnegative();

  const NonnegativeGridFunction<DOUBLEVAL, N> & operator +=(const NumericGridFunction<DOUBLEVAL, N> & rhs);
  const NonnegativeGridFunction<DOUBLEVAL, N> & operator -=(const NumericGridFunction<DOUBLEVAL, N> & rhs);
};

#include "NonnegativeGridFunction.cpp"

#endif
