// stackoverflow question: use member or argument?

template <typename DOUBLEVAL, std::size_t N>
SmoothedHistogram<DOUBLEVAL, N>::SmoothedHistogram(const NumericGridFunction<DOUBLEVAL, N> & ngf, double n):
  NonnegativeGridFunction<DOUBLEVAL, N>(ngf), n_(n) {
  // scale so that the definite_integral is n
  DOUBLEVAL total = NumericGridFunction<DOUBLEVAL, N>::definite_integral();

  // note: this statement should trigger if total is nan
  if (total <= DOUBLEVAL(0.0)) {
    throw NumericalStabilityException("Nonpos AUC in SmoothedHistogram ctor: " + to_string(total));
  }

  for (DOUBLEVAL & f: NonnegativeGridFunction<DOUBLEVAL, N>::values_) {
    LogDouble temp = LogDouble(n_) / total;
    f *= DOUBLEVAL(temp);
  }
}

