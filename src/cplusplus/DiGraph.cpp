template <typename K, typename NodeType >
void DiGraph<K, NodeType>::copy_edges_from_rhs(const DiGraph<K, NodeType> & rhs) {
  // add the edges from rhs
  for (const std::pair<K, DiGraph<K, NodeType>::NodePtr> & key_and_node_ptr : rhs.key_to_node_ptr) {
      const NodePtr n_ptr = key_and_node_ptr.second;
      for ( const NodePtr n_ptr2 : n_ptr->get_successor_ptrs() )
	add_edge_between_keys(n_ptr->get_key(), n_ptr2->get_key());
    }
}

template <typename K, typename NodeType >
typename DiGraph<K, NodeType>::NodePtr DiGraph<K, NodeType>::operator[](const K & key) const {
  check_key_is_in_graph(key);
  return key_to_node_ptr.find( key )->second;
}

template <typename K, typename NodeType >
typename std::set<typename DiGraph<K, NodeType>::NodePtr> DiGraph<K, NodeType>::operator[](const std::set<K> & key_set) const {
  std::set<DiGraph<K, NodeType>::NodePtr> result;
  for (const K & key : key_set)
    result.insert( (*this)[key] );
  return result;
}

template <typename K, typename NodeType >
void DiGraph<K, NodeType>::clear() {
  // make a copy of the node ptrs (i.e. values) to prevent altering
  // the set as it is being iterated through
  for (DiGraph<K, NodeType>::NodePtr node_ptr : values(key_to_node_ptr) )
    // remove all nodes
    remove_node_ptr( node_ptr );
}

template <typename K, typename NodeType >
const DiGraph<K, NodeType> & DiGraph<K, NodeType>::operator =(const DiGraph<K, NodeType> & rhs) {
  if ( this != & rhs ) {
      clear();
      copy_from_rhs();
    }
  return *this;
}

template <typename K, typename NodeType >
void DiGraph<K, NodeType>::add_node(const DiGraph<K, NodeType>::Node & n) {
  // add the node only if it is not in the graph
  if ( ! contains_key(n.get_key()) ) {
      // makes a pointer to a new copy of the node
      DiGraph<K, NodeType>::NodePtr np = n.clone();
      key_to_node_ptr.insert(std::pair<K, DiGraph<K, NodeType>::NodePtr>(n.get_key(), np));
      node_ptr_set.insert(np);
    }
}

template <typename K, typename NodeType >
void DiGraph<K, NodeType>::remove_key(const K & key) {
  DiGraph<K, NodeType>::NodePtr node_ptr = operator[](key);
  remove_node_ptr( node_ptr );
}

template <typename K, typename NodeType >
void DiGraph<K, NodeType>::remove_node_ptr(DiGraph<K, NodeType>::NodePtr node_ptr) {
  check_node_ptr_is_in_graph(node_ptr);

  key_to_node_ptr.erase(node_ptr->get_key());
  node_ptr_set.erase(node_ptr);

  // make copies of predecessors and successors so they don't
  // corrupt the loops as they're modified:

  // remove the node from the successors in its predecessors
  std::set<DiGraph<K, NodeType>::NodePtr> predecessors = node_ptr->get_predecessor_ptrs();
  for (const DiGraph<K, NodeType>::NodePtr pred : predecessors)
    pred->remove_edge_to_ptr(node_ptr);

  // remove the node from the predecessors in its successors
  std::set<DiGraph<K, NodeType>::NodePtr> successors = node_ptr->get_successor_ptrs();
  for (const DiGraph<K, NodeType>::NodePtr succ : successors)
    node_ptr->remove_edge_to_ptr(succ);

  // delete the node itself
  delete node_ptr;
}

// should only be performed on nodes that are already in the graph
template <typename K, typename NodeType >
void DiGraph<K, NodeType>::add_edge(DiGraph<K, NodeType>::NodePtr from, DiGraph<K, NodeType>::NodePtr to) {
  // note: these checks are reminiscient of array bounds checking;
  // removal will enhance speed
  check_node_ptr_is_in_graph(from);
  check_node_ptr_is_in_graph(to);

  from->add_edge_to_ptr( to );
}

template <typename K, typename NodeType >
void DiGraph<K, NodeType>::remove_edge(DiGraph<K, NodeType>::NodePtr from, DiGraph<K, NodeType>::NodePtr to) {
  // note: these checks are reminiscient of array bounds checking;
  // removal will enhance speed
  check_node_ptr_is_in_graph(from);
  check_node_ptr_is_in_graph(to);

  from->remove_edge_to_ptr( to );
}

template <typename K, typename NodeType >
void DiGraph<K, NodeType>::add_edge_between_keys(const K & from_key, const K & to_key) {
  add_edge(operator[](from_key), operator[](to_key));
}

template <typename K, typename NodeType >
void DiGraph<K, NodeType>::remove_edge_between_keys(const K & from_key, const K & to_key) {
  remove_edge(operator[](from_key), operator[](to_key));
}

template <typename K, typename NodeType >
void DiGraph<K, NodeType>::copy_from_rhs(const DiGraph<K, NodeType> & rhs) {
  // add the nodes from rhs
  for (const std::pair<K, DiGraph<K, NodeType>::NodePtr> & key_and_node_ptr : rhs.key_to_node_ptr)
    add_node(*key_and_node_ptr.second);
  copy_edges_from_rhs(rhs);
}

// primarily for debugging
template <typename K, typename NodeType >
void DiGraph<K, NodeType>::print() const
{
  for (const std::pair<K, DiGraph<K, NodeType>::NodePtr> & key_and_node_ptr: key_to_node_ptr)
    {
      std::cout << "NODE:" << std::endl;
      const DiGraph<K, NodeType>::NodePtr np = key_and_node_ptr.second;
      for (const NodePtr pred : np->get_predecessor_ptrs()) {
	std::cout << pred << " ";
	pred->print();
      }

      std::cout << "\t --> ";
      std::cout << key_and_node_ptr.second << " ";
      key_and_node_ptr.second->print();
      std::cout << "\t\t\t\t --> " << std::endl;

      for (const DiGraph<K, NodeType>::NodePtr succ : np->get_successor_ptrs()) {
	std::cout << "\t\t\t\t\t";
	std::cout << succ << " ";
	succ->print();
      }
    }
}

template <typename K, typename NodeType >
void DiGraph<K, NodeType>::print_keys() const
{
  for (const std::pair<K, DiGraph<K, NodeType>::NodePtr> & key_and_node_ptr: key_to_node_ptr)
    {
      const DiGraph<K, NodeType>::NodePtr np = key_and_node_ptr.second;
      for (const NodePtr pred : np->get_predecessor_ptrs()) {
	pred->print();
      }

      std::cout << "\t --> ";
      key_and_node_ptr.second->print();
      std::cout << "\t\t\t\t --> " << std::endl;

      for (const DiGraph<K, NodeType>::NodePtr succ : np->get_successor_ptrs()) {
	std::cout << "\t\t\t\t\t";
	succ->print();
      }
    }
}

template <typename K, typename NodeType >
void DiGraph<K, NodeType>::make_edges_symmetric() {
  for (NodePtr node_ptr : node_ptr_set) {
    const std::set<NodePtr> & succs = node_ptr->get_successors_ptrs();
    for (NodePtr succ : succs) {
      // add the edge B --> A
      add_edge(succ, node_ptr);
    }
  }
}

template <typename K, typename NodeType >
void DiGraph<K, NodeType>::moralize() {
  for (NodePtr node_ptr : node_ptr_set) {
    const std::set<NodePtr> & preds = node_ptr->get_predecessor_ptrs();
    for (NodePtr pred_a : preds) {
      for (NodePtr pred_b : preds) {
	if ( pred_a != pred_b ) {
	  // these nodes share a child. marry them.
	  add_edge(pred_a, pred_b);
	  add_edge(pred_b, pred_a);
	}
      }
    }
  }

  // now this should be treated as an undirected graph; add a B --> A
  // edge for every A --> B edge
  make_edges_symmetric();
}


template <typename K, typename NodeType >
void DiGraph<K, NodeType>::triangulate() {
  // fixme: not sure if return type will be void, etc. (cliques
  // should also be stored here)
  throw NotImplementedException("triangulation code goes here");
}
