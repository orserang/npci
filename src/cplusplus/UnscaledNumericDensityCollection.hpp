#ifndef _UNSCALEDDENSITYCOLLECTION_H
#define _UNSCALEDDENSITYCOLLECTION_H

#include <iterator>
#include <array>

#include "LogDouble.hpp"
#include "KroneckerHistogramCollection.hpp"
#include "UnscaledNumericDensity.hpp"

template <std::size_t> class UnscaledNumericDensityCollection;

template <std::size_t N>
class UnscaledNumericDensityCollectionIterator : public std::iterator< UnscaledNumericDensity<LogDouble, N>, std::forward_iterator_tag > {
public:

  template <std::size_t> friend class UnscaledNumericDensityCollection;

  UnscaledNumericDensityCollectionIterator & operator++() {
    // if the histogram change is empty, do not bother smoothing and
    // += it. also, do not use get_n() to check for empty histogram
    // (it will fail when there are some positive and equally many
    // negative counts); instead, use number_of_unique_keys()
    if ( iter_->number_of_unique_keys() != 0 )
      state_ += UnscaledNumericDensity<LogDouble, N>(*iter_, undc_ptr_->nr_, undc_ptr_->sigma_);
    ++iter_;
    return *this;
  }

  UnscaledNumericDensityCollectionIterator operator++(int) {
    UnscaledNumericDensityCollectionIterator tmp(*this);
    ++*this;
    return tmp;
  }

  UnscaledNumericDensity<LogDouble, N> const& operator*() const { return state_; }

  UnscaledNumericDensity<LogDouble, N> const* operator->() const { return & state_; }

  bool operator ==(const UnscaledNumericDensityCollectionIterator<N> & rhs) const {
    return undc_ptr_ == rhs.undc_ptr_ && iter_ == rhs.iter_;
  }
  bool operator !=(const UnscaledNumericDensityCollectionIterator<N> & rhs) const {
    return ! (*this == rhs);
  }
private:
  UnscaledNumericDensityCollectionIterator(const UnscaledNumericDensityCollection<N> & undc, const typename std::list<KroneckerHistogram<std::array<double, N> > >::const_iterator & iter) :
    undc_ptr_( & undc ), iter_(iter) { }

  UnscaledNumericDensityCollectionIterator(const UnscaledNumericDensityCollection<N> & undc, const typename std::list<KroneckerHistogram<std::array<double, N> > >::const_iterator & iter, const UnscaledNumericDensity<LogDouble, N> & state) :
    undc_ptr_( & undc ), iter_(iter), state_(state) { }

  const UnscaledNumericDensityCollection<N> * undc_ptr_;
  typename std::list<KroneckerHistogram<std::array<double, N> > >::const_iterator iter_;
  UnscaledNumericDensity<LogDouble, N> state_;
};

template <std::size_t N>
class UnscaledNumericDensityCollection {
public:
  UnscaledNumericDensityCollection(const KroneckerHistogramCollection<N> & khc, const NumericRange<N> & nr, const std::array<double, N> & sigma) :
    khc_(khc), nr_(nr), sigma_(sigma) { }
  
  template <std::size_t> friend class UnscaledNumericDensityCollectionIterator;
  typedef UnscaledNumericDensityCollectionIterator<N> const_iterator;
  
  const_iterator begin() const;
  const_iterator end() const;
  
private:
  KroneckerHistogramCollection<N> khc_;
  NumericRange<N> nr_;
  std::array<double, N> sigma_;
};
  
template <std::size_t N>
typename UnscaledNumericDensityCollection<N>::const_iterator UnscaledNumericDensityCollection<N>::begin() const {
  return UnscaledNumericDensityCollection<N>::const_iterator(*this, khc_.begin_deltas(), UnscaledNumericDensity<LogDouble, N>(khc_.get_initial(), nr_, sigma_) );
}

template <std::size_t N>
typename UnscaledNumericDensityCollection<N>::const_iterator UnscaledNumericDensityCollection<N>::end() const {
  return UnscaledNumericDensityCollection<N>::const_iterator(*this, khc_.end_deltas());
}

#endif

