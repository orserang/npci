template <typename PROTEOMICS_GRAPH_TYPE>
std::unique_ptr<PROTEOMICS_GRAPH_TYPE> ProteinIDGraphFactory<PROTEOMICS_GRAPH_TYPE>::create_ungrouped_proteomics_graph(const FileAndFormat & faf)
{
  std::unique_ptr<PROTEOMICS_GRAPH_TYPE> pg_ptr( new PROTEOMICS_GRAPH_TYPE );
  const ProteinIDGraphLoaderRegistry & pglr = Singleton<ProteinIDGraphLoaderRegistry>::get_instance();
  pglr.load_from_file(*pg_ptr, faf);
  return pg_ptr;
}

