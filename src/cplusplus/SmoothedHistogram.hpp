#ifndef _SMOOTHEDHISTOGRAM_H
#define _SMOOTHEDHISTOGRAM_H

#include "NonnegativeGridFunction.hpp"
#include "PDF.hpp"

template <typename DOUBLEVAL, std::size_t N>
class SmoothedHistogram : public NonnegativeGridFunction<DOUBLEVAL, N>
{
protected:
  double n_;
public:
  SmoothedHistogram() : n_(0) {}
  SmoothedHistogram(const NumericGridFunction<DOUBLEVAL, N> & ngf, double n);
  int get_n() const { return n_; }
};

#include "SmoothedHistogram.cpp"

#endif
