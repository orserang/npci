#ifndef _SINGLEREPLICATECUTOUTANALYSISREPORT_H
#define _SINGLEREPLICATECUTOUTANALYSISREPORT_H

#include "BaseReport.hpp"

template <typename K, std::size_t N>
class SingleReplicateCutoutAnalysisReport : public BaseReport {
public:
  SingleReplicateCutoutAnalysisReport(const SingleReplicateCutoutAnalysis<K,N> & srca):
    BaseReport("SingleReplicateCutoutAnalysisReport")
  {
    write_labeled_value("label", srca.get_label());
    write_axis_labels(srca);
    write_sub_report( srca.likelihood_similarity_ptr_->generate_report() );
  }

protected:
  void write_axis_labels(const SingleReplicateCutoutAnalysis<K,N> & srca) {
    // don't write them if the labels have not been set
    std::array<std::string, N> empty_labels;
    const std::array<std::string, N> & axis_labels = srca.cg_ptr_->get_axis_labels();
    if (srca.cg_ptr_->get_axis_labels() == empty_labels)
      return;

    rapidxml::xml_node<> * axis_labels_node = BaseReport::create_node_ptr("axis_labels");
    for (std::size_t i=0; i<N; ++i) {
      rapidxml::xml_node<> * single_axis_label = BaseReport::create_node_ptr("axis");
      single_axis_label->append_attribute( BaseReport::create_attribute_ptr("number", i) );
      single_axis_label->append_attribute( BaseReport::create_attribute_ptr("label", axis_labels[i]) );
      axis_labels_node->append_node(single_axis_label);
    }

    root_->append_node(axis_labels_node);
  }
};

#endif
