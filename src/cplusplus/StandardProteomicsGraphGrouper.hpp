#ifndef _STANDARDPROTEOMICSGRAPHGROUPER_H
#define _STANDARDPROTEOMICSGRAPHGROUPER_H

#include "ProteomicsGraphGrouper.hpp"

template <typename PROTEOMICSGRAPH>
class StandardProteomicsGraphGrouper: public ProteomicsGraphGrouper<PROTEOMICSGRAPH> {
public:
  virtual std::list<std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> > get_protein_groups(const PROTEOMICSGRAPH & pg) const;
};

#include "StandardProteomicsGraphGrouper.cpp"

#endif
