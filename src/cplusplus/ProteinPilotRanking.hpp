#ifndef _PROTEINPILOTRANKING_H
#define _PROTEINPILOTRANKING_H

#include <fstream>
#include <sstream>
#include <string>

#include "Ranking.hpp"
#include "TabSeparatedFile.hpp"

class ProteinPilotRanking : public Ranking<std::string> {
public:
  ProteinPilotRanking(const std::string & filename);
};

#endif
