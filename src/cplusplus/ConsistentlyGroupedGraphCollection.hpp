#ifndef _CONSISTENTLYGROUPEDGRAPHCOLLECTION_H
#define _CONSISTENTLYGROUPEDGRAPHCOLLECTION_H

#include <list>
#include <string>

// fixme: should this be templated or use inheritance (accept a list of ProteoimicsGraph*)?
template <typename GRAPHTYPE>
class ConsistentlyGroupedGraphCollection
{
public:
  ConsistentlyGroupedGraphCollection(const std::list<GRAPHTYPE> & all_graphs_param, const std::string & grouping_name);

  const std::list<GRAPHTYPE> & get_consistently_grouped_graphs() const { return all_graphs; }
  const GRAPHTYPE & get_union_graph() const { return union_graph; }
protected:
  std::list<GRAPHTYPE> all_graphs;
  GRAPHTYPE union_graph;
};

#include "ConsistentlyGroupedGraphCollection.cpp"

#endif
