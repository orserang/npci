#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <numeric>
 
#include "PrefixSuffixTargetDecoy.hpp"
#include "PlainTextSetTargetDecoy.hpp"
#include "PlainTextRanking.hpp"
#include "GroupRanking.hpp"

#include "ProteinQuantificationGraph.hpp"
#include "ProteomicsGraphGrouperRegistry.hpp"

#include "SingleReplicateCutoutAnalysis.hpp"
#include "MultiReplicateCutoutAnalysis.hpp"

#include "DirichletSmoothedLikelihoodSimilarity.hpp"
#include "IIDSmoothedLikelihoodSimilarity.hpp"
#include "KullbackLeiblerSmoothedLikelihoodSimilarity.hpp"
#include "KolmogorovSmirnovLikelihoodSimilarity.hpp"

Ranking<std::string> merge_rankings(const Ranking<std::string> & a, const Ranking<std::string> & b) {
  std::map<double, std::set<std::string> > score_to_object_set;
  
  const std::map<double, std::set<std::string> > & score_to_object_set_a = a.get_double_to_scored_object_set_map(), & score_to_object_set_b = b.get_double_to_scored_object_set_map();

  std::map<std::string, double> a_to_score, b_to_score, both_to_score;
  for (std::pair<double, std::set<std::string> > score_and_set : score_to_object_set_a)
    for (std::string str : score_and_set.second)
      a_to_score[str] = score_and_set.first / 2.0;
  for (std::pair<double, std::set<std::string> > score_and_set : score_to_object_set_b)
    for (std::string str : score_and_set.second)
      b_to_score[str] = score_and_set.first / 2.0;

  // compute sum of a_to_score and b_to_score
  for (std::pair<std::string, double> key_and_score: a_to_score) {
    double total_score = key_and_score.second + b_to_score.find(key_and_score.first)->second;
    both_to_score[key_and_score.first] = total_score;
  }

  // propagate sum into score_to_object set
  for (std::pair<std::string, double> key_and_score: both_to_score) {
    score_to_object_set[key_and_score.second].insert(key_and_score.first);
  }

  return Ranking<std::string>(score_to_object_set, a.get_label());
}

int main(int argc, char**argv) {
  if (argc >= 6 ) {

    std::cout.precision(10);

    std::cerr << "Loading Graph" << std::endl;

    std::string proteinPeptideGraphFile = argv[1];
    std::string caseLabel = argv[2];
    std::string controlLabel1 = argv[3];
    std::string controlLabel2 = argv[4];

    std::string savePdfs = argv[5];
    if (savePdfs != "true" && savePdfs != "false")
      throw FormatException("savePdfs must be \"true\" or \"false\" (without quotes)");

    // Amount to downsample plotted results (a higher downsample factor makes for faster but rougher processing):
    const int downsampleFactor = 3;
    // Maximum amount of points on the plot to process (set to be effectively infinite for most data):
    const int upperIndexLimit = 1000;
    //    const int upperIndexLimit = 65536;

    const int numBucketsForNumericIntegration = 100;
    const int minimumNumberOfPeptidesConsidered = 10;

    // If no external rankings are provided, use 1-peptide and 2-peptide rankings:
    const int maximumKPeptideRankingFamily = 2;

    // Note: In some settings it may be good to use {{caseLabel,
    // controlLabel1}}, {{controlLabel1, controlLabel2}} rather than
    // {{caseLabel, controlLabel1}}, {{controlLabel2, controlLabel1}}
    // in order to force controlLabel1 to be compared to
    // controlLabel2, rather than compared to itself:

    // Also note: The 0.1, 0.1 arguments define the intensity threshold and
    // pseudocount parameters:
    std::shared_ptr<TMTFoldChangeGraph> pqg_ptr( new TMTFoldChangeGraph(argv[1], ',', {{caseLabel, controlLabel1}}, {{controlLabel2, controlLabel1}}, 0.1, 0.1) );
    std::shared_ptr<TMTIntensityScatterGraph> pqg_scatter_ptr( new TMTIntensityScatterGraph(argv[1], ',', {{caseLabel, controlLabel1}}, {{controlLabel2, controlLabel1}}, 0.0, 1) );

    ProteomicsGraphGrouperRegistry<TMTFoldChangeGraph> grouper;
    grouper.group_proteins_from_name(*pqg_ptr, "identical_connectivity");

    ProteomicsGraphGrouperRegistry<TMTIntensityScatterGraph> scatter_grouper;
    scatter_grouper.group_proteins_from_name(*pqg_scatter_ptr, "identical_connectivity");

    std::cerr << "Loading TargetDecoy" << std::endl;
    std::set<std::string> all_protein_keys;
    const auto & node_ptr_set = pqg_ptr->get_protein_node_ptr_set();
    for (const auto & node_ptr : node_ptr_set)
      all_protein_keys.insert(node_ptr->get_key());

    // The suffix ":DCY" is used internally to specify the control-control differentials:
    std::shared_ptr<TargetDecoy<std::string> > td_ptr( new PrefixSuffixTargetDecoy("", ":DCY", all_protein_keys) );

    std::cerr << "Creating and Grouping Rankings" << std::endl;
    std::list<std::shared_ptr<GroupRanking<std::string> > > g_rk_ptr_lst;

    // Additional arguments specify rankings to be read (rather than k-peptide rankings):
    if (argc > 6) {
      /* plain text rankings */
      std::cerr << "\tusing plain text rankings (provided in arguments)" << std::endl;
      for (int i=2; i<argc; ++i) {
	std::shared_ptr<GroupRanking<std::string> > g_rk_ptr( new GroupRanking<std::string>( PlainTextRanking<std::string>(argv[i] ), pqg_ptr->get_grouped_keys())) ;
	g_rk_ptr_lst.push_back( g_rk_ptr );
      }
    }

    // No additional arguments means that the family of k-peptide rankings are used:
    else {
      std::cerr << "\tusing peptide rule rankings" << std::endl;
      for (int i=1; i<=maximumKPeptideRankingFamily; ++i) {

	// Specifies whether shared peptides are used in the k-peptide ranking:
	const bool considerSharedPeptides = true;
	auto merged_ranking = pqg_ptr->geom_mean_k_peptide_ranking(i, considerSharedPeptides);

	// When processing multiple data sets, merge the rankings over all data graphs:
	//	auto merged_ranking = merge_rankings(pqg_ptr->geom_mean_k_peptide_ranking(i, true), pqg_ptr2->geom_mean_k_peptide_ranking(i, true));

	std::shared_ptr<GroupRanking<std::string> > geom_mean_g_rk_shared_ptr( new GroupRanking<std::string> (merged_ranking.downsampled(downsampleFactor, upperIndexLimit), pqg_ptr->get_grouped_keys()) );

	g_rk_ptr_lst.push_back( geom_mean_g_rk_shared_ptr );
      }
    }
    
    std::cerr << "Performing CutoutAnalysis" << std::endl;
    std::shared_ptr<LikelihoodSimilarity<2> > similarity_engineA( new DirichletSmoothedLikelihoodSimilarity<2>(numBucketsForNumericIntegration, minimumNumberOfPeptidesConsidered, savePdfs == "true" ) );

    std::shared_ptr<BaseCutoutAnalysis<ProteomicsKey> > repA_ptr( new SingleReplicateCutoutAnalysis<ProteomicsKey, 2>(pqg_scatter_ptr, td_ptr, g_rk_ptr_lst, similarity_engineA, "Channels " + caseLabel + "," + controlLabel1 + "," + controlLabel2) );

    // Note: Other data sets can be added in this manner:

    //    std::shared_ptr<LikelihoodSimilarity<2> > similarity_engineB( new DirichletSmoothedLikelihoodSimilarity<2>(50, 10, save_pdfs) );
    //    std::shared_ptr<BaseCutoutAnalysis<ProteomicsKey> > repB_ptr( new SingleReplicateCutoutAnalysis<ProteomicsKey, 2>(pqg_scatter_ptr2, td_ptr, g_rk_ptr_lst, similarity_engineB, "Channels 4,5,6") );

    //    std::list<std::shared_ptr<BaseCutoutAnalysis<ProteomicsKey> > > single_rep_list{repA_ptr, repB_ptr};

    std::list<std::shared_ptr<BaseCutoutAnalysis<ProteomicsKey> > > single_rep_list{repA_ptr};
    MultiReplicateCutoutAnalysis<ProteomicsKey> mca(single_rep_list, "All channels");

    std::cerr << "Generating and Writing Report" << std::endl;
    std::cout << mca.generate_report() << std::endl;
  }
  else
    std::cerr << "usage:\tnpci-quant <TMT TSV file> <case label string> <control label string 1> <control label string 2> <savePdfs \"true\" or \"false\"> [ ranking1... ]" << std::endl;
}

