#ifndef _LIKELIHOODSIMILARITY_H
#define _LIKELIHOODSIMILARITY_H

#include <array>
#include <algorithm>

#include "LogDouble.hpp"
#include "KroneckerHistogram.hpp"
#include "IteratorUtilities.hpp"

// forward declaration (to allow LikelihoodSimilarityReport<N> to be
// declared beforehand)
template <std::size_t> class LikelihoodSimilarityReport;

template <std::size_t N>
class LikelihoodSimilarity {
  friend class LikelihoodSimilarityReport<N>;
protected:
  std::list<std::array<KroneckerHistogramCollection<N>, 2> > kronicker_collection_lists_;
  double n0_, n1_;
  const int minimum_data_;

  // cache all_likelihoods (prevents potentially expensive
  // re-computation when computing the product for
  // MultiReplicateCutoutAnalysis)
  std::list<std::vector<LogDouble> > all_likelihoods_;

  virtual std::list<std::vector<LogDouble> > compute_all_likelihoods() const = 0;

  void init_all_likelihoods() {
    all_likelihoods_ = compute_all_likelihoods();
    std::list<LogDouble> flattened = flatten(all_likelihoods_);
    LogDouble max_of_maximum_likelihoods = *std::max_element( flattened.begin(), flattened.end() );
    if ( all_likelihoods_.size() == 0 or LogDouble::is_inf(max_of_maximum_likelihoods) )
      throw NumericalStabilityException("No collection result in a finite likelihood");
  }

private:
  void init_counts() {
    // compute the average sample sizes for every collection (for each
    // collection, compute the average KroneckerHistogram::get_n() and
    // weight all collections equally). this ensures that the order of
    // collections and the order of histograms in each collection will
    // not affect the computed values of n0_ and n1_.

    n0_ = n1_ = 0;
    for (const std::array<KroneckerHistogramCollection<N>, 2> & collections : kronicker_collection_lists_) {
      // for this collection, compute the average number n0 and n1
      double average_n_for_collection0 = 0;
      for (const KroneckerHistogram<std::array<double, N> > & kh0 : collections[0])
	  average_n_for_collection0 += kh0.get_n();
      average_n_for_collection0 /= collections[0].size();

      double average_n_for_collection1 = 0;
      for (const KroneckerHistogram<std::array<double, N> > & kh1 : collections[1])
	average_n_for_collection1 += kh1.get_n();
      average_n_for_collection1 /= collections[1].size();

      // accumulate to compute the average of the n0 and n1 over all
      // collections. note that this average is unweighted
      // (collections with more KroneckerHistogram do not have more
      // influence).

      n0_ += average_n_for_collection0;
      n1_ += average_n_for_collection1;
    }
    n0_ /= kronicker_collection_lists_.size();
    n1_ /= kronicker_collection_lists_.size();

    if (n0_ < 0 || n1_ < 0)
      throw NumericalStabilityException("Average Kronecker histogram count is negative (should not be)");
  }

public:

  // to construct without initializing data
  LikelihoodSimilarity(int minimum_data_param):
    minimum_data_(minimum_data_param)
  { }

  // to construct with data
  LikelihoodSimilarity(const std::list<std::array<KroneckerHistogramCollection<N>, 2> > & kronicker_collection_lists, int minimum_data_param):
    minimum_data_(minimum_data_param)
  {
    // using an init method allows reseting the data externally in a
    // subtype-specific way without the need for clone()
    init_data(kronicker_collection_lists);
  }

  virtual ~LikelihoodSimilarity() { }

  virtual void init_data(const std::list<std::array<KroneckerHistogramCollection<N>, 2> > & kronicker_collection_lists) {
    kronicker_collection_lists_ = kronicker_collection_lists;

    // make sure each pair of collections has identical size
    for (const std::array<KroneckerHistogramCollection<N>, 2> & kronicker_collections: kronicker_collection_lists_)
      if ( kronicker_collections[0].size() != kronicker_collections[1].size() )
	throw SizeMismatchException("Two KroneckerHistogramCollection don't have the same size when unpacking KroneckerHistogramCollections");

    // make sure no KroneckerHistograms have negative counts
    for (const std::array<KroneckerHistogramCollection<N>, 2> & kronicker_collections: kronicker_collection_lists_) {

      for (const KroneckerHistogram<std::array<double, N> > & kh : kronicker_collections[0])
	if ( kh.number_of_unique_keys() > 0 ) {
	  std::list<int> vals = values(kh);

	  if (*min_element(vals.begin(), vals.end()) < 0)
	    throw NegativeCountException("Negative counts encountered when unpacking all KroneckerHistogramCollection");
	}

      for (const KroneckerHistogram<std::array<double, N> > & kh : kronicker_collections[1])
	if ( kh.number_of_unique_keys() > 0 ) {
	  std::list<int> vals = values(kh);

	  if (*min_element(vals.begin(), vals.end()) < 0)
	    throw NegativeCountException("Negative counts encountered when unpacking all KroneckerHistogramCollection");
	}
    }

    init_counts();
  }

  std::list<std::vector<LogDouble> > get_all_likelihoods() const {
    return all_likelihoods_;
  }

  static std::list<std::vector<LogDouble> > normalized_likelihoods(const std::list<std::vector<LogDouble> > & likelihoods) {
    std::list<std::vector<LogDouble> > result;
    std::list<LogDouble> flattened = flatten(likelihoods);
    LogDouble maximum_likelihood = *std::max_element( flattened.begin(), flattened.end() );
    for (const std::vector<LogDouble> & single_series : likelihoods) {
      std::vector<LogDouble> normalized_series(single_series.size());
      for (std::size_t k=0; k<single_series.size(); ++k) {
	normalized_series[k] = single_series[k] / maximum_likelihood;
      }
      result.push_back(normalized_series);
    }
    return result;
  }

  // moved outside of class so that LikelihoodSimilarity<N> is defined
  // before LikelihoodSimilarityReport<N> is used (which needs
  // LikelihoodSimilarity<N> to be defined).
  virtual BaseReport generate_report() const;
};

// include the report class before defining generate_report
#include "LikelihoodSimilarityReport.hpp"

template <std::size_t N>
BaseReport LikelihoodSimilarity<N>::generate_report() const {
  return LikelihoodSimilarityReport<N>(*this);
}


#endif
