#ifndef _CUTOUTGRAPHNODES_H
#define _CUTOUTGRAPHNODES_H

#include <memory>
#include <array>
#include <limits>

#include "MarkableDiGraphNode.hpp"
#include "ArrayPrint.hpp"

template <typename K>
class NoEvidenceCutoutGraphNode : public MarkableDiGraphNode<K> {
public:
  NoEvidenceCutoutGraphNode(const K & key_param):
    MarkableDiGraphNode<K>(key_param) {}
  virtual ~NoEvidenceCutoutGraphNode() {}

  virtual NoEvidenceCutoutGraphNode<K>*clone() const { return new NoEvidenceCutoutGraphNode<K>(*this); }
};

template <typename K, std::size_t N>
class EvidenceCutoutGraphNode : public MarkableDiGraphNode<K> {
public:
  // equivalent of typedef
  static const std::size_t DIMENSION = N;

  EvidenceCutoutGraphNode(const K & key_param, const std::array<double, N> & evidence_param):
    MarkableDiGraphNode<K>(key_param), evidence(evidence_param) { }
  virtual ~EvidenceCutoutGraphNode() {}

  const std::array<double, N> & get_evidence() const { return evidence; }
  void set_evidence(const std::array<double, N> & evidence_param) {
    evidence = evidence_param;
  }

  virtual void print() const {
    std::cout << "evidence=" << evidence << " ";
    MarkableDiGraphNode<K>::print();
  }

  virtual EvidenceCutoutGraphNode<K,N>*clone() const { return new EvidenceCutoutGraphNode<K,N>(*this); }

private:
  std::array<double, N> evidence;
};

#endif
