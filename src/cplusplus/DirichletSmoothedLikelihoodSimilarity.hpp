#ifndef _DIRICHLETSMOOTHEDLIKELIHOODSIMILARITY_H
#define _DIRICHLETSMOOTHEDLIKELIHOODSIMILARITY_H

#include "SmoothedLikelihoodSimilarity.hpp"

template <std::size_t N>
class DirichletSmoothedLikelihoodSimilarity : public SmoothedLikelihoodSimilarity<N> {
public:
  DirichletSmoothedLikelihoodSimilarity(const std::list< std::array<KroneckerHistogramCollection<N>, 2> > & kronicker_collection_lists_param, int num_bins_param, int minimum_data_param, bool save_distributions):
    SmoothedLikelihoodSimilarity<N>(kronicker_collection_lists_param, num_bins_param, minimum_data_param, save_distributions) {
    this->init_best_sigmas();
  }

  DirichletSmoothedLikelihoodSimilarity(int num_bins_param, int minimum_data_param, bool save_distributions):
    SmoothedLikelihoodSimilarity<N>(num_bins_param, minimum_data_param, save_distributions) {
    // if no data is provided, don't bother initializing sigma
  }

  // if the data is reinitialized, estimate the best sigmas again
  virtual void init_data(const std::list< std::array<KroneckerHistogramCollection<N>, 2> > & kronicker_collection_lists_param) {
    SmoothedLikelihoodSimilarity<N>::init_data(kronicker_collection_lists_param);
  }

  virtual std::shared_ptr<SmoothedLikelihoodSimilarity<1> > clone_without_data() const {
    return std::shared_ptr<SmoothedLikelihoodSimilarity<1> >( new DirichletSmoothedLikelihoodSimilarity<1>(SmoothedLikelihoodSimilarity<N>::num_bins_, SmoothedLikelihoodSimilarity<N>::minimum_data_, SmoothedLikelihoodSimilarity<N>::save_distributions_) );
  }

protected:
  virtual LogDouble smoothed_likelihood_function(const KroneckerHistogram<std::array<double, N> > & lhs_hist, const SmoothedHistogram<LogDouble, N> & lhs_hist_smoothed, const KroneckerHistogram<std::array<double, N> > & rhs_hist, const SmoothedHistogram<LogDouble, N> & rhs_hist_smoothed, const std::array<double, N> & sigma) const {
    if ( ! (lhs_hist_smoothed.get_numeric_range() == rhs_hist_smoothed.get_numeric_range()) )
      throw SizeMismatchException("The numeric ranges integrated by the DirichletLikelihoodFunctor must be identical");

    // the density_scaling_product cancels out the effects of
    // providing the same data, but with scaled x-values
    const NumericRange<N> & nr = lhs_hist_smoothed.get_numeric_range();

    LogDouble density_scaling_product = nr.template volume<LogDouble>();

    LogDouble result = directed_dirichlet_likelihood(lhs_hist_smoothed, rhs_hist_smoothed, density_scaling_product) * directed_dirichlet_likelihood(rhs_hist_smoothed, lhs_hist_smoothed, density_scaling_product);

    return result;
  }
private:
  LogDouble directed_dirichlet_likelihood(const SmoothedHistogram<LogDouble, N> & bg, const SmoothedHistogram<LogDouble, N> & fg, LogDouble density_scaling_product) const {
    LogDouble result(1.0);
    int num_buckets = 0;
    for (auto iter_pair = std::make_pair(bg.cbegin(), fg.cbegin()); iter_pair != std::make_pair(bg.cend(), fg.cend()); ++iter_pair.first, ++iter_pair.second ) {
      LogDouble bg_hist_val = *iter_pair.first * density_scaling_product;
      LogDouble fg_hist_val = *iter_pair.second * density_scaling_product;
      ++num_buckets;
      LogDouble term = directed_dirichlet_likelihood_term(bg_hist_val, bg.get_n(), fg_hist_val, fg.get_n());
      result *= term;
    }

    // take the geometric mean over all terms
    return pow(result, LogDouble(1.0) / LogDouble(num_buckets));
  }

  LogDouble directed_dirichlet_likelihood_term(LogDouble bg_hist_val, double bg_n, LogDouble fg_hist_val, double fg_n) const {
    // for terms where both base and exponent approach zero, the
    // product is not affected (LogDouble will ignore
    // these). Effectively, these values are not in the
    // support). Hence, for readability, only consider values with
    // nonzero fg_hist_val (the if statement is not necessary
    // because LogDouble already handles it)
    
    if ( fg_hist_val == LogDouble(0.0) )
      return LogDouble(1.0);
    // for numeric stability, do not count terms when the histogram
    // count is < e^(-20) (instead of zero). this prevents problems
    // where the foreground and background are both approximately
    // zero, but where the background is set to zero, causing a -inf
    // likelihood term.
    
    // rather than rescale the histograms to make the PDF, simply
    // divide each histogram value by n (since the integral was n,
    // it will become 1 by linearity)
    LogDouble bg_pdf_val = bg_hist_val / LogDouble(bg_n);
    
    // dirichlet

    // note: the n! in the numerator should not influence the result;
    // it's written here for completeness (and numeric stability), but
    // it isn't necessary, and its removal could speed execution.
    LogDouble term = pow(bg_pdf_val, fg_hist_val) * LogDouble::create_from_log_absolute_value(lgamma(1+fg_n)) / LogDouble::create_from_log_absolute_value(lgamma(1+ double(fg_hist_val) ));
    
    if ( LogDouble::is_nan(term) )
      throw NumericalStabilityException("Encountered nan in DirichletLikelihoodFunctor");
    return term;
  }
};

#endif
