#ifndef _LIKELIHOODSIMILARITYREPORT_H
#define _LIKELIHOODSIMILARITYREPORT_H

#include <string>

#include "BaseReport.hpp"

template <std::size_t N>
class LikelihoodSimilarityReport : public BaseReport {
public:
  LikelihoodSimilarityReport(const LikelihoodSimilarity<N> & ls):
    BaseReport("LikelihoodSimilarityReport")
  {
    BaseReport::write_labeled_value("dimensions", N);

    write_all_likelihoods(ls.all_likelihoods_);
    write_labeled_value("minimum_data", ls.minimum_data_);
    write_sample_sizes(ls.n0_, ls.n1_);
  }
  
protected:
  void write_sample_sizes(int n0, int n1) {
    rapidxml::xml_node<> * both_sample_sizes = BaseReport::create_node_ptr("both_sample_sizes");
    
    rapidxml::xml_node<> * sample_size0 = BaseReport::create_node_ptr("sample");
    sample_size0->append_attribute( BaseReport::create_attribute_ptr("sample_number", 0) );
    sample_size0->append_attribute( BaseReport::create_attribute_ptr("size", n0) );

    rapidxml::xml_node<> * sample_size1 = BaseReport::create_node_ptr("sample");
    sample_size1->append_attribute( BaseReport::create_attribute_ptr("sample_number", 1) );
    sample_size1->append_attribute( BaseReport::create_attribute_ptr("size", n1) );

    both_sample_sizes->append_node(sample_size0);
    both_sample_sizes->append_node(sample_size1);
    root_->append_node(both_sample_sizes);
  }
};

#endif
