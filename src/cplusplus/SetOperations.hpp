#ifndef _SETOPERATIONS_H
#define _SETOPERATIONS_H

#include <set>
#include <algorithm>
#include <iterator>
#include <iostream>

template <typename set_type>
struct SetCompare
{
  bool operator() (const set_type & lhs, const set_type & rhs) const;
};

std::ostream & operator <<(std::ostream & os, const std::set<int> & rhs);

// |= operator; faster than making a copy and performing union
template <typename set_type>
void insert_all(set_type & lhs, const set_type & rhs);

template <typename set_type>
set_type sunion(const set_type & lhs, const set_type & rhs);

template <typename set_type>
set_type sintersection(const set_type & lhs, const set_type & rhs);

template <typename set_type>
set_type sdifference(const set_type & lhs, const set_type & rhs);

template <typename T>
typename std::iterator_traits<T>::value_type sunion_over_iterator_range(T begin, T end);

#include "SetOperations.cpp"

#endif
