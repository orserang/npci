#ifndef _UNSMOOTHEDLIKELIHOODSIMILARITY_H
#define _UNSMOOTHEDLIKELIHOODSIMILARITY_H

#include "LikelihoodSimilarity.hpp"

template <std::size_t N>
class UnsmoothedLikelihoodSimilarity : public LikelihoodSimilarity<N> {
public:
  virtual std::list<std::vector<LogDouble> > compute_all_likelihoods() const {
    std::list<std::vector<LogDouble> > result;

    bool any_collections_with_enough_data = false;
    for (const std::array<KroneckerHistogramCollection<N>, 2> & khc_pair : LikelihoodSimilarity<N>::kronicker_collection_lists_) {
      
      std::vector<LogDouble> result_for_single_collection;

      // iterate through every KroneckerHistogram
      for ( auto kh_iters = make_tuple(khc_pair[0].begin(), khc_pair[1].begin());
	    std::get<0>(kh_iters) != khc_pair[0].end() && std::get<1>(kh_iters) != khc_pair[1].end();
	    ++std::get<0>(kh_iters), ++std::get<1>(kh_iters) ) {

	const KroneckerHistogram<std::array<double, N> > & kh0 = *std::get<0>(kh_iters), & kh1 = *std::get<1>(kh_iters);

	// when there are fewer than minimum_data points, you cannot compare (use a likelihood of 0)
	if ( kh0.get_n() < LikelihoodSimilarity<N>::minimum_data_ or kh1.get_n() < LikelihoodSimilarity<N>::minimum_data_ )
	  result_for_single_collection.push_back(LogDouble(0.0));
	else {

	  LogDouble score = unsmoothed_likelihood_function( kh0, double(LikelihoodSimilarity<N>::n0_), kh1, double(LikelihoodSimilarity<N>::n1_) );
	  result_for_single_collection.push_back(score);
	  any_collections_with_enough_data = true;
	}
      }
      if (! any_collections_with_enough_data)
	throw NumericalStabilityException("No collections cointained minimum number of points (" + to_string(LikelihoodSimilarity<N>::minimum_data_) + "); did you have enough data (or enough decoys?)");

      result.push_back(result_for_single_collection);
    }
    return LikelihoodSimilarity<N>::normalized_likelihoods(result);
  }

  // to construct without initializing data
  UnsmoothedLikelihoodSimilarity(int minimum_data_param):
    LikelihoodSimilarity<N>(minimum_data_param) {
  }

  // to construct with data
  UnsmoothedLikelihoodSimilarity(const std::list<std::array<KroneckerHistogramCollection<N>, 2> > & kronicker_collection_lists, int minimum_data_param):
    LikelihoodSimilarity<N>(kronicker_collection_lists, minimum_data_param) {
    LikelihoodSimilarity<N>::init_all_likelihoods();
  }

  // note: should only be called within derived classes, which have the
  // information available to call init_best_sigmas
  virtual void init_data(const std::list<std::array<KroneckerHistogramCollection<N>, 2> > & kronicker_collection_lists) {
    LikelihoodSimilarity<N>::init_data(kronicker_collection_lists);
    LikelihoodSimilarity<N>::init_all_likelihoods();
  }

protected:
  virtual LogDouble unsmoothed_likelihood_function(const KroneckerHistogram<std::array<double, 1> > & lhs_hist, int lhs_n, const KroneckerHistogram<std::array<double, 1> > & rhs_hist, int rhs_n) const = 0;
};

#endif
