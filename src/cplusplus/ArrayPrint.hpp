#ifndef _ARRAYPRINT_H
#define _ARRAYPRINT_H

#include <iostream>
#include <array>

template <std::size_t N>
std::ostream & operator <<(std::ostream & os, const std::array<double, N> & arr) {
  //  os << "{ ";
  unsigned int i;
  for (i=0; i<N-1; ++i)
    os << arr[i] << " ";
  os << arr[i];
  //  os << " }";
  return os;
}

#endif

