#ifndef _PROTEOMICSGRAPHGROUPERREGISTRY_H
#define _PROTEOMICSGRAPHGROUPERREGISTRY_H

#include "Registry.hpp"
#include "ProteomicsGraphGrouper.hpp"

#include "TrivialProteomicsGraphGrouper.hpp"
#include "StandardProteomicsGraphGrouper.hpp"

template <typename PROTEOMICSGRAPH>
class ProteomicsGraphGrouperRegistry : public Registry<ProteomicsGraphGrouper<PROTEOMICSGRAPH> > {
public:
  ProteomicsGraphGrouperRegistry();
public:
  void group_proteins_from_name(PROTEOMICSGRAPH & pg, const std::string & grouping_name) const;
  const ProteomicsGraphGrouper<PROTEOMICSGRAPH> & lookup_grouper(const std::string & grouping_name) const;
};

#include "ProteomicsGraphGrouperRegistry.cpp"

#endif
