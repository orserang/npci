#ifndef _CUTOUTGRAPH_H
#define _CUTOUTGRAPH_H

#include <algorithm>
#include <memory>

#include "MarkableDiGraph.hpp"
#include "CutoutGraphNodes.hpp"
#include "SetOperations.hpp"

template <typename K, std::size_t N>
class CutoutGraph : public MarkableDiGraph<K> {
public:
  std::vector<std::array<double, N> > all_evidence() const {
    std::vector<std::array<double, N> > result;
    for (const EvidenceCutoutGraphNode<K,N> * e_ptr : evidence_node_ptrs)
      result.push_back( e_ptr->get_evidence() );
    return result;
  }
  
  // note: when doing this many times, it can be done more efficiently
  // by sparsely retrieving the evidence nodes that change due to
  // marking (using unmarked_descendent_evidence_ptrs)
  std::vector<std::array<double, N> > unmarked_evidence() const {
    std::vector<std::array<double, N> > result;
    for (const EvidenceCutoutGraphNode<K,N> * e_ptr : evidence_node_ptrs)
      if (! e_ptr->is_marked())
	result.push_back( e_ptr->get_evidence() );
    return result;
  }

  // note: when doing this many times, it can be done more efficiently
  // by sparsely retrieving the evidence nodes that change due to
  // marking (using unmarked_descendent_evidence_ptrs)
  std::vector<std::array<double, N> > marked_evidence() const {
    std::vector<std::array<double, N> > result;
    for (const EvidenceCutoutGraphNode<K,N> * e_ptr : evidence_node_ptrs)
      if (e_ptr->is_marked())
	result.push_back( e_ptr->get_evidence() );
    return result;
  }

  std::set<EvidenceCutoutGraphNode<K,N>*> unmarked_descendent_evidence_ptrs(const std::set<typename MarkableDiGraph<K>::NodePtr> & node_ptr_set) const {
    std::set<typename MarkableDiGraph<K>::NodePtr> unmarked_descendent_ptrs = MarkableDiGraph<K>::unmarked_node_and_descendent_ptrs_from_ptr_set(node_ptr_set);
    return evidence_ptr_subset(unmarked_descendent_ptrs);
  }

  std::set<EvidenceCutoutGraphNode<K,N>*> unmarked_dependent_evidence_ptrs(const std::set<typename MarkableDiGraph<K>::NodePtr> & node_ptr_set) const {
    std::set<typename MarkableDiGraph<K>::NodePtr> unmarked_dependent_ptrs = MarkableDiGraph<K>::unmarked_node_and_dependent_ptrs_from_ptr_set(node_ptr_set);
    return evidence_ptr_subset(unmarked_dependent_ptrs);
  }

  std::vector<std::array<double, N> > unmarked_descendent_evidence(const std::set<typename MarkableDiGraph<K>::NodePtr> & node_ptr_set) const {
    std::set<EvidenceCutoutGraphNode<K,N>* > evidence_descendent_ptrs = unmarked_descendent_evidence_ptrs(node_ptr_set);
    return node_ptrs_to_evidence(evidence_descendent_ptrs);
  }

  std::vector<std::array<double, N> > unmarked_dependent_evidence(const std::set<typename MarkableDiGraph<K>::NodePtr> & node_ptr_set) const {
    std::set<EvidenceCutoutGraphNode<K,N>* > evidence_dependent_ptrs = unmarked_dependent_evidence_ptrs(node_ptr_set);
    return node_ptrs_to_evidence(evidence_dependent_ptrs);
  }

  virtual ~CutoutGraph() {
    evidence_node_ptrs.clear();
    // DiGraph<K>::clear() will be called by the grandparent destructor
  }

  virtual void clear() {
    evidence_node_ptrs.clear();
    MarkableDiGraph<K>::clear();
  }
  virtual void add_node(const NoEvidenceCutoutGraphNode<K> & noE) {
    MarkableDiGraph<K>::add_node(noE);
  }
  virtual void add_node(const EvidenceCutoutGraphNode<K,N> & E) {
    MarkableDiGraph<K>::add_node(E);
    typename MarkableDiGraph<K>::NodePtr base_ptr = (*this)[ E.get_key() ];
    evidence_node_ptrs.insert( dynamic_cast<EvidenceCutoutGraphNode<K,N>*>(base_ptr) );
  }
  virtual void remove_node_ptr(EvidenceCutoutGraphNode<K,N>* e_ptr) {
    evidence_node_ptrs.erase(e_ptr);
    MarkableDiGraph<K>::remove_node_ptr(e_ptr);
  }
  virtual void remove_node_ptr(NoEvidenceCutoutGraphNode<K>* ne_ptr) {
    MarkableDiGraph<K>::remove_node_ptr(ne_ptr);
  }

  void set_axis_labels(std::array<std::string, N> axis_labels_param) {
    axis_labels_ = axis_labels_param;
  }
  const std::array<std::string, N> & get_axis_labels() const {
    return axis_labels_;
  }

protected:
  std::vector<std::array<double, N> > node_ptrs_to_evidence(const std::set<EvidenceCutoutGraphNode<K,N>* > & evidence_node_ptrs) const {
    std::vector<std::array<double, N> > result;
    for (const EvidenceCutoutGraphNode<K,N> * e_ptr : evidence_node_ptrs)
      if (not e_ptr->is_marked())
	result.push_back( e_ptr->get_evidence() );
    return result;
  }

  std::set<EvidenceCutoutGraphNode<K,N>* > evidence_ptr_subset(const std::set<typename MarkableDiGraph<K>::NodePtr> & node_ptr_set) const {
    std::set<EvidenceCutoutGraphNode<K,N>* > evidence_node_ptr_subset;
    // take the intersection of node_ptr_set and evidence_node_ptrs
    // (cannot use sintersection because the sets do not use the same
    // type: one uses markable nodes and the other uses subtype
    // evidence nodes)

    for (typename MarkableDiGraph<K>::Node * n_ptr : node_ptr_set) {
      try {
	EvidenceCutoutGraphNode<K,N> * e_ptr = & dynamic_cast<EvidenceCutoutGraphNode<K,N> &>(*n_ptr);
	evidence_node_ptr_subset.insert(e_ptr);
      }
      catch (const std::bad_cast & bc_e) {
	// this node is not an evidence node; don't add to the
	// evidence descendent_ptrs
      }
    }
    return evidence_node_ptr_subset;
  }

  std::set<EvidenceCutoutGraphNode<K,N>* > evidence_node_ptrs;
  std::array<std::string, N> axis_labels_;
};

#endif
