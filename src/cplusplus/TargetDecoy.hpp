#ifndef _TARGETDECOY_H
#define _TARGETDECOY_H

#include "KeyComparable.hpp"
#include "GroupRanking.hpp"
#include "SetOperations.hpp"

// forward declaration
template <typename K>
class GroupRanking;

template <typename K>
class TargetDecoy
{
public:
  bool contains_target(const std::set<K> & group) const;
  bool contains_decoy_and_no_targets(const std::set<K> & group) const;

  std::vector<std::pair<int, int> > roc(const GroupRanking<K> & gr) const;
  std::vector<std::pair<double, int> > fdr_vs_targets(const GroupRanking<K> & gr) const;
  std::vector<std::pair<double, int> > q_value_vs_targets(const GroupRanking<K> & gr) const;
  static double get_fdr_from_roc_point(const std::pair<int, int> & decoy_and_target_count);

  std::pair<int,int> number_target_and_decoy_matches(const std::set<K> & obj_set) const;
  std::pair<int, int> number_target_and_decoy_matches_from_single_group(const std::set<K> & group) const;
  std::pair<int, int> number_target_and_decoy_group_matches(const std::set<std::set<K> > & group_obj_set) const;

  std::set<K> target_matches(const std::set<K> & key_set) const;
  std::set<K> decoy_matches(const std::set<K> & key_set) const;

  virtual bool is_target(const K & key) const = 0;
  virtual bool is_decoy(const K & key) const = 0;
};

#include "TargetDecoy.cpp"

#endif
