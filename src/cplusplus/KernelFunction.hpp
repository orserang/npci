#ifndef _KERNELFUNCTION_H
#define _KERNELFUNCTION_H

template <typename DOUBLEVAL, typename OBJECTVAL>
class KernelFunction
{
public:
  virtual inline DOUBLEVAL operator()(const OBJECTVAL & lhs, const OBJECTVAL & rhs) const = 0;
  inline DOUBLEVAL squared_dist(const OBJECTVAL & x1, const OBJECTVAL & x2) const;
};

// the following forces specialilzation for arrays (but it isn't significantly faster)

// template <typename DOUBLEVAL, std::size_t N>
// class KernelFunction<DOUBLEVAL, std::array<double, N> >
// {
// public:
//   virtual inline DOUBLEVAL operator()(const std::array<double, N> & lhs, const std::array<double, N> & rhs) const = 0;
//   inline DOUBLEVAL squared_dist(const std::array<double, N> & lhs, const std::array<double, N> & rhs) const;
// };

#include "KernelFunction.cpp"

#endif
