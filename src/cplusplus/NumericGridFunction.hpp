#ifndef _NUMERICGRIDFUNCTION_H
#define _NUMERICGRIDFUNCTION_H

#include <algorithm>

#include "NumericRange.hpp"
#include "StandardExceptions.hpp"
#include "Functional.hpp"

template <typename DOUBLEVAL, std::size_t N>
class NumericGridFunction
{
protected:
  NumericRange<N> numeric_range_;
  std::vector<DOUBLEVAL> values_;
public:
  typedef typename std::vector<DOUBLEVAL>::iterator iterator;
  typedef typename std::vector<DOUBLEVAL>::const_iterator const_iterator;

  NumericGridFunction() {}
  NumericGridFunction(const NumericRange<N> & nr_param, const std::vector<DOUBLEVAL> & values_param);

  const_iterator cbegin() const { return values_.cbegin(); }
  const_iterator cend() const { return values_.cend(); }

  iterator begin() { return values_.begin(); }
  iterator end() { return values_.end(); }

  const NumericGridFunction & operator +=(const NumericGridFunction & rhs);
  const NumericGridFunction & operator -=(const NumericGridFunction & rhs);

  template <typename Functor>
  const NumericGridFunction<DOUBLEVAL, N> & vector_operation(NumericGridFunction<DOUBLEVAL, N> rhs) {
    if ( !(this->numeric_range_ == rhs.numeric_range_) )
      throw SizeMismatchException("Vector operations require equal dimension");
    // note: possibly unoptimized: may perform a = (a += b) on each element
    std::transform(begin(), end(), rhs.cbegin(), begin(), Functor() );
    return *this;
  }

  const NumericRange<N> & get_numeric_range() const { return numeric_range_; }
  const std::vector<DOUBLEVAL> get_values() const { return values_; }
  size_t size() const { return numeric_range_.size(); }

  DOUBLEVAL definite_integral() const;

  void print() const;
};

#include "NumericGridFunction.cpp"

#endif
