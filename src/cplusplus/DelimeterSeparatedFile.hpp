#ifndef _DELIMETERSEPARATEDFILE_H
#define _DELIMETERSEPARATEDFILE_H

#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>

#include "StringOperations.hpp"

class DelimeterSeparatedFile {
public:
  DelimeterSeparatedFile(const std::string & fname, char delim, const std::vector<std::string> & query_field_names);
  const std::vector<std::vector<std::string> > get_query_value_rows() const { return query_value_rows_; }
  const std::vector<std::string> get_query_field_names() const { return query_field_names_; }

  std::size_t num_rows() const {
    return query_value_rows_.size();
  }
  
protected:
  std::string fname;
private:
  std::map<std::string, std::size_t> field_to_column_number;

  void init_field_to_column_number(const std::vector<std::string> & headers_strings);
  
  char delim_;
  std::vector<std::string> query_field_names_;
  std::vector<std::vector<std::string> > query_value_rows_;
};

#endif
