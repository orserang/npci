#ifndef _BASECUTOUTANALYSIS_H
#define _BASECUTOUTANALYSIS_H

#include <list>
#include <vector>
#include <fstream>
#include <iostream>

#include "LogDouble.hpp"

#include "BaseReport.hpp"

// defines cutout analysis functionality that does not use <N>. this
// is important to allow different replicates to inherit from
// BaseCutoutAnalysis but still have different dimension. It also
// allows MultiReplicateCutoutAnalysis to inherit shared
// functionality.

template <typename K>
class BaseCutoutAnalysis
{
public:
  BaseCutoutAnalysis(std::shared_ptr<TargetDecoy<K> > td_ptr_param, std::list<std::shared_ptr<GroupRanking<K> > > gr_ptr_list_param, const std::string & label_param):
    td_ptr_(td_ptr_param),
    gr_ptr_list_(gr_ptr_list_param),
    label_(label_param)
  { }

  template <typename> friend class MultiReplicateCutoutAnalysis;

  std::shared_ptr<TargetDecoy<K> > get_target_decoy_ptr() const {
    return td_ptr_;
  }
  const std::list<std::shared_ptr<GroupRanking<K> > > & get_group_ranking_ptr_list() const {
    return gr_ptr_list_;
  }
  const std::string & get_label() const {
    return label_;
  }

  virtual BaseReport generate_report() const = 0;

protected:
  BaseCutoutAnalysis() { }

  // functions for retrieving raw results:
  virtual std::list<std::vector<LogDouble> > get_all_likelihoods() const = 0;

  std::list<std::vector<std::pair<int, int> > > all_rocs() const {
    std::list<std::vector<std::pair<int, int> > > result;
    for (std::shared_ptr<GroupRanking<K> > gr_ptr : BaseCutoutAnalysis<K>::gr_ptr_list_) {
      result.push_back(BaseCutoutAnalysis<K>::td_ptr_->roc(*gr_ptr));
    }
    return result;
  }

  std::shared_ptr<TargetDecoy<K> > td_ptr_;
  std::list<std::shared_ptr<GroupRanking<K> > > gr_ptr_list_;

  std::string label_;
};

#endif
