#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <numeric>
 
#include "PrefixSuffixTargetDecoy.hpp"
#include "PlainTextSetTargetDecoy.hpp"
#include "PlainTextRanking.hpp"
#include "GroupRanking.hpp"

#include "ProteinQuantificationGraph.hpp"
#include "ProteomicsGraphGrouperRegistry.hpp"

#include "SingleReplicateCutoutAnalysis.hpp"
#include "MultiReplicateCutoutAnalysis.hpp"

#include "DirichletSmoothedLikelihoodSimilarity.hpp"
#include "IIDSmoothedLikelihoodSimilarity.hpp"
#include "KullbackLeiblerSmoothedLikelihoodSimilarity.hpp"
#include "KolmogorovSmirnovLikelihoodSimilarity.hpp"

Ranking<std::string> merge_rankings(const Ranking<std::string> & a, const Ranking<std::string> & b) {
  std::map<double, std::set<std::string> > score_to_object_set;
  
  const std::map<double, std::set<std::string> > & score_to_object_set_a = a.get_double_to_scored_object_set_map(), & score_to_object_set_b = b.get_double_to_scored_object_set_map();

  std::map<std::string, double> a_to_score, b_to_score, both_to_score;
  for (std::pair<double, std::set<std::string> > score_and_set : score_to_object_set_a)
    for (std::string str : score_and_set.second)
      a_to_score[str] = score_and_set.first / 2.0;
  for (std::pair<double, std::set<std::string> > score_and_set : score_to_object_set_b)
    for (std::string str : score_and_set.second)
      b_to_score[str] = score_and_set.first / 2.0;

  // compute sum of a_to_score and b_to_score
  for (std::pair<std::string, double> key_and_score: a_to_score) {
    double total_score = key_and_score.second + b_to_score.find(key_and_score.first)->second;
    both_to_score[key_and_score.first] = total_score;
  }

  // propagate sum into score_to_object set
  for (std::pair<std::string, double> key_and_score: both_to_score) {
    score_to_object_set[key_and_score.second].insert(key_and_score.first);
  }

  return Ranking<std::string>(score_to_object_set, a.get_label());
}

int main(int argc, char**argv) {
  if (argc >= 2 ) {

    std::cout.precision(10);

    std::cerr << "Loading Graph" << std::endl;

    // std::string caseStr = "Quant 3 Raw";
    // std::string ctrlStrA = "Quant 1 Raw";;
    // std::string ctrlStrB = "Quant 2 Raw";

    // std::string caseStr = "SY5Y TrkB Treated - BDNF Raw";
    // std::string ctrlStrA = "SY5Y TrkB Treated - Inh Raw";;
    // std::string ctrlStrB = "SY5Y TrkB Treated - NGF Raw";

    std::string caseStr = "SY5Y TrkB Treated - BDNF";
    std::string ctrlStrA = "SY5Y TrkB Treated - Inh";;
    std::string ctrlStrB = "SY5Y TrkB Treated - NGF";

    // This graph is used for npCI:
    std::shared_ptr<TMTFoldChangeGraph> pqg_ptr( new TMTFoldChangeGraph(argv[1], ',', {{caseStr, ctrlStrA}}, {{ctrlStrA, ctrlStrB}}, 0.1, 0.1) );

    // This graph is used for ranking the proteins (if no rankings are
    // given as command line arguemtns, then these rankings are fed
    // into the npCI analysis):
    std::shared_ptr<TMTIntensityScatterGraph> pqg_scatter_ptr( new TMTIntensityScatterGraph(argv[1], ',', {{caseStr, ctrlStrA}}, {{ctrlStrA, ctrlStrB}}, 0.0, 1) );

    // Group the proteins (for both graphs)
    ProteomicsGraphGrouperRegistry<TMTFoldChangeGraph> grouper;
    grouper.group_proteins_from_name(*pqg_ptr, "identical_connectivity");

    ProteomicsGraphGrouperRegistry<TMTIntensityScatterGraph> scatter_grouper;
    scatter_grouper.group_proteins_from_name(*pqg_scatter_ptr, "identical_connectivity");

    // Load a target-decoy object containing rough labels of true and
    // false positives (essentially a sanity check for building ROC
    // results). The current implementation adds the suffix ":DCY" to
    // proteins in the control-control graph (those proteins are not
    // expected exhibit a substantial change in quantity). The
    // target-decoy object could also be constructed using an external
    // set of labels (e.g. using gene ontology annotation).
    std::cerr << "Loading TargetDecoy" << std::endl;
    std::set<std::string> all_protein_keys;
    const auto & node_ptr_set = pqg_ptr->get_protein_node_ptr_set();
    for (const auto & node_ptr : node_ptr_set)
      all_protein_keys.insert(node_ptr->get_key());

    std::shared_ptr<TargetDecoy<std::string> > td_ptr( new PrefixSuffixTargetDecoy("", ":DCY", all_protein_keys) );

    std::cerr << "Creating and Grouping k-peptide Rankings" << std::endl;
    std::list<std::shared_ptr<GroupRanking<std::string> > > g_rk_ptr_lst;

    if (argc > 2) {
      /* Use plain text rankings provided via command line */
      std::cerr << "\tusing plain text rankings (provided in arguments)" << std::endl;
      for (int i=2; i<argc; ++i) {
	std::shared_ptr<GroupRanking<std::string> > g_rk_ptr( new GroupRanking<std::string>( PlainTextRanking<std::string>(argv[i] ), pqg_ptr->get_grouped_keys())) ;
	g_rk_ptr_lst.push_back( g_rk_ptr );
      }
    }

    else {
      /* Use peptide rule rankings */
      std::cerr << "\tusing peptide rule rankings" << std::endl;

      // Only perform a 1-peptide level ranking (i.e. do not discard
      // proteins with fewer than two peptides).
      const int maximum_peptide_rule = 1;
      for (int i=1; i<=maximum_peptide_rule; ++i) {
	auto merged_ranking = pqg_ptr->geom_mean_k_peptide_ranking(i, true);

	// To merge rankings from multiple replicates, use the following
	//	auto merged_ranking = merge_rankings(pqg_ptr->geom_mean_k_peptide_ranking(i, true), pqg_ptr2->geom_mean_k_peptide_ranking(i, true));

	// Downsample factor of 3 saves every third index in the series (to save time and disk space)
	const int downsample_factor = 3;
	// Do not go more than 1000 points out in the series
	// (i.e. save time and disk space by assuming that there are
	// <=1000 true identifications to be found):
	const int upper_index_limit = 1000;
	std::shared_ptr<GroupRanking<std::string> > geom_mean_g_rk_shared_ptr( new GroupRanking<std::string> (merged_ranking.downsampled(downsample_factor, upper_index_limit), pqg_ptr->get_grouped_keys()) );

	g_rk_ptr_lst.push_back( geom_mean_g_rk_shared_ptr );
      }
    }
    
    // specifies that probability density functions should be saved
    // (takes more time and disk space, but gives extra animations in
    // the final chart).
    bool save_pdfs = true;

    // 100 is the number of buckets for numeric integration, 10 is the
    // minimum number of "leftover" peptide data points for which
    // analysis will even be performed (i.e. don't try making a
    // nonparametric distribution with fewer than 10 points).

    std::cerr << "Performing CutoutAnalysis" << std::endl;
    std::shared_ptr<LikelihoodSimilarity<2> > similarity_engineA( new DirichletSmoothedLikelihoodSimilarity<2>(50, 10, save_pdfs) );
    std::shared_ptr<BaseCutoutAnalysis<ProteomicsKey> > replicateA_ptr( new SingleReplicateCutoutAnalysis<ProteomicsKey, 2>(pqg_scatter_ptr, td_ptr, g_rk_ptr_lst, similarity_engineA, "Replicate 1") );

    // ...
    // More replicate experiments would go here (e.g. defining
    // variable replicateB_ptr), and could be merged in for a larger
    // analysis
    // ...

    // Merge together all replicates performed: 
    std::list<std::shared_ptr<BaseCutoutAnalysis<ProteomicsKey> > > single_rep_list{replicateA_ptr};

    // If more replicates are used, include them instead in this fashion:
    //    std::list<std::shared_ptr<BaseCutoutAnalysis<ProteomicsKey> > > single_rep_list{replicateA_ptr, replicateB_ptr, ..., replicateZ_ptr};

    MultiReplicateCutoutAnalysis<ProteomicsKey> mca(single_rep_list, "All replicates");

    std::cerr << "Generating and Writing Report" << std::endl;
    std::cout << mca.generate_report() << std::endl;
  }
  else
    std::cerr << "usage:\tnpci-quant <TMT TSV file> [ ranking1... ]" << std::endl;
}

