#ifndef _KOLMOGOROVSMIRNOVLIKELIHOODSIMILARITY_H
#define _KOLMOGOROVSMIRNOVLIKELIHOODSIMILARITY_H

#include "UnsmoothedLikelihoodSimilarity.hpp"

// note: could be put in a more standard location if it recurs
const double PI = 3.141592653589793238462;

template <std::size_t N>
class KolmogorovSmirnovLikelihoodSimilarity;

// specialize so that KolmogorovSmirnovLikelihoodSimilarity can only
// be used with N=1
template <>
class KolmogorovSmirnovLikelihoodSimilarity<1> : public UnsmoothedLikelihoodSimilarity<1> {
public:
  KolmogorovSmirnovLikelihoodSimilarity(const std::list< std::array<KroneckerHistogramCollection<1>, 2> > & kronicker_collection_lists, int minimum_data_param):
    UnsmoothedLikelihoodSimilarity<1>(kronicker_collection_lists, minimum_data_param) { }

  KolmogorovSmirnovLikelihoodSimilarity(int minimum_data_param):
    UnsmoothedLikelihoodSimilarity<1>(minimum_data_param) {
  }

protected:
  virtual LogDouble unsmoothed_likelihood_function(const KroneckerHistogram<std::array<double, 1> > & lhs_hist, int lhs_n, const KroneckerHistogram<std::array<double, 1> > & rhs_hist, int rhs_n) const {
    // note: technically, this assumes n is large enough to use the
    // approximate CDF

    double two_sample_n_factor = sqrt(lhs_n * rhs_n / (lhs_n + rhs_n));
    double ks = get_ks_statistic(lhs_hist, lhs_n, rhs_hist, rhs_n);

    double scaled_ks = two_sample_n_factor*ks;

    // alternative approximate formulation
    //    return spss_ks_test(scaled_ks);

    // the complement of the p-value can be used as a likelihood
    LogDouble p_value = LogDouble(1.0) - asymptotic_ks_cdf(scaled_ks);
    return p_value;
  }

  static LogDouble spss_approximate_ks_cdf(double z) {
    if (z < 0.27) return LogDouble(1.0);
    if (z < 1) {
      LogDouble log_q(-1.233701 / (z*z));
      LogDouble q = exp(log_q);
      return LogDouble(1.0) - LogDouble(2.506628)/LogDouble(z) * (q + pow(q, LogDouble(9.0)) + pow(q, LogDouble(25.0)) );
    }
    if (z < 3.1) {
      LogDouble log_q(-2*z*z);
      LogDouble q = exp(log_q);
      LogDouble(2.0)*(q - pow(q, LogDouble(4.0)) + pow(q, LogDouble(9.0)) - pow(q, LogDouble(16.0)));
    }
    return LogDouble(0.0);
  }

  static LogDouble asymptotic_ks_cdf(double ks) {
    LogDouble multiplier = pow(LogDouble(2*PI), LogDouble(0.5)) / LogDouble(ks);

    LogDouble sum(0.0);

    const int max_iterations_for_summation = 100;
    for (int k=1; k<max_iterations_for_summation; ++k) {
      // exp( -(2*k-1)^2 pi^2 / (8x^2) )
      LogDouble log_term = LogDouble(2*k-1)*LogDouble(2*k-1) * LogDouble(PI)*LogDouble(PI) / ( LogDouble(8)*LogDouble(ks)*LogDouble(ks) );
      LogDouble term = exp( -log_term );
      sum += term;
    }

    return multiplier*sum;
  }
private:
  // note: because the KroneckerHistogram uses map instead of
  // unordered_map, then the items do not need to be sorted each
  // iteration (that would make it faster to call repeatedly). this
  // shouldn't matter much, though; the KS test is really only done
  // for comparison.
  double get_ks_statistic(const KroneckerHistogram<std::array<double, 1> > & lhs_hist, int lhs_n, const KroneckerHistogram<std::array<double, 1> > & rhs_hist, int rhs_n) const {

    // compute the CDF for each histogram
    std::map<std::array<double, 1>, double> lhs_cdf = hist_to_cdf(lhs_hist);
    std::map<std::array<double, 1>, double> rhs_cdf = hist_to_cdf(rhs_hist);

    // return the maximum absolute difference between the two
    // CDFs. note: this could be performed more elegantly by first
    // makeing each cdf map contain the union of the keys of both.
    double ks = 0;
    for (auto lhs_rhs_iter = make_tuple(lhs_cdf.begin(), rhs_cdf.begin()); std::get<0>(lhs_rhs_iter) != lhs_cdf.end() && std::get<1>(lhs_rhs_iter) != rhs_cdf.end(); ) {

      auto & lhs_iter = std::get<0>(lhs_rhs_iter);
      auto & rhs_iter = std::get<1>(lhs_rhs_iter);

      double current_absolute_gap = fabs(lhs_iter->second - rhs_iter->second);

      // update ks statistic using the current gap
      ks = std::max(ks, current_absolute_gap);

      // advance the one that will get to the closer
      // next point; this guarantees that each point will always be
      // compared to its two closest points, ensuring the maximum
      // difference.
      auto lhs_iter_next = std::next(lhs_iter);
      auto rhs_iter_next = std::next(rhs_iter);

      // if either next point is end(), advance the other
      if ( lhs_iter_next == lhs_cdf.end() || rhs_iter_next == rhs_cdf.end() ) {
	if ( lhs_iter_next == lhs_cdf.end() && rhs_iter_next != rhs_cdf.end())
	  ++rhs_iter;
	else if ( rhs_iter_next == rhs_cdf.end() && lhs_iter_next != lhs_cdf.end())
	  ++lhs_iter;
	else
	  // both are the final in their list; advancing either will break
	  ++lhs_iter;
      }
      else {
	// when neither next point is end(), choose the nearest
	if ( lhs_iter_next->first < rhs_iter_next->first )
	  ++lhs_iter;
	else
	  // in the case where the next points tie, advancing just one is fine
	  ++rhs_iter;
      }
    }
    return ks;
  }

  // use ordered map to make sorting afterward unnecessary (iteration
  // on ordered map is in sorted order)
  std::map<std::array<double, 1>, double> hist_to_cdf(const KroneckerHistogram<std::array<double, 1> > & hist) const {
    // sort by copying into an ordered map
    std::map<std::array<double, 1>, int> points_to_counts;
    for ( auto val : hist )
      points_to_counts.insert(val);

    std::map<std::array<double, 1>, double> cdf;
    double n = double(hist.get_n());
    int cumulative_count = 0;

    for (const std::pair<const std::array<double, 1>, int> & point_and_count : points_to_counts) {
      cumulative_count += point_and_count.second;
      cdf.insert( std::pair<std::array<double, 1>, double>(point_and_count.first, cumulative_count / n) );
    }
    return cdf;
  }
};

#endif
