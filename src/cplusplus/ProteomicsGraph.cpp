template <class EVIDENCENODE>
const ProteomicsGraph<EVIDENCENODE> & ProteomicsGraph<EVIDENCENODE>::operator =(const ProteomicsGraph<EVIDENCENODE> & rhs) {
  if ( this != & rhs )
    {
      clear();
      copy_from_rhs(rhs);
    }
  return *this;
}

template <class EVIDENCENODE>
void ProteomicsGraph<EVIDENCENODE>::copy_from_rhs(const ProteomicsGraph<EVIDENCENODE> & rhs) {
  // add nodes
  for (ProteinGroupNodePtr prot_group_ptr : rhs.protein_group_node_ptrs)
    add_node( *prot_group_ptr );
  for (ProteinNodePtr prot_ptr : rhs.protein_node_ptrs)
    add_node( *prot_ptr );
  for (PeptideNodePtr pep_ptr : rhs.peptide_node_ptrs)
    add_node( *pep_ptr );
  for (EVIDENCENODEPtr evidence_ptr : rhs.evidence_node_ptrs)
    add_node( *evidence_ptr );

  this->copy_edges_from_rhs(rhs);
}

template <class EVIDENCENODE>
std::list<std::set<ProteomicsKey> > ProteomicsGraph<EVIDENCENODE>::get_grouped_keys() const {
  std::list<std::set<ProteomicsKey> > result;
  for (const ProteinGroupNodePtr group_node_ptr : protein_group_node_ptrs) {
    std::set<ProteomicsKey> key_set;
    for (const BaseNodePtr base_node_ptr : group_node_ptr->get_successor_ptrs()) {
      key_set.insert( base_node_ptr->get_key() );
    }
    result.push_back( key_set );
  }
  return result;
}

template <class EVIDENCENODE>
void ProteomicsGraph<EVIDENCENODE>::clear() {
  CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION>::clear();
  protein_group_node_ptrs.clear();
  protein_node_ptrs.clear();
  peptide_node_ptrs.clear();
  evidence_node_ptrs.clear();
}

template <class EVIDENCENODE>
void ProteomicsGraph<EVIDENCENODE>::add_node(const ProteinGroupNode & protein_group_node) {
  CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION>::add_node(protein_group_node);
  BaseNodePtr protein_group_base_ptr = CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION>::operator[](protein_group_node.get_key());
  protein_group_node_ptrs.insert( & dynamic_cast<ProteinGroupNode &>( *protein_group_base_ptr ) );
}

template <class EVIDENCENODE>
void ProteomicsGraph<EVIDENCENODE>::add_node(const ProteinNode & protein_node) {
  CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION>::add_node(protein_node);
  BaseNodePtr protein_base_ptr = CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION>::operator[](protein_node.get_key());
  protein_node_ptrs.insert( & dynamic_cast<ProteinNode &>( *protein_base_ptr ) );
}

template <class EVIDENCENODE>
void ProteomicsGraph<EVIDENCENODE>::add_node(const PeptideNode & peptide_node) {
  CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION>::add_node(peptide_node);
  BaseNodePtr peptide_base_ptr = CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION>::operator[](peptide_node.get_key());
  peptide_node_ptrs.insert( & dynamic_cast<PeptideNode &>( *peptide_base_ptr ) );
}

template <class EVIDENCENODE>
void ProteomicsGraph<EVIDENCENODE>::add_node(const EVIDENCENODE & evidence_node) {
  CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION>::add_node(evidence_node);
  BaseNodePtr evidence_base_ptr = CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION>::operator[](evidence_node.get_key());
  evidence_node_ptrs.insert( & dynamic_cast<EVIDENCENODE &>( *evidence_base_ptr ) );
}

template <class EVIDENCENODE>
void ProteomicsGraph<EVIDENCENODE>::remove_node_ptr(ProteinGroupNodePtr protein_group_node_ptr) {
  CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION>::remove_node_ptr( protein_group_node_ptr );
  protein_group_node_ptrs.erase( protein_group_node_ptr );
}

template <class EVIDENCENODE>
void ProteomicsGraph<EVIDENCENODE>::remove_node_ptr(ProteinNodePtr protein_node_ptr) {
  CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION>::remove_node_ptr( protein_node_ptr );
  protein_node_ptrs.erase( protein_node_ptr );
}

template <class EVIDENCENODE>
void ProteomicsGraph<EVIDENCENODE>::remove_node_ptr(PeptideNodePtr peptide_node_ptr) {
  CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION>::remove_node_ptr( peptide_node_ptr );
  peptide_node_ptrs.erase( peptide_node_ptr );
}

template <class EVIDENCENODE>
void ProteomicsGraph<EVIDENCENODE>::remove_node_ptr(EVIDENCENODEPtr evidence_node_ptr) {
  CutoutGraph<ProteomicsKey, EVIDENCENODE::DIMENSION>::remove_node_ptr( evidence_node_ptr );
  evidence_node_ptrs.erase( evidence_node_ptr );
}
