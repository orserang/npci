#ifndef _NPCIEXCEPTIONS_H
#define _NPCIEXCEPTIONS_H

#include "StandardExceptions.hpp"

struct InvalidGroupingException : public StandardException {
  InvalidGroupingException(const std::string & str) throw():
    StandardException(str) { }
};

struct IncompatibleFilesException : public StandardException {
  IncompatibleFilesException(const std::string & str) throw():
    StandardException(str) { }
};

#endif
