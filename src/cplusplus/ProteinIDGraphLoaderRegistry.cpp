#include "ProteinIDGraphLoaderRegistry.hpp"

ProteinIDGraphLoaderRegistry::ProteinIDGraphLoaderRegistry()
{
  ProteinIDGraphLoaderRegistry::register_name(std::string("text:plain"), std::shared_ptr<ProteinIDGraphLoader>(new PlainTextProteinIDGraphLoader));
  ProteinIDGraphLoaderRegistry::register_name(std::string("text:ProteinPilot"), std::shared_ptr<ProteinIDGraphLoader>(new ProteinPilotProteinIDGraphLoader));
}

void ProteinIDGraphLoaderRegistry::load_from_file(ProteinIDGraph & pg, const FileAndFormat & faf) const
{
  if ( faf.has_format() )
    load_from_file_with_format(pg, faf);
  else
    load_from_file_without_format(pg, faf);
}

void ProteinIDGraphLoaderRegistry::load_from_file_with_format(ProteinIDGraph & pg, const FileAndFormat & faf) const
{
  const std::string & format = faf.get_format();
  if ( not Registry<ProteinIDGraphLoader>::contains(format) )
    throw RegistryException("File format \"" + format + "\" isn't registered");
  Registry<ProteinIDGraphLoader>::lookup_name(format)->load_and_verify(pg, faf.get_file());
}

const std::string & ProteinIDGraphLoaderRegistry::load_from_file_without_format(ProteinIDGraph & pg, const FileAndFormat & faf) const {
  std::cerr << "autodetecting format " << std::endl;
  std::list<std::string> valid_formats;
  for ( const std::pair<std::string, std::shared_ptr<ProteinIDGraphLoader> > & name_and_loader : ProteinIDGraphLoaderRegistry::get_name_to_object_ptr() )
    {
      // must use tester because unsuccessful files may introduce
      // changes to pg as they attempt to read
      ProteinIDGraph tester;
      try {
	name_and_loader.second->load_and_verify(tester, faf.get_file());
      }
      catch (...) {
	continue;
      }
      valid_formats.push_back(name_and_loader.first);
    }
  if ( valid_formats.size() == 1 )
    {
      // this file can be loaded successfully. may also want to
      // check if either the file or graph is empty
      const std::string & format = valid_formats.front();
      load_from_file_with_format(pg, FileAndFormat(faf.get_file(), format));
      std::cerr << "format was: " << format << std::endl;
      return format;
    }
  else if ( valid_formats.size() > 1 )
    {
      std::string valid_formats_string;
      for (const std::string & format : valid_formats)
	valid_formats_string += format + " ";
      throw AmbiguousFormatException(valid_formats_string);
    }
  else
    throw FormatException(std::string("No file formats match file " + faf.get_file()));
}


