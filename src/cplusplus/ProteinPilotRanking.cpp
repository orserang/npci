#include "ProteinPilotRanking.hpp"

ProteinPilotRanking::ProteinPilotRanking(const std::string & filename):
  Ranking<std::string>(filename)
{
  // constructs the ranking from the protein scores in ProteinSummary.txt

  std::vector<std::string> header_names{ "Accession", "Total"};
  TabSeparatedFile tsv(filename, header_names);

  for (const std::vector<std::string> & value_row : tsv.get_query_value_rows()) {
    const std::string & accession = value_row[0];
    double score = from_string<double>(value_row[1]);
	  
    Ranking<std::string>::score_to_scored_object_set[score].insert(accession);
  }

  Ranking<std::string>::create_best_sets_first_from_map();
}
