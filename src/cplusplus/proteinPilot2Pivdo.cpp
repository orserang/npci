#include "TabSeparatedFile.hpp"
#include <string>
#include <algorithm>

int main(int argc, char ** argv) {
  if (argc < 2) {
    std::cerr << "usage: ProteinPilot_peptide_summary.tsv" << std::endl;
    exit(1);
  }

  std::cout.precision(10);

  TabSeparatedFile tsf(argv[1], std::vector<std::string>{"Accessions", "Sequence", "Conf"});
  
  for (const std::vector<std::string> & values_at_row : tsf.get_query_value_rows()) {
    std::string peptide_sequence = values_at_row[1];
    double score = from_string<double>(values_at_row[2]);
    
    std::cout << "e " << peptide_sequence << std::endl;
    
    // remove spaces so that ';' is the delimeter between proteins
    std::string all_protein_accessions = values_at_row[0];
    all_protein_accessions.erase(std::remove(all_protein_accessions.begin(), all_protein_accessions.end(), ' '), all_protein_accessions.end());
    
    for (const std::string & protein_accession : split(all_protein_accessions, ';') )
      std::cout << "r " << protein_accession << std::endl;
    
    std::cout << "p " << score/100.0 << std::endl;
  }
}
