template <typename DOUBLEVAL, std::size_t N>
UnscaledNumericDensity<DOUBLEVAL, N>::UnscaledNumericDensity(const KroneckerHistogram<std::array<double, N> > & kh, const NumericRange<N> & nr_param, const std::array<double, N> & sigma)
{
  NumericGridFunction<DOUBLEVAL, N>::numeric_range_ = nr_param;

  // smooth using the kernel
  GaussianKernelFunction<DOUBLEVAL, std::array<double, N> > similarity(sigma);
  for ( const std::array<double, N> & x: NumericGridFunction<DOUBLEVAL, N>::numeric_range_ )
    {
      DOUBLEVAL total_mass_at_x(0.0);
      for ( const std::pair<std::array<double, N>, int> & p : kh )
	// DOUBLEVAL must be able to be constructed from a double
	total_mass_at_x += DOUBLEVAL(double(p.second)) * similarity(p.first, x);
      
      // note: for performance, can size the result at beginning,
      // and fill instead of using push_back
      UnscaledNumericDensity::values_.push_back(total_mass_at_x);
    }
}

template <typename DOUBLEVAL, std::size_t N>
UnscaledNumericDensity<DOUBLEVAL, N> UnscaledNumericDensity<DOUBLEVAL, N>::create_unsmoothed(const KroneckerHistogram<std::array<double, N> > & kh, const NumericRange<N> & nr_param)
{
  UnscaledNumericDensity<DOUBLEVAL, N> result;
  result.numeric_range_ = nr_param;

  std::array<double, N> arbitrary_sigma;
  arbitrary_sigma.fill(1.0);
  GaussianKernelFunction<DOUBLEVAL, std::array<double, N> > similarity(arbitrary_sigma);

  KroneckerHistogram<std::array<double, N> > rounded_values_to_counts;
  for ( const std::pair<std::array<double, N>, int> & p : kh ) {
    // find the closest point to p.first and put mass there (note:
    // this could be done much faster by defining a closest_point_to_x
    // function in NumericRange)
    int counts_at_max_similarity = 0;
    DOUBLEVAL max_similarity(0.0);
    std::array<double, N> point_with_max_similarity;
    for ( const std::array<double, N> & x: nr_param ) {
      DOUBLEVAL point_similarity = similarity(p.first, x);
      if ( point_similarity > max_similarity ) {
        max_similarity = point_similarity;
        counts_at_max_similarity = p.second;
        point_with_max_similarity = x;
      }
    }
    rounded_values_to_counts.add_value( point_with_max_similarity, counts_at_max_similarity );
  }
  for ( const std::array<double, N> & x: nr_param ) {
    if ( rounded_values_to_counts.contains_key(x) )
      result.values_.push_back( DOUBLEVAL(double(rounded_values_to_counts.find(x)->second)) );
    else
      result.values_.push_back( DOUBLEVAL(0.0) );
  }

  return result;
}

template <typename T, std::size_t N>
std::array<T, N> operator -(const std::array<T, N> & lhs, const std::array<T, N> & rhs) {
  std::array<T, N> result = lhs;
  for (std::size_t k=0; k<rhs.size(); ++k) {
    result[k] -= rhs[k];
  }
  return result;
}

template <typename T, std::size_t N>
std::array<T, N> operator +(const std::array<T, N> & lhs, const std::array<T, N> & rhs) {
  std::array<T, N> result = lhs;
  for (std::size_t k=0; k<rhs.size(); ++k) {
    result[k] += rhs[k];
  }
  return result;
}

template <typename T, std::size_t N>
std::array<T, N> operator /(const std::array<T, N> & lhs, const std::array<T, N> & rhs) {
  std::array<T, N> result;
  for (std::size_t k=0; k<lhs.size(); ++k)
    result[k] = lhs[k] / rhs[k];
  return result;
}

template <typename T, std::size_t N>
T operator *(const std::array<T, N> & lhs, const std::array<T, N> & rhs) {
  T result = T(0.0);
  for (std::size_t k=0; k<lhs.size(); ++k)
    result += lhs[k] * rhs[k];
  return result;
}
