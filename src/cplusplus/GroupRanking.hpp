#ifndef _GROUPRANKING_H
#define _GROUPRANKING_H

#include <sstream>
#include <string>

#include "Ranking.hpp"
#include "TargetDecoy.hpp"
#include "SetOperations.hpp"

// forward declaration
template <typename K>
class TargetDecoy;

// for actual use in TargetDecoy, etc.: the philosophy is that
// everything is grouped (ungrouped simply implies a trivial grouping).
template <typename K>
class GroupRanking : public Ranking< std::set<K> >
{
public:
  typedef typename Ranking<std::set<K> >::DoubleToObjectSet DoubleToGroupSet;

  GroupRanking(const std::string & label_param = ""):
    Ranking< std::set<K> >(label_param)
  { }

  // groups w/ trivial grouping:
  GroupRanking(const Ranking<K> & r);

  // groups using the specified sets:
  GroupRanking(const Ranking<K> & r, const std::list<std::set<K> > & grouped_keys);

protected:
  void init_from_ranking_and_object_to_group(const Ranking<K> & r, std::map<K, std::set<K> > object_to_group);
  static typename GroupRanking<K>::DoubleToGroupSet group_map(const typename Ranking<K>::DoubleToObjectSet & ungrouped_map, const std::map<K, std::set<K> > & object_to_group);
};

#include "GroupRanking.cpp"

#endif
