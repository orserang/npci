#ifndef _SETTARGETDECOY_H
#define _SETTARGETDECOY_H

#include "TargetDecoy.hpp"

template <typename K>
class SetTargetDecoy : public TargetDecoy<K> {
public:
  SetTargetDecoy(std::set<K> & targets_param, std::set<K> & decoys_param):
    targets(targets_param), decoys(decoys_param)
  { }

  SetTargetDecoy() { }

  virtual ~SetTargetDecoy() { }

  virtual bool is_target(const K & key) const {
    return targets.count(key) > 0;
  }

  virtual bool is_decoy(const K & key) const {
    return decoys.count(key) > 0;
  }

  const std::set<K> & get_target_set() const {
    return targets;
  }

  const std::set<K> & get_decoy_set() const {
    return decoys;
  }

protected:
  std::set<K> targets, decoys;
};

#endif
