#ifndef _IIDSMOOTHEDLIKELIHOODSIMILARITY_H
#define _IIDSMOOTHEDLIKELIHOODSIMILARITY_H

#include "SmoothedLikelihoodSimilarity.hpp"

template <std::size_t N>
class IIDSmoothedLikelihoodSimilarity : public SmoothedLikelihoodSimilarity<N> {
public:
  IIDSmoothedLikelihoodSimilarity(const std::list< std::array<KroneckerHistogramCollection<N>, 2> > & kronicker_collection_lists, int num_bins, int minimum_data_param, bool save_distributions):
    SmoothedLikelihoodSimilarity<N>(kronicker_collection_lists, num_bins, minimum_data_param, save_distributions) {
    this->init_best_sigmas();
  }

  IIDSmoothedLikelihoodSimilarity(int num_bins_param, int minimum_data_param, bool save_distributions):
    SmoothedLikelihoodSimilarity<N>(num_bins_param, minimum_data_param, save_distributions) {
    // if no data is provided, don't bother initializing sigma
  }

  virtual std::shared_ptr<SmoothedLikelihoodSimilarity<1> > clone_without_data() const {
    return std::shared_ptr<SmoothedLikelihoodSimilarity<1> >( new IIDSmoothedLikelihoodSimilarity<1>(SmoothedLikelihoodSimilarity<N>::num_bins_, SmoothedLikelihoodSimilarity<N>::minimum_data_, SmoothedLikelihoodSimilarity<N>::save_distributions_) );
  }

protected:
  virtual LogDouble smoothed_likelihood_function(const KroneckerHistogram<std::array<double, N> > & lhs_hist, const SmoothedHistogram<LogDouble, N> & lhs_hist_smoothed, const KroneckerHistogram<std::array<double, N> > & rhs_hist, const SmoothedHistogram<LogDouble, N> & rhs_hist_smoothed, const std::array<double, N> & sigma) const {
        if ( ! (lhs_hist_smoothed.get_numeric_range() == rhs_hist_smoothed.get_numeric_range()) )
      throw SizeMismatchException("The numeric ranges integrated by the IIDLikelihoodFunctor must be identical");
    // circular: uses both as background
    LogDouble result = directed_iid_likelihood(lhs_hist_smoothed, SmoothedHistogram<LogDouble,N>(UnscaledNumericDensity<LogDouble, N>::create_unsmoothed(rhs_hist, rhs_hist_smoothed.get_numeric_range()), rhs_hist_smoothed.get_n()) ) * directed_iid_likelihood(rhs_hist_smoothed, SmoothedHistogram<LogDouble,N>(UnscaledNumericDensity<LogDouble, N>::create_unsmoothed(lhs_hist, lhs_hist_smoothed.get_numeric_range()), lhs_hist_smoothed.get_n()) );
    
    // directed: uses decoy as background
    //    LogDouble result = directed_iid_likelihood(rhs_hist_smoothed, SmoothedHistogram<LogDouble,N>(UnscaledNumericDensity<LogDouble, N>::create_unsmoothed(lhs_hist, lhs_hist_smoothed.get_numeric_range()), lhs_hist_smoothed.get_n()) );
    return result;
  }
private:
  LogDouble directed_iid_likelihood(const SmoothedHistogram<LogDouble, N> & bg, const SmoothedHistogram<LogDouble, N> & fg) const {
    LogDouble result(1.0);
    for (auto iter_pair = std::make_pair(bg.cbegin(), fg.cbegin()); iter_pair != std::make_pair(bg.cend(), fg.cend()); ++iter_pair.first, ++iter_pair.second ) {
      LogDouble fg_hist_val = *iter_pair.second;

      // for terms where both base and exponent approach zero, the
      // product is not affected (LogDouble will ignore
      // these). Effectively, these values are not in the
      // support). hence, for readability, only consider values with
      // nonzero fg_hist_val (the if statement is not necessary
      // because LogDouble already handles it)
      if ( fg_hist_val > LogDouble(0.0) ) {
	// rather than rescale the histograms to make the PDF, simply
	// divide each histogram value by n (since the integral was n,
	// it will become 1 by linearity)
	LogDouble bg_hist_val = *iter_pair.first;
	LogDouble bg_pdf_val = bg_hist_val / LogDouble(bg.get_n());
	//      LogDouble fg_pdf_val = fg_hist_val / LogDouble(fg.get_n());

	// compute likelihood term
	const NumericRange<N> & nr = fg.get_numeric_range();
	LogDouble term = pow( pow(bg_pdf_val, fg_hist_val), nr.template delta_product<LogDouble>() );
	result *= term;

	if ( LogDouble::is_nan(term) )
	  throw NumericalStabilityException("Encountered nan in IIDLikelihoodFunctor");
      }
    }

    return result;
  }
};

#endif
