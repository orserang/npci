#ifndef _PROTEINQUANTIFICATION
#define _PROTEINQUANTIFICATION

#include "ProteomicsGraph.hpp"
#include "TabSeparatedFile.hpp"
#include "Ranking.hpp"
#include "SetOperations.hpp"

template <std::size_t N>
class PSMQuantNode : public EvidenceCutoutGraphNode<ProteomicsKey, N> {
public:
  PSMQuantNode<N>(const ProteomicsKey & string_param, std::array<double, N> score_tuple):
    EvidenceCutoutGraphNode<ProteomicsKey, N>(string_param, score_tuple) { }

  void set_score(const std::array<double, N> & score_param) { EvidenceCutoutGraphNode<ProteomicsKey, N>::set_evidence(score_param); }
  virtual void print() const {
    std::cout << KeyComparable<ProteomicsKey>::get_key() << " ";
    std::cout << this->get_evidence() << " ";
    std::cout << std::endl;
  }

  virtual PSMQuantNode<N> * clone() const {
    return new PSMQuantNode<N>(*this);
  }
};

// fixme: rankings are only appropriate for fold change graph
template <std::size_t N>
class ProteinQuantificationGraph : public ProteomicsGraph<PSMQuantNode<N> > {
public:
  ProteinQuantificationGraph():
    intensity_threshold(0.1), pseudo_count(0.1) { }

  // a hack because protected members can't be reached using inheritance
  double intensity_threshold;
  double pseudo_count;
protected:

  // fixme: use geometric mean?
  double average_value_for_column(const TabSeparatedFile & tsv, int index) const {
    // compute the average intensities for both columns to normalize

    // fixme: doesn't operate after values are removed; should only
    // operate on the used values (right?)
    double sum = 0.0;
    int num_counted = 0;
    for (const std::vector<std::string> & value_row : tsv.get_query_value_rows()) {
      // Exclude any row with a blank entry
      if (value_row[index] == "")
	continue;

      double val = from_string<double>(value_row[index]);
      sum += val;
      ++num_counted;
    }

    return sum / num_counted;
  }
};

// note: does not remove contaminants, which may result in
// differentially regulated evidence present in the control-control
// sample (note that this is, in some ways, a good thing because it
// should also be true for the case-control sample). but it may make
// the protein ranking problem more difficult.
template <std::size_t N>
class TMTProteinQuantificationGraph : public ProteinQuantificationGraph<N> {
public:
  TMTProteinQuantificationGraph() { }
  TMTProteinQuantificationGraph(const ProteinQuantificationGraph<N> & rhs) { copy_from_rhs(rhs); }
  TMTProteinQuantificationGraph(const std::string & fname, char protein_delim, std::array<std::string, 2> case_channel_names, std::array<std::string, 2> control_channel_names) { load_from_file(fname, protein_delim, case_channel_names, control_channel_names); }

protected:
  void load_from_file(const std::string & fname, char protein_delim, std::array<std::string, 2> case_channel_names, std::array<std::string, 2> control_channel_names) {
    std::vector<std::string> header_names{ "Sequence", "Accession Numbers", "Prob"};

    std::vector<std::string> case_and_control_channel_names;
    case_and_control_channel_names.insert(case_and_control_channel_names.end(), case_channel_names.begin(), case_channel_names.end());
    case_and_control_channel_names.insert(case_and_control_channel_names.end(), control_channel_names.begin(), control_channel_names.end());
    
    // extend header_names with channel_names
    header_names.insert(header_names.end(), case_and_control_channel_names.begin(), case_and_control_channel_names.end());

    TabSeparatedFile tsv(fname, header_names);

    std::array<double, 4> channel_averages;
    
    // Note: this code exploits the fact that the channel intensities
    // are 3...6 (it's a bit of a hack)
    for (int k=0; k<4; ++k)
      channel_averages[k] = this->average_value_for_column(tsv, k+3);

    std::cerr << "Adding lines..." << std::endl;
    int numberOfAddedLines = 0;
    for (const std::vector<std::string> & value_row : tsv.get_query_value_rows()) {
      // the TSV file should prepend all node keys with whether they
      // belong to the target or decoy graph (this is a special case
      // where the two graphs do not crosstalk). take care that the
      // prepended key survives to_unique_peptide_sequence (which is
      // called on peptides).
      
      bool lineWasAdded = add_line_from_tsv(value_row, channel_averages, protein_delim);
      if (lineWasAdded)
	++numberOfAddedLines;
    }

    if ( numberOfAddedLines == 0 )
      throw FormatException("No lines found in file; all lines contain an empty cell for a field with a needed intensity channel");

    std::cerr << "Added " << numberOfAddedLines << " rows out of " << tsv.get_query_value_rows().size() << " possible" << std::endl;
  }

  // return value indicates whether the line was successfully added
  // (because it doesn't lack any necessary fields)
  bool add_line_from_tsv(std::vector<std::string> value_row, const std::array<double, 4> & channel_averages, char protein_delim) {
    const std::string & peptide_seq = value_row[0];
    const std::string & proteins = value_row[1];

    // fixme: old way; restrictive
    // if value_row contains any blank entries, skip it
    //    if ( find(value_row.begin(), value_row.end(), "") != value_row.end() )
    //      return false;

    // Remove the "%" symbol (for scaffold compatability). this is why
    // value_row must be passed by value, to allow replace to modify
    // it.
    replace(value_row[2], "%", "");
    double score = from_string<double>(value_row[2]);

    std::vector<std::string> all_proteins_for_peptide = split(proteins, protein_delim);

    bool anyNonBlankPair = false;
    if (value_row[3] != "" && value_row[4] != "") {
      anyNonBlankPair = true;
      double case_ch1 = from_string<double>(value_row[3]) / channel_averages[0];
      double case_ch2 = from_string<double>(value_row[4]) / channel_averages[1];
      add_peptide_proteins_psm(peptide_seq, all_proteins_for_peptide, score, case_ch1, case_ch2, "");
    }
    if (value_row[5] != "" && value_row[6] != "") {
      anyNonBlankPair = true;
      double control_ch1 = from_string<double>(value_row[5]) / channel_averages[2];
      double control_ch2 = from_string<double>(value_row[6]) / channel_averages[3];
      add_peptide_proteins_psm(peptide_seq, all_proteins_for_peptide, score, control_ch1, control_ch2, ":DCY");
    }

    return anyNonBlankPair;
  }

  virtual void add_peptide_proteins_psm(const std::string & peptide_seq, std::vector<std::string> all_proteins_for_peptide, double score, double ch1, double ch2, const std::string & suffix) = 0;
};

class TMTFeatureGraph : public TMTProteinQuantificationGraph<3> {
public:
  TMTFeatureGraph() {}
  TMTFeatureGraph(const ProteinQuantificationGraph<3> & rhs) { copy_from_rhs(rhs); }

  TMTFeatureGraph(const std::string & fname, char protein_delim, std::array<std::string, 2> case_channel_names, std::array<std::string, 2> control_channel_names) {
    //    TMTProteinQuantificationGraph<3>(fname, protein_delim, case_channel_names, control_channel_names)

    // fixme: sloppy (doesn't call parent constructor). this is
    // because the overridden method isn't available in the parent
    // constructor, and will call a pure virtual function.
    load_from_file(fname, protein_delim, case_channel_names, control_channel_names);
  }

  void dump_protein_features() const {
    // note: includes shared peptides
    
    // for every protein, compute number of supporting PSMs, average
    // intensity (foreground and background), average (over PSMs)
    // log fold change, 
    std::cout << "protein\tnum_psms\tavg_score\tavg_log_fold_change\tavg_log_intensity" << std::endl;
    const auto & prot_node_ptr_set = this->get_protein_node_ptr_set();
    for (const auto prot_node_ptr : prot_node_ptr_set) {
      const auto & psm_node_ptr_set = unmarked_descendent_evidence_ptrs(std::set<typename ProteomicsGraph<PSMQuantNode<3> >::NodePtr >{prot_node_ptr});

      std::size_t num_counted_psms = 0;

      // note: doesn't work with zero intensities
      double avg_log_fold_change = 0.0;
      double avg_log_intensity = 0.0;
      double avg_score = 0.0;
      for (const EvidenceCutoutGraphNode<ProteomicsKey, 3> * psm_node_ptr : psm_node_ptr_set) {
  	// note: needs to be updated when the dimension of the evidence changes
  	double score = psm_node_ptr->get_evidence()[0];
  	double ch1 = psm_node_ptr->get_evidence()[1];
  	double ch2 = psm_node_ptr->get_evidence()[2];

  	if (ch1 < this->intensity_threshold || ch2 < this->intensity_threshold)
  	  continue;
	
  	double log_fold_change = log(ch2 + this->pseudo_count) - log(ch1 + this->pseudo_count);

  	avg_log_fold_change += log_fold_change;
  	avg_log_intensity += (log(ch1) + log(ch2))/2.0;
  	avg_score += score;

  	++num_counted_psms;
      }

      if (num_counted_psms == 0)
  	continue;

      avg_log_fold_change /= num_counted_psms;
      avg_log_intensity /= num_counted_psms;
      avg_score /= num_counted_psms;

      std::cout << prot_node_ptr->get_key() << "\t" << num_counted_psms << "\t" << avg_score << "\t" << fabs(avg_log_fold_change) << "\t" << avg_log_intensity << std::endl;
    }
  }
  
protected:
  void add_peptide_proteins_psm(const std::string & peptide_seq, std::vector<std::string> all_proteins_for_peptide, double score, double ch1, double ch2, const std::string & suffix) {
    PeptideNode peptide_node(peptide_seq + suffix);

    std::size_t spectrum_number = get_evidence_node_ptr_set().size();
    PSMQuantNode<3> psm_intensity_node("spectrum_" + to_string(spectrum_number) + suffix, std::array<double, 3>{{ score, ch1, ch2 }} );

    // only add peptide and PSM nodes if there are any associated proteins
    if (all_proteins_for_peptide.size() == 0)
      return;

    add_node(peptide_node);
    add_node(psm_intensity_node);
    add_edge_between_keys(peptide_node.get_key(), psm_intensity_node.get_key());

    for (const std::string & prot : all_proteins_for_peptide) {
      ProteinNode protein_node(prot + suffix);

      add_node(protein_node);
      add_edge_between_keys(protein_node.get_key(), peptide_node.get_key());
    }
  }
};

class TMTFoldChangeGraph : public TMTProteinQuantificationGraph<1> {
public:
  TMTFoldChangeGraph() {}
  TMTFoldChangeGraph(const ProteinQuantificationGraph<1> & rhs) { copy_from_rhs(rhs);
    intensity_threshold = rhs.intensity_threshold;
    pseudo_count = rhs.pseudo_count;
  }

  TMTFoldChangeGraph(const std::string & fname, char protein_delim, std::array<std::string, 2> case_channel_names, std::array<std::string, 2> control_channel_names, double intensity_threshold_param, double pseudo_count_param) {
    TMTProteinQuantificationGraph<1>::intensity_threshold = intensity_threshold_param;
    TMTProteinQuantificationGraph<1>::pseudo_count = pseudo_count_param;

    //    TMTProteinQuantificationGraph<3>(fname, protein_delim, case_channel_names, control_channel_names)

    // fixme: sloppy (doesn't call parent constructor). this is
    // because the overridden method isn't available in the parent
    // constructor, and will call a pure virtual function.
    load_from_file(fname, protein_delim, case_channel_names, control_channel_names);
  }

  // making k-peptide ranking (of proteins with k peptides over score
  // threshold, rank by average absolute log fold change)
  Ranking<ProteomicsKey> geom_mean_k_peptide_ranking(std::size_t k, bool allow_shared) {
    // note: clears markings
    this->unmark_all();

    // note: includes shared peptides
    std::map<double, std::set<ProteomicsKey> > log_abs_fold_change_to_proteins;
    
    // for every protein, if it has >= k peptides, compute log
    // absolute fold change and add to ranking
    const auto & prot_node_ptr_set = this->get_protein_node_ptr_set();
    for (const auto prot_node_ptr : prot_node_ptr_set) {

      const auto & psm_node_ptr_set = this->unmarked_descendent_evidence_ptrs(std::set<typename ProteomicsGraph<PSMQuantNode<1> >::NodePtr >{prot_node_ptr});
      if (psm_node_ptr_set.size() >= k) {
	double log_fold_change = 0.0;
	for (const EvidenceCutoutGraphNode<ProteomicsKey, 1> * psm_node_ptr : psm_node_ptr_set) {

	  // a PSM should have exactly one predecessor
	  if ( ! allow_shared && (*psm_node_ptr->get_predecessor_ptrs().begin())->get_predecessor_ptrs().size() > 1)
	    continue;

	  // note: needs to be updated when the dimension of the evidence changes

	  log_fold_change += psm_node_ptr->get_evidence()[0];
	}
	log_fold_change /= psm_node_ptr_set.size();

	log_abs_fold_change_to_proteins[ fabs(log_fold_change) ].insert(prot_node_ptr->get_key());
      }
    }

    std::string name = to_string(k) + " (geometric) mean PSM fold change";
    if (allow_shared)
      name += " w/ shared";
    else
      name += " no shared";
      
    return Ranking<ProteomicsKey>(log_abs_fold_change_to_proteins, name);
  }

  Ranking<ProteomicsKey> max_fold_k_peptide_ranking(std::size_t k) {
    // note: clears markings
    this->unmark_all();

    // note: includes shared peptides
    std::map<double, std::set<ProteomicsKey> > log_abs_fold_change_to_proteins;
    
    // for every protein, if it has >= k peptides, compute log
    // absolute fold change and add to ranking
    const auto & prot_node_ptr_set = this->get_protein_node_ptr_set();
    for (const auto prot_node_ptr : prot_node_ptr_set) {
      const auto & psm_node_ptr_set = this->unmarked_descendent_evidence_ptrs(std::set<typename ProteomicsGraph<PSMQuantNode<1> >::NodePtr >{prot_node_ptr});

      if (psm_node_ptr_set.size() >= k) {
	double max_magnitude_log_fold_change = 0.0;
	for (const EvidenceCutoutGraphNode<ProteomicsKey, 1> * psm_node_ptr : psm_node_ptr_set) {
	  // note: needs to be updated when the dimension of the evidence changes

	  max_magnitude_log_fold_change = std::max(max_magnitude_log_fold_change, fabs(psm_node_ptr->get_evidence()[0]));
	}

	log_abs_fold_change_to_proteins[ max_magnitude_log_fold_change ].insert(prot_node_ptr->get_key());
      }
    }

    return Ranking<ProteomicsKey>(log_abs_fold_change_to_proteins, to_string(k) + " max PSM fold change");
  }

protected:
  void add_peptide_proteins_psm(const std::string & peptide_seq, std::vector<std::string> all_proteins_for_peptide, double score, double ch1, double ch2, const std::string & suffix) {

    if (ch1 < intensity_threshold || ch2 < intensity_threshold)
      return;

    // throws away very low probability PSMs (prob < 0.1)
    //    if (score < 10)
    //      return;

    PeptideNode peptide_node(peptide_seq + suffix);

    std::size_t spectrum_number = get_evidence_node_ptr_set().size();

    double log_fold_change = log(ch2 + this->pseudo_count) - log(ch1 + this->pseudo_count);

    PSMQuantNode<1> psm_intensity_node("spectrum_" + to_string(spectrum_number) + suffix, std::array<double, 1>{{ log_fold_change }} );

    // only add peptide and PSM nodes if there are any associated proteins
    if (all_proteins_for_peptide.size() == 0)
      return;

    add_node(peptide_node);
    add_node(psm_intensity_node);
    add_edge_between_keys(peptide_node.get_key(), psm_intensity_node.get_key());

    for (const std::string & prot : all_proteins_for_peptide) {
      ProteinNode protein_node(prot + suffix);

      add_node(protein_node);
      add_edge_between_keys(protein_node.get_key(), peptide_node.get_key());
    }
  }
};

class TMTFoldChangeAndIntensityGraph : public TMTProteinQuantificationGraph<2> {
public:
  TMTFoldChangeAndIntensityGraph() {}
  TMTFoldChangeAndIntensityGraph(const ProteinQuantificationGraph<2> & rhs) { copy_from_rhs(rhs); }

  TMTFoldChangeAndIntensityGraph(const std::string & fname, char protein_delim, std::array<std::string, 2> case_channel_names, std::array<std::string, 2> control_channel_names, double intensity_threshold_param, double pseudo_count_param) {

    this->intensity_threshold = intensity_threshold_param;
    this->pseudo_count = pseudo_count_param;
    //    TMTProteinQuantificationGraph<3>(fname, protein_delim, case_channel_names, control_channel_names)

    // fixme: sloppy (doesn't call parent constructor). this is
    // because the overridden method isn't available in the parent
    // constructor, and will call a pure virtual function.
    load_from_file(fname, protein_delim, case_channel_names, control_channel_names);
  }

protected:
  void add_peptide_proteins_psm(const std::string & peptide_seq, std::vector<std::string> all_proteins_for_peptide, double score, double ch1, double ch2, const std::string & suffix) {

    if (ch1 < intensity_threshold || ch2 < intensity_threshold)
      return;

    // throws away very low probability PSMs (prob < 0.1)
    //    if (score < 10)
    //      return;

    PeptideNode peptide_node(peptide_seq + suffix);

    std::size_t spectrum_number = get_evidence_node_ptr_set().size();

    double log_fold_change = log(ch2 + pseudo_count) - log(ch1 + pseudo_count);
    double average_log_intensity = (log(ch2 + pseudo_count) + log(ch1 + pseudo_count))/2.0;

    PSMQuantNode<2> psm_intensity_node("spectrum_" + to_string(spectrum_number) + suffix, std::array<double, 2>{{ log_fold_change, average_log_intensity }} );

    // only add peptide and PSM nodes if there are any associated proteins
    if (all_proteins_for_peptide.size() == 0)
      return;

    add_node(peptide_node);
    add_node(psm_intensity_node);
    add_edge_between_keys(peptide_node.get_key(), psm_intensity_node.get_key());

    for (const std::string & prot : all_proteins_for_peptide) {
      ProteinNode protein_node(prot + suffix);

      add_node(protein_node);
      add_edge_between_keys(protein_node.get_key(), peptide_node.get_key());
    }
  }
};

class TMTIntensityScatterGraph : public TMTProteinQuantificationGraph<2> {
public:
  TMTIntensityScatterGraph() {}
  TMTIntensityScatterGraph(const ProteinQuantificationGraph<2> & rhs) { copy_from_rhs(rhs); }

  TMTIntensityScatterGraph(const std::string & fname, char protein_delim, std::array<std::string, 2> case_channel_names, std::array<std::string, 2> control_channel_names, double intensity_threshold_param, double pseudo_count_param) {
    this->intensity_threshold = intensity_threshold_param;
    this->pseudo_count = pseudo_count_param;
    //    TMTProteinQuantificationGraph<3>(fname, protein_delim, case_channel_names, control_channel_names)

    // fixme: sloppy (doesn't call parent constructor). this is
    // because the overridden method isn't available in the parent
    // constructor, and will call a pure virtual function.
    load_from_file(fname, protein_delim, case_channel_names, control_channel_names);
  }

protected:
  void add_peptide_proteins_psm(const std::string & peptide_seq, std::vector<std::string> all_proteins_for_peptide, double score, double ch1, double ch2, const std::string & suffix) {

    if (ch1 < this->intensity_threshold || ch2 < this->intensity_threshold)
      return;

    // The following line throws away very low probability PSMs (prob < 0.1)
    //    if (score < 10)
    //      return;

    PeptideNode peptide_node(peptide_seq + suffix);

    std::size_t spectrum_number = get_evidence_node_ptr_set().size();

    double log_ch1 = log(ch1 + this->pseudo_count);
    double log_ch2 = log(ch2 + this->pseudo_count);

    PSMQuantNode<2> psm_intensity_node("spectrum_" + to_string(spectrum_number) + suffix, std::array<double, 2>{{ log_ch1, log_ch2 }} );

    // only add peptide and PSM nodes if there are any associated proteins
    if (all_proteins_for_peptide.size() == 0)
      return;

    add_node(peptide_node);
    add_node(psm_intensity_node);
    add_edge_between_keys(peptide_node.get_key(), psm_intensity_node.get_key());

    for (const std::string & prot : all_proteins_for_peptide) {
      ProteinNode protein_node(prot + suffix);

      add_node(protein_node);
      add_edge_between_keys(protein_node.get_key(), peptide_node.get_key());
    }
  }
};

/* does not currently compile */
/* class SILACProteinQuantificationGraph : public ProteinQuantificationGraph<1> {
public:
  SILACProteinQuantificationGraph() {}
  SILACProteinQuantificationGraph(const ProteinQuantificationGraph & rhs) { copy_from_rhs(rhs); }

  SILACProteinQuantificationGraph(const std::string & case_fname, char protein_delim, const std::string & control_fname, const std::string & contaminant_prefix) {
    load_from_file(case_fname, protein_delim, control_fname, contaminant_prefix);
  }

private:
  void load_from_file(const std::string & case_fname, char protein_delim, const std::string & control_fname, const std::string & contaminant_prefix) {
    std::vector<std::string> header_names{ "Sequence", "Proteins", "PEP", "Intensity H", "Intensity L"};
    TabSeparatedFile case_tsv(case_fname, header_names);
    TabSeparatedFile control_tsv(control_fname, header_names);

    insert_single_TSV(case_tsv, "", contaminant_prefix);
    // append all decoy nodes with ":DCY", creating two disjoint
    // graphs (note that this prefix must be safe from peptide
    // sequence editing, e.g. 'I- --> 'L')
    insert_single_TSV(control_tsv, ":DCY", contaminant_prefix);
  }

  void insert_single_TSV(const TabSeparatedFile & tsv, const std::string & suffix, const std::string & contaminant_prefix) {
    int line_number = 0;

    // each column uses its average to normalize
    double avg3 = average_value_for_column(tsv, 3);
    double avg4 = average_value_for_column(tsv, 4);
    for (const std::vector<std::string> & value_row : tsv.get_query_value_rows()) {
      
      // the TSV file should prepend all node keys with whether they
      // belong to the target or decoy graph (this is a special case
      // where the two graphs do not crosstalk). take care that the
      // prepended key survives to_unique_peptide_sequence (which is
      // called on peptides).
      const std::string & peptide_seq = value_row[0];
      const std::string & proteins = value_row[1];

      double score = from_string<double>(value_row[2]);

      double intensityA = from_string<double>(value_row[3]) / avg3;
      double intensityB = from_string<double>(value_row[4]) / avg4;

      // provide a superset of all evidence, which can be used by the
      // derived class
      add_evidence_node_and_edges(proteins, peptide_seq, score, intensityA, intensityB, suffix);

      PeptideNode peptide_node(peptide_seq + suffix);
      PSMQuantNode<2> psm_intensity_node("spectrum_" + to_string(line_number) + suffix, std::array<double, 1>{{ log(ratio) }} );

      bool any_proteins_to_be_added = false;
      for (const std::string & prot : split(proteins, ';')) {
	if ( starts_with(prot, contaminant_prefix) )
	  continue;

	any_proteins_to_be_added = true;
      }

      if (any_proteins_to_be_added) {
	add_node(peptide_node);
	add_node(psm_intensity_node);
	add_edge_between_keys(peptide_node.get_key(), psm_intensity_node.get_key());
      }

      for (const std::string & prot : split(proteins, protein_delim)) {
	// ignore contaminants because they will frequently be
	// differential, and will mess up the empirical null. note:
	// this may not properly eliminate contaminants that group
	// with other proteins.
	if ( starts_with(prot, contaminant_prefix) )
	  continue;

	ProteinNode protein_node(prot + suffix);

	add_node(protein_node);
	add_edge_between_keys(protein_node.get_key(), peptide_node.get_key());
      }
      
     ++line_number;
    }
  }
};
*/

#endif

