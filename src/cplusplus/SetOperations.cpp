template <typename set_type>
bool SetCompare<set_type>::operator() (const set_type & lhs, const set_type & rhs) const
{
  //  return std::less<set_type>(lhs, rhs);

  if (lhs.size() != rhs.size())
    return lhs.size() < rhs.size();
  typename set_type::const_reverse_iterator lhs_iter = lhs.rbegin();
  typename set_type::const_reverse_iterator rhs_iter = rhs.rbegin();
  for (; lhs_iter != lhs.rend() and rhs_iter != rhs.rend(); ++lhs_iter, ++rhs_iter) {
    //    const typename set_type::key_type & lhs_val = *lhs_iter;
    //    const typename set_type::key_type & rhs_val = *rhs_iter;
    if (*lhs_iter < *rhs_iter)
      return true;
    else if (*lhs_iter > *rhs_iter)
      return false;
  }
  // the sets are equal
  return false;
}

// |= operator; faster than making a copy and performing union
template <typename set_type>
void insert_all(set_type & lhs, const set_type & rhs)
{
  for (typename set_type::iterator iter = rhs.begin(); iter != rhs.end(); ++iter)
    {
      lhs.insert(*iter);
    }
}

template <typename set_type>
set_type sunion(const set_type & lhs, const set_type & rhs)
{
  // note: may be faster to simply insert lhs and rhs
  set_type result;
  std::set_union( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::inserter(result, result.end()), typename set_type::key_compare() );
  return result;
}

template <typename set_type>
set_type sintersection(const set_type & lhs, const set_type & rhs)
{
  std::cerr << "warning: calling std::set_intersection can be slow; it's much faster to check if every element in the smaller set is in the larger set" << std::endl;
  set_type result;
  std::set_intersection( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::inserter(result, result.end()), typename set_type::key_compare() );
  return result;
}

template <typename set_type>
set_type sdifference(const set_type & lhs, const set_type & rhs)
{
  set_type result;
  std::set_difference( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::inserter(result, result.end()), typename set_type::key_compare() );
  return result;
}

template <typename T>
typename std::iterator_traits<T>::value_type sunion_over_iterator_range(T begin, T end)
{
  typename std::iterator_traits<T>::value_type result;
  for (T iter = begin; iter != end; ++iter)
    {
      insert_all(result, *iter);
    }
  return result;
}

