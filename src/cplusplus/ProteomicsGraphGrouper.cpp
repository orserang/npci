template <typename PROTEOMICSGRAPH>
void ProteomicsGraphGrouper<PROTEOMICSGRAPH>::group_proteins(PROTEOMICSGRAPH & pg) const {
  clear_groups(pg);
  std::list<std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> > protein_groups = get_protein_groups(pg);
  group_proteins_from_defined_sets(pg, protein_groups);
  check_valid_grouping(pg);
}

template <typename PROTEOMICSGRAPH>
void ProteomicsGraphGrouper<PROTEOMICSGRAPH>::group_proteins_from_defined_sets(PROTEOMICSGRAPH & pg, const std::list<std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> > & protein_group_ptrs) const {
  int number_groups = 0;
  for (const typename std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> prot_node_ptr_set : protein_group_ptrs )
    {
      ProteinGroupNode group_node("group_" + to_string(number_groups));
      for (const typename PROTEOMICSGRAPH::ProteinNodePtr prot_node_ptr : prot_node_ptr_set) {
	// if a grouped protein is not in the graph, add it
	pg.add_node(*prot_node_ptr);
	//	if ( pg.contains(*prot_node_ptr) )
	//	  {
	pg.add_node(group_node);
	pg.add_edge_between_keys(group_node.get_key(), prot_node_ptr->get_key());
	    //	  }
      }
      ++number_groups;
    }
}

template <typename PROTEOMICSGRAPH>
void ProteomicsGraphGrouper<PROTEOMICSGRAPH>::clear_groups(PROTEOMICSGRAPH & pg) const {
  // clear existing protein groups to prevent multiple grouping
  
  // make temporary copy of prot_group_ptrs so that nodes can be
  // deleted during iteration
  const typename std::set<typename PROTEOMICSGRAPH::ProteinGroupNodePtr> & prot_group_ptrs = pg.get_protein_group_node_ptr_set();
  for (typename PROTEOMICSGRAPH::ProteinGroupNodePtr protein_group_node_ptr : prot_group_ptrs)
    pg.remove_node_ptr( protein_group_node_ptr );
}

template <typename PROTEOMICSGRAPH>
void ProteomicsGraphGrouper<PROTEOMICSGRAPH>::check_valid_grouping(const PROTEOMICSGRAPH & pg) const {
  // make sure each protein belongs to exactly one group
  for (const ProteinNode* prot_ptr : pg.get_protein_node_ptr_set())
    if ( prot_ptr->get_predecessor_ptrs().size() != 1 )
      throw InvalidGroupingException("Each protein must belong to exactly one group");
}

