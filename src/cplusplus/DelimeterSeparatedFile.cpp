#include "DelimeterSeparatedFile.hpp"

DelimeterSeparatedFile::DelimeterSeparatedFile(const std::string & fname_param, char delim, const std::vector<std::string> & query_field_names):
  fname(fname_param),
  delim_(delim), query_field_names_(query_field_names)
{
  std::ifstream fin(fname);

  // get the columns for each query string of interest
  std::string header_string;
  getline(fin, header_string);
  init_field_to_column_number(split(header_string, delim_));
  
  // cache the column numbers for each query string (preventing string
  // lookup for each value of interest)
  std::vector<std::size_t> ordered_header_indices;
  for (const std::string & field : query_field_names_)
    ordered_header_indices.push_back( field_to_column_number.find(field)->second );

  // read the file and build a table of the rows for each query string
  std::size_t line_number = 1;
  std::string line_string;
  while (getline(fin, line_string)) {
    std::vector<std::string> row_for_each_query;

    std::vector<std::string> row = split(line_string, delim_);
    for (std::size_t field_index : ordered_header_indices) {
      if ( field_index > row.size()-1 )
	throw FormatException("Line " + to_string(line_number) + " in DelimiterSeparatedFile " + fname + " does not have enough columns");
      row_for_each_query.push_back( row[field_index] );
    }

    query_value_rows_.push_back(row_for_each_query);
    ++line_number;
  }
}

void DelimeterSeparatedFile::init_field_to_column_number(const std::vector<std::string> & headers_strings) {
  for (std::size_t col_num = 0; col_num < headers_strings.size(); ++col_num) {
    const std::string & field = headers_strings[col_num];

    // if the string is only whitespace, don't use as a label
    if ( first_word(field) == "" )
      continue;
    
    if ( field_to_column_number.count(field) > 0 )
      throw FormatException("Field labels multiple columns in file header: \"" + field + "\""  + " in DelimiterSeparatedFile " + fname);
    field_to_column_number[field] = col_num;
  }

  for (const std::string & query : query_field_names_)
    if ( field_to_column_number.count(std::string(query)) == 0 )
      throw FormatException("No header column for field: " + query + " in DelimiterSeparatedFile " + fname);
}
