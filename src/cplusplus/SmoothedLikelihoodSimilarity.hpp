#ifndef _SMOOTHEDLIKELIHOODSIMILARITY_H
#define _SMOOTHEDLIKELIHOODSIMILARITY_H

#include "SmoothedHistogram.hpp"
#include "LikelihoodSimilarity.hpp"
#include "UnscaledNumericDensity.hpp"
#include "IteratorUtilities.hpp"
#include "VectorPrint.hpp" // used for to_string(sigma_range_) 

// forward declaration: don't #include
// SmoothedLikelihoodSimilarityReport.hpp until after
// BaseSmoothedLikelihoodSimilarity is defined (prevents circular
// include where both need each other to be defined)
template <std::size_t N>
class SmoothedLikelihoodSimilarityReport;

template <std::size_t N>
class BaseSmoothedLikelihoodSimilarity : public LikelihoodSimilarity<N> {

  // declare corresponding report to be a friend, allowing it to see
  // kronicker_collection_lists_, etc. this is used by the report to
  // write the downsampled distributions without holding the full
  // (non-downsampled) distributions in memory.
  friend class SmoothedLikelihoodSimilarityReport<N>;
public:
  BaseSmoothedLikelihoodSimilarity(const std::list<std::array<KroneckerHistogramCollection<N>, 2> > & kronicker_collection_lists, int num_bins, int minimum_data_param, bool save_distributions):
    LikelihoodSimilarity<N>(kronicker_collection_lists, minimum_data_param),
    num_bins_(num_bins),
    save_distributions_(save_distributions) {

    init_nr();
    // does NOT call init_best_sigmas (or init_data, which calls
    // init_best_sigmas), because init_best_sigmas must be called
    // within derived classes
  }

  BaseSmoothedLikelihoodSimilarity(int num_bins, int minimum_data_param, bool save_distributions):
    LikelihoodSimilarity<N>(minimum_data_param),
    num_bins_(num_bins),
    save_distributions_(save_distributions)
  { }

  const std::array<double, N> & get_best_sigmas() const {
    return best_sigmas_;
  }

  const std::array<std::vector< std::pair<LogDouble, double> >, N> & get_negative_scores_and_sigmas() const {
    return negative_scores_and_sigmas_;
  }

  // moved to cpp to prevent circular include
  BaseReport generate_report() const;
protected:
  // accepts sigma so that a prior can be placed on it (e.g. for MDL KL divergence)
  virtual LogDouble smoothed_likelihood_function(const KroneckerHistogram<std::array<double, N> > & lhs_hist, const SmoothedHistogram<LogDouble, N> & lhs_hist_smoothed, const KroneckerHistogram<std::array<double, N> > & rhs_hist, const SmoothedHistogram<LogDouble, N> & rhs_hist_smoothed, const std::array<double, N> & sigma) const = 0;

  // used by derived classes:
  static const std::vector<double> sigma_range_;
  NumericRange<N> nr_;
  std::array<double, N> best_sigmas_;
  std::array<double, N> data_scaling_factors_;
  LogDouble maximum_likelihood_;

  void init_nr() {
    std::array<double, N> lower = get_lower();
    std::array<double, N> upper = get_upper();
    nr_ = NumericRange<N>::create_from_lower_and_upper_bounds( lower, upper, num_bins_ );

    std::array<double, N> new_deltas = nr_.get_delta();
    for (std::size_t k=0; k<N; ++k) {
      if (lower[k] == upper[k])
	new_deltas[k] = 1e-10;
    }
    nr_ = NumericRange<N>(nr_.get_lower(), nr_.get_upper() + new_deltas, new_deltas);
  }

  std::list<std::vector<LogDouble> > compute_all_likelihoods_for_sigma(std::array<double, N> sigma) const {
    std::list<std::vector<LogDouble> > result;

    bool any_collections_with_enough_data = false;
    for (const std::array<KroneckerHistogramCollection<N>, 2> & khc_pair : LikelihoodSimilarity<N>::kronicker_collection_lists_) {
      UnscaledNumericDensityCollection<N> undc0(khc_pair[0], nr_, sigma), undc1(khc_pair[1], nr_, sigma);
      
      std::vector<LogDouble> result_for_single_collection;

      // iterate through every KroneckerHistogram and the corresponding UnscaledNumericDensity
      for ( auto kh_undc_iters = make_tuple(khc_pair[0].begin(), khc_pair[1].begin(), undc0.begin(), undc1.begin());
	    std::get<0>(kh_undc_iters) != khc_pair[0].end() && std::get<1>(kh_undc_iters) != khc_pair[1].end() && std::get<2>(kh_undc_iters) != undc0.end() && std::get<3>(kh_undc_iters) != undc1.end();
	    ++std::get<0>(kh_undc_iters), ++std::get<1>(kh_undc_iters), ++std::get<2>(kh_undc_iters), ++std::get<3>(kh_undc_iters) ) {

	const KroneckerHistogram<std::array<double, N> > & kh0 = *std::get<0>(kh_undc_iters), & kh1 = *std::get<1>(kh_undc_iters);
	const UnscaledNumericDensity<LogDouble, N> & und0 = *std::get<2>(kh_undc_iters), & und1 = *std::get<3>(kh_undc_iters);

	// when there are fewer than minimum_data points, you cannot compare (use a likelihood of 0)
	if ( kh0.get_n() < LikelihoodSimilarity<N>::minimum_data_ or kh1.get_n() < LikelihoodSimilarity<N>::minimum_data_ )
	  result_for_single_collection.push_back(LogDouble(0.0));
	else {
	  LogDouble score = smoothed_likelihood_function( kh0, SmoothedHistogram<LogDouble, N>(und0, double(LikelihoodSimilarity<N>::n0_)), kh1, SmoothedHistogram<LogDouble, N>(und1, double(LikelihoodSimilarity<N>::n1_)), sigma );
	  result_for_single_collection.push_back(score);
	  any_collections_with_enough_data = true;
	}
      }
      if (! any_collections_with_enough_data)
	throw NumericalStabilityException("No collections cointained minimum number of points (" + to_string(LikelihoodSimilarity<N>::minimum_data_) + "); did you have enough data (or enough decoys?)");

      result.push_back(result_for_single_collection);
    }
    return result;
  }

  // note: should only be called within derived classes, which have the
  // information available to call init_best_sigmas
  virtual void init_data(const std::list<std::array<KroneckerHistogramCollection<N>, 2> > & kronicker_collection_lists) {
    LikelihoodSimilarity<N>::init_data(kronicker_collection_lists);
    init_nr();
    init_best_sigmas();
    LikelihoodSimilarity<N>::init_all_likelihoods();
  }

  // must be called by derived class constructors (so that the derived function is called)
  virtual void init_best_sigmas() = 0;

  virtual std::list<std::vector<LogDouble> > compute_all_likelihoods() const {
    std::list<std::vector<LogDouble> > likelihoods = compute_all_likelihoods_for_sigma(best_sigmas_);
    return LikelihoodSimilarity<N>::normalized_likelihoods( likelihoods );
  }

  // number of bins used for numeric methods (e.g. numeric integration)
  const int num_bins_;

  // scores for all sigmas (to be written to report); negative scores
  // are used so that the smaller sigma is chosen when likelihoods are
  // equal. these negative scores should be corrected (i.e. negated
  // again) before being written to report.
  std::array<std::vector< std::pair<LogDouble, double> >, N> negative_scores_and_sigmas_;

  // whether distributions should be saved when writing the report
  bool save_distributions_;

  // functions to get the box bounding all points
  std::array<double, N> get_lower() const {
    std::list< std::array<double, N>  > each_lower;
    for (const std::array<KroneckerHistogramCollection<N>, 2> & khc_pair : LikelihoodSimilarity<N>::kronicker_collection_lists_) {
      each_lower.push_back( get_lower_from_kronicker_histogram_collection(khc_pair[0]) );
      each_lower.push_back( get_lower_from_kronicker_histogram_collection(khc_pair[1]) );
    }
    return get_lower_from_collection(each_lower);
  }
  
  std::array<double, N> get_upper() const {
    std::list< std::array<double, N>  > each_upper;
    for (const std::array<KroneckerHistogramCollection<N>, 2> & khc_pair : LikelihoodSimilarity<N>::kronicker_collection_lists_) {
      each_upper.push_back( get_upper_from_kronicker_histogram_collection(khc_pair[0]) );
      each_upper.push_back( get_upper_from_kronicker_histogram_collection(khc_pair[1]) );
    }
    return get_upper_from_collection(each_upper);
  }
};

template <std::size_t N>
class SmoothedLikelihoodSimilarity : public BaseSmoothedLikelihoodSimilarity<N> {
public:
  // to construct without initializing data
  SmoothedLikelihoodSimilarity(int num_bins, int minimum_data_param, bool save_distributions):
    BaseSmoothedLikelihoodSimilarity<N>(num_bins, minimum_data_param, save_distributions)
  { }

  // to construct with data
  SmoothedLikelihoodSimilarity(const std::list< std::array<KroneckerHistogramCollection<N>, 2> > & kronicker_collection_lists, int num_bins, int minimum_data_param, bool save_distributions):
    BaseSmoothedLikelihoodSimilarity<N>(kronicker_collection_lists, num_bins, minimum_data_param, save_distributions)
  { }

  // placed in .cpp file so that specialized class
  // SmoothedLikelihoodSimilarity<1> is fully defined before it is
  // used within init_best_sigmas
  virtual void init_best_sigmas();

  virtual std::shared_ptr<SmoothedLikelihoodSimilarity<1> > clone_without_data() const = 0;
};

template <>
class SmoothedLikelihoodSimilarity<1> : public BaseSmoothedLikelihoodSimilarity<1> {
  // allow SmoothedLikelihoodSimilarity<N> to externally access
  // SmoothedLikelihoodSimilarity<1> (this is necessary when
  // SmoothedLikelihoodSimilarity<N> is estimating sigma using each
  // column)
  template <std::size_t> friend class SmoothedLikelihoodSimilarity;
public:
  SmoothedLikelihoodSimilarity(const std::list< std::array<KroneckerHistogramCollection<1>, 2> > & kronicker_collection_lists, int num_bins, int minimum_data_param, bool save_distributions):
    BaseSmoothedLikelihoodSimilarity<1>(kronicker_collection_lists, num_bins, minimum_data_param, save_distributions)
  { }
  
  // with no data provided:
  SmoothedLikelihoodSimilarity(int num_bins, int minimum_data_param, bool save_distributions):
    BaseSmoothedLikelihoodSimilarity<1>(num_bins, minimum_data_param, save_distributions)
  { }

  virtual void init_best_sigmas() {
    std::cerr << "\t\tfinding best sigma on single cross section... ";
    BaseSmoothedLikelihoodSimilarity<1>::negative_scores_and_sigmas_[0].clear();

    data_scaling_factors_[0] = this->nr_.volume<double>();

    // note: could perform three-point convex optimization instead of grid search
    for (double sigma : this->sigma_range_ ) {
      // sigma is in units relative to the data itself (if the domain
      // is scaled by 2, sigma should also be, to prevent artifacts
      // from the grid search, which may need a higher resolution to
      // get the optimal unscaled sigma * 2).
      sigma *= BaseSmoothedLikelihoodSimilarity<1>::nr_.volume<double>();

      //      std::cerr << sigma << " ";
      std::list<std::vector<LogDouble> > all_scores_for_sigma = this->compute_all_likelihoods_for_sigma(std::array<double, 1>{ {sigma} });
      std::list<LogDouble> flattened = flatten(all_scores_for_sigma);
      LogDouble best_score_for_sigma = *std::max_element( flattened.begin(), flattened.end() );
      //      std::cerr << best_score_for_sigma << std::endl;
      negative_scores_and_sigmas_[0].push_back( std::make_pair(-best_score_for_sigma, sigma) );
    }

    // use the min of the (-score, sigma) so that less smoothing is
    // chosen when the scores tie
    SmoothedLikelihoodSimilarity<1>::best_sigmas_[0] = std::min_element(negative_scores_and_sigmas_[0].begin(), negative_scores_and_sigmas_[0].end())->second;
    std::cerr << "done: " << SmoothedLikelihoodSimilarity<1>::best_sigmas_[0] << std::endl;
  }
};

#include "SmoothedLikelihoodSimilarityReport.hpp"
#include "SmoothedLikelihoodSimilarity.cpp"

#endif
