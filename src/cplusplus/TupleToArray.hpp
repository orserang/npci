#ifndef _TUPLETOARRAY_H
#define _TUPLETOARRAY_H

#include <iostream>
#include <tuple>
#include <array>

template<class First, class Tuple, std::size_t N, std::size_t K = N>
struct ArrayFiller {
  static void fill_array_from_tuple(const Tuple& t, std::array<First, N> & arr) {
    ArrayFiller<First, Tuple, N, K-1>::fill_array_from_tuple(t, arr);
    arr[K-1] = std::get<K-1>(t);
  }
};

template<class First, class Tuple, std::size_t N>
struct ArrayFiller<First, Tuple, N, 1> {
  static void fill_array_from_tuple( const Tuple& t, std::array<First, N> & arr) {
    arr[0] = std::get<0>(t);
  }
};

template<typename First, typename... Rem>
void fill_array_from_tuple(const std::tuple<First, Rem...>& t, std::array<First, 1+sizeof...(Rem)> & arr) {
  ArrayFiller<First, decltype(t), 1+sizeof...(Rem)>::fill_array_from_tuple(t, arr);
}

template<typename First, typename... Rem>
std::array<First, 1+sizeof...(Rem)> tuple_to_array(const std::tuple<First, Rem...>& t) {
  std::array<First, 1+sizeof...(Rem)> result;
  ArrayFiller<First, decltype(t), 1+sizeof...(Rem)>::fill_array_from_tuple(t, result);
  return result;
}

template<typename CastTo, typename First, typename... Rem>
std::array<CastTo, 1+sizeof...(Rem)> tuple_to_array_and_cast(const std::tuple<First, Rem...>& t) {
  std::array<CastTo, 1+sizeof...(Rem)> result;
  ArrayFiller<CastTo, decltype(t), 1+sizeof...(Rem)>::fill_array_from_tuple(t, result);
  return result;
}

// template<typename CastTo, typename Tup>
// std::array<CastTo, sizeof(Tup) > tuple_to_array2(const Tup & t) {
//   std::array<CastTo, sizeof(Tup) > result;
//   ArrayFiller<CastTo, decltype(t), sizeof(Tup) >::fill_array_from_tuple(t, result);
//   return result;
// }

#endif
