template <typename K>
GroupRanking<K>::GroupRanking(const Ranking<K> & r, const std::list<std::set<K> > & grouped_keys):
  Ranking< std::set<K> >(r.get_label())
{
  std::map<K, std::set<K> > object_to_group;
  for ( const std::set<K> & group : grouped_keys )
    for (const K & key : group)
      object_to_group[key] = group;

  init_from_ranking_and_object_to_group(r, object_to_group);
}

template <typename K>
GroupRanking<K>::GroupRanking(const Ranking<K> & r):
  Ranking< std::set<K> >(r.get_label())
{
  std::map<K, std::set<K> > object_to_group;
  const std::list<std::set<K> > & collection_list = r.get_best_sets_first();
  for ( const std::set<K> & collection : collection_list )
    for (const K & key : collection)
      object_to_group[key] = std::set<K>{key};

  init_from_ranking_and_object_to_group(r, object_to_group);
}

template <typename K>
void GroupRanking<K>::init_from_ranking_and_object_to_group(const Ranking<K> & r, std::map<K, std::set<K> > object_to_group) {
  Ranking< std::set<K> >::score_to_scored_object_set = group_map( r.get_double_to_scored_object_set_map(), object_to_group);
  
  Ranking< std::set<K> >::create_best_sets_first_from_map();
  Ranking< std::set<K> >::label_ = r.get_label();
}

template <typename K>
typename GroupRanking<K>::DoubleToGroupSet GroupRanking<K>::group_map(const typename Ranking<K>::DoubleToObjectSet & ungrouped_map, const std::map<K, std::set<K> > & object_to_group)
{
  std::set<std::set<K> > all_observed_groups;
  GroupRanking<K>::DoubleToGroupSet result;
    for ( const std::pair<double, std::set<K> > & double_and_object_set : ungrouped_map )
      {
	// convert the objects to groups
	std::set<std::set<K> > group_set;
	for ( const K & obj : double_and_object_set.second )
	  if ( object_to_group.count(obj) > 0 )
	    group_set.insert( object_to_group.find(obj)->second );
	  else
	    group_set.insert( std::set<K>{ obj } );
	
	std::set<std::set<K> > new_group_set = sdifference(group_set, all_observed_groups);
	if ( new_group_set.size() > 0 )
	  result[ double_and_object_set.first ] = new_group_set;
	insert_all( all_observed_groups, new_group_set );
      }
    return result;
}

