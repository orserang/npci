#ifndef _GAUSSIANKERNELFUNCTION_H
#define _GAUSSIANKERNELFUNCTION_H

#include "KernelFunction.hpp"

template <typename DOUBLEVAL, typename OBJECTVAL>
class GaussianKernelFunction : KernelFunction<DOUBLEVAL, OBJECTVAL>
{
protected:
  OBJECTVAL sigma_;
public:
  GaussianKernelFunction(const OBJECTVAL & sigma_param) :
    sigma_(sigma_param) { }
  // requires squared_dist defined for OBJECTVALs, /= for DOUBLEVAL,
  // and exp(DOUBLEVAL)
  virtual inline DOUBLEVAL operator()(const OBJECTVAL & lhs, const OBJECTVAL & rhs) const;
};

#include "GaussianKernelFunction.cpp"

#endif
