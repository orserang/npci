template <typename K>
PlainTextSetTargetDecoy<K>::PlainTextSetTargetDecoy(const std::string & filename)
{
  std::ifstream fin(filename);
  std::string line_string;
  while ( getline(fin, line_string) )
    {
      K accession;
      std::string type;
      std::istringstream ist(line_string);
      ist >> accession >> type;

      if ( SetTargetDecoy<K>::targets.count(accession) > 0 or SetTargetDecoy<K>::decoys.count(accession) > 0 )
	throw FormatException(to_string(accession) + " appears multiple times in target decoy");
	
      if (type == "target")
	SetTargetDecoy<K>::targets.insert( accession );
      else if (type == "decoy")
	SetTargetDecoy<K>::decoys.insert( accession );
      else
	throw FormatException("Type must be either \"target\" or \"decoy\" and was \"" + type + "\"");
    }
}

