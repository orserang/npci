#ifndef _TABSEPARATEDFILE_H
#define _TABSEPARATEDFILE_H

#include "DelimeterSeparatedFile.hpp"

class TabSeparatedFile : public DelimeterSeparatedFile {
public:
  TabSeparatedFile(const std::string & fname, const std::vector<std::string> & query_field_names):
    DelimeterSeparatedFile(fname, '\t', query_field_names) { }
};

#endif

