#ifndef _MARKABLEMIXIN_H
#define _MARKABLEMIXIN_H

class MarkableMixin
{
public:
  MarkableMixin() { marked = false; }
  void mark() { marked = true; }
  void unmark() { marked = false; }
  bool is_marked() const { return marked; }
private:
  bool marked;
};

#endif
