template <typename PROTEOMICSGRAPH>
ProteomicsGraphGrouperRegistry<PROTEOMICSGRAPH>::ProteomicsGraphGrouperRegistry() {
  ProteomicsGraphGrouperRegistry::register_name("trivial", std::shared_ptr<ProteomicsGraphGrouper<PROTEOMICSGRAPH> >(new TrivialProteomicsGraphGrouper<PROTEOMICSGRAPH>));
  ProteomicsGraphGrouperRegistry::register_name("identical_connectivity", std::shared_ptr<ProteomicsGraphGrouper<PROTEOMICSGRAPH> >(new StandardProteomicsGraphGrouper<PROTEOMICSGRAPH>));
}

template <typename PROTEOMICSGRAPH>
void ProteomicsGraphGrouperRegistry<PROTEOMICSGRAPH>::group_proteins_from_name(PROTEOMICSGRAPH & pg, const std::string & grouping_name) const {
  ProteomicsGraphGrouperRegistry::lookup_name(grouping_name)->group_proteins(pg);
}

template <typename PROTEOMICSGRAPH>
const ProteomicsGraphGrouper<PROTEOMICSGRAPH> & ProteomicsGraphGrouperRegistry<PROTEOMICSGRAPH>::lookup_grouper(const std::string & grouping_name) const {
  return *ProteomicsGraphGrouperRegistry::lookup_name(grouping_name);
}

