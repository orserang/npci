#include "StringOperations.hpp"

std::string operator +(const std::string & lhs, char*rhs)
{
  return lhs + std::string(rhs);
  
}

std::string operator +(char*lhs, const std::string & rhs)
{
  return std::string(lhs) + rhs;
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
  std::istringstream ss(s);
  std::string item;
  while(std::getline(ss, item, delim)) {
    elems.push_back(item);
  }
  return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
  std::vector<std::string> elems;
  return split(s, delim, elems);
}

std::vector<std::string> split_whitespace(const std::string &s) {
  std::istringstream ss(s);
  std::vector<std::string> elems;
  std::string str;
  while (ss >> str)
    elems.push_back(str);
  return elems;
}

std::string first_word(const std::string & s) {
  std::istringstream sin(s);
  std::string x;
  sin >> x;
  return x;
}

bool starts_with(const std::string & a, const std::string & prefix) {
    if (prefix.size() > a.size()) return false;
    return std::equal(prefix.begin(), prefix.end(), a.begin());
}

bool ends_with(const std::string & a, const std::string & suffix) {
    if (suffix.size() > a.size()) return false;
    return std::equal(suffix.begin(), suffix.end(), a.begin() + a.size() - suffix.size());
}

void replace(std::string & str, const std::string & from, const std::string& to) {
    if(from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
    }
}
