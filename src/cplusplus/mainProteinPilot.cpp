#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <numeric>
 
#include "PrefixSuffixTargetDecoy.hpp"
#include "ProteinPilotRanking.hpp"
#include "GroupRanking.hpp"
#include "ProteinIDGraphFactory.hpp"
#include "ProteomicsGraphGrouperRegistry.hpp"

#include "SingleReplicateCutoutAnalysis.hpp"
#include "MultiReplicateCutoutAnalysis.hpp"

#include "IIDSmoothedLikelihoodSimilarity.hpp"
#include "DirichletSmoothedLikelihoodSimilarity.hpp"
#include "KullbackLeiblerSmoothedLikelihoodSimilarity.hpp"
#include "KolmogorovSmirnovLikelihoodSimilarity.hpp"

int main(int argc, char**argv)
{
  if (argc == 4 ) {

    std::cout.precision(10);

    std::cerr << "Loading Graph" << std::endl;
    // create the graph by tring all file types and choosing the one
    // that doesn't throw an exception or return an empty graph. also,
    // use trivial grouping (each protein has its own group). the
    // grouping string could also be "identical_connectivity", which
    // would perform standard protein grouping.
    std::shared_ptr<ProteinIDGraph> pg_ptr = ProteinIDGraphFactory<ProteinIDGraph>::create_ungrouped_proteomics_graph(FileAndFormat(argv[1], "text:ProteinPilot"));
    Singleton<ProteomicsGraphGrouperRegistry<ProteinIDGraph> >::get_instance().group_proteins_from_name(*pg_ptr, "identical_connectivity");

    // create TargetDecoy from the graph and the ProteinPilot standard decoy prefix
    std::cerr << "Loading TargetDecoy" << std::endl;

    std::set<std::string> all_protein_keys;
    const std::set<ProteinIDGraph::ProteinNodePtr> & prot_node_ptr_set = pg_ptr->get_protein_node_ptr_set();
    for (const ProteinIDGraph::ProteinNodePtr pn_ptr : prot_node_ptr_set)
      all_protein_keys.insert(pn_ptr->get_key());
    std::shared_ptr<TargetDecoy<std::string> > td_ptr( new PrefixSuffixTargetDecoy("RRRRR", "", all_protein_keys) );

    std::cerr << "Loading and Grouping Ranking" << std::endl;
    ProteinPilotRanking rk(argv[2]);
    //    std::shared_ptr<GroupRanking<std::string> > g_rk_ptr(new GroupRanking<std::string>(rk, pg_ptr->get_grouped_keys()));
    std::shared_ptr<GroupRanking<std::string> > g_rk_ptr(new GroupRanking<std::string>(rk, pg_ptr->get_grouped_keys()));
    std::list<std::shared_ptr<GroupRanking<std::string> > > g_rk_ptr_lst{ g_rk_ptr };

    std::shared_ptr<LikelihoodSimilarity<1> > similarity_engine( new DirichletSmoothedLikelihoodSimilarity<1>(100, 10, true) );
    //    std::shared_ptr<LikelihoodSimilarity<1> > similarity_engine( new IIDSmoothedLikelihoodSimilarity<1>(100, 10, true) );

    std::cerr << "Performing CutoutAnalysis" << std::endl;
    std::string human_readable_label = argv[3];

    std::shared_ptr<BaseCutoutAnalysis<ProteomicsKey> > repA_ptr( new SingleReplicateCutoutAnalysis<ProteomicsKey, 1>(pg_ptr, td_ptr, g_rk_ptr_lst, similarity_engine, human_readable_label) );

    std::list<std::shared_ptr<BaseCutoutAnalysis<ProteomicsKey> > > single_rep_list{repA_ptr};
    MultiReplicateCutoutAnalysis<ProteomicsKey> mca(single_rep_list, human_readable_label);

    std::cerr << "Generating and Writing Report" << std::endl;
    std::cout << mca.generate_report() << std::endl;
  }
  else
    std::cerr << "usage:\tnpCI-ProteinPilot <peptides.txt> <proteinGroups.txt> <human-readable label string>" << std::endl;
}

