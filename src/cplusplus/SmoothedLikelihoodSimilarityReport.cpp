template <std::size_t N>
void SmoothedLikelihoodSimilarityReport<N>::write_all_distributions(const BaseSmoothedLikelihoodSimilarity<N> & similarity_engine, double downsample_factor) {
  // construct a downsampled NumericRange:
  std::array<double, N> downsampled_delta = similarity_engine.nr_.get_delta();
  for (std::size_t k=0; k<N; ++k)
    downsampled_delta[k] *= downsample_factor;

  // the NumericRange is constructed once for every ranking and for
  // every PDF from that ranking; thus, output the support (i.e. the
  // NumericRange) once.
  NumericRange<N> nr(similarity_engine.nr_.get_lower(), similarity_engine.nr_.get_upper(), downsampled_delta);

  rapidxml::xml_node<> * distributions_subtree = BaseReport::create_node_ptr("density_series_collection");

  rapidxml::xml_node<> * support_subtree = create_subtree_for_distribution_support(nr);
  distributions_subtree->append_node(support_subtree);

  // for each ranking iterate through smoothed distributions
    std::size_t ranking_index = 0;
  for (const std::array<KroneckerHistogramCollection<N>, 2> & khc_pair : similarity_engine.kronicker_collection_lists_) {
    UnscaledNumericDensityCollection<N> undc0(khc_pair[0], nr, similarity_engine.get_best_sigmas()), undc1(khc_pair[1], nr, similarity_engine.get_best_sigmas());
      
    rapidxml::xml_node<> * single_distribution_subtree = BaseReport::create_node_ptr("density_series");
    single_distribution_subtree->append_attribute( BaseReport::create_attribute_ptr("ranking_number", ranking_index) );
    // iterate through every UnscaledNumericDensity
    std::size_t point_index = 0;
    for ( auto undc_iters = make_tuple(undc0.begin(), undc1.begin());
	  std::get<0>(undc_iters) != undc0.end() && std::get<1>(undc_iters) != undc1.end();
	  ++std::get<0>(undc_iters), ++std::get<1>(undc_iters) ) {

      const UnscaledNumericDensity<LogDouble, N> & und0 = *std::get<0>(undc_iters), & und1 = *std::get<1>(undc_iters);

      single_distribution_subtree->append_node( create_distribution_pair_subtree(PDF<LogDouble, N>(und0), PDF<LogDouble, N>(und1), point_index) );
      ++point_index;
    }
    distributions_subtree->append_node(single_distribution_subtree);
    ++ranking_index;
  }

  BaseReport::root_->append_node(distributions_subtree);
}

