#ifndef _DIGRAPHNODE_H
#define _DIGRAPHNODE_H

#include "KeyComparable.hpp"

template <typename, typename> class DiGraph;

// note: DiGraphNode is not meant to be instantiated; instead, it
// should be inherited from in a derived class. the derived class
// should provide its own type as NodeType
template <typename K, typename NodeType>
class DiGraphNode : public KeyComparable<K> {
public:
  template <typename, typename> friend class DiGraph;

  typedef K KeyType;
  typedef NodeType Node;
  typedef NodeType* NodePtr;

  // constructors and assignment
  DiGraphNode(const K & key_param):
    KeyComparable<K>::KeyComparable(key_param) { }
  DiGraphNode(const DiGraphNode<K, NodeType> & rhs):
    KeyComparable<K>(rhs.get_key()) {
    // do not copy predecessors or successors
  }
  virtual ~DiGraphNode() {}

  const NodeType & operator=(const DiGraphNode<K, NodeType> & rhs);

  // accessors
  const std::set<NodePtr> & get_predecessor_ptrs() const { return predecessor_ptrs; }
  const std::set<NodePtr> & get_successor_ptrs() const { return successor_ptrs; }

  virtual void print() const { std::cout << KeyComparable<K>::get_key() << std::endl; }

  // must inherit and override this function
  virtual DiGraphNode<K,NodeType>* clone() const = 0;
private:
  // these would be weak_ptr (does not own the memory) to prevent
  // circular references
  std::set<NodePtr> predecessor_ptrs, successor_ptrs;

  // private modifiers: should only be used within DiGraph. classes
  // that inherit from DiGraph will need to use DiGraph-based
  // functions to access these.
  void add_edge_to_ptr(NodePtr succ);
  void remove_edge_to_ptr(NodePtr succ);

  void add_predecessor_ptr(NodePtr neighbor) { predecessor_ptrs.insert(neighbor); }
  void remove_predecessor_ptr(NodePtr neighbor) { predecessor_ptrs.erase(neighbor); }
  void add_successor_ptr(NodePtr neighbor) { successor_ptrs.insert(neighbor); }
  void remove_successor_ptr(NodePtr neighbor) { successor_ptrs.erase(neighbor); }
};

#include "DiGraphNode.cpp"

#endif
