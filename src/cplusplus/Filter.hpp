#ifndef _FILTER_H
#define _FILTER_H

#include <iostream>
#include <string>
#include <functional>
#include <algorithm>
#include <iterator>

template <
  template <typename, typename> class Container,
  typename Predicate,
  typename Allocator,
  typename A
>
Container<A, Allocator> filter(Container<A, Allocator> const & container, Predicate const & pred) {
  Container<A, Allocator> filtered(container);
  filtered.erase(remove_if(filtered.begin(), filtered.end(), pred), filtered.end());
  return filtered;
}

#endif
