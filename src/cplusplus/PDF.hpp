#ifndef _PDF_H
#define _PDF_H

#include "NonnegativeGridFunction.hpp"

template <typename DOUBLEVAL, std::size_t N>
class PDF : public NonnegativeGridFunction<DOUBLEVAL, N>
{
protected:
public:
  PDF() {}
  PDF(const NumericGridFunction<DOUBLEVAL, N> & ngf);
};

#include "PDF.cpp"

#endif
