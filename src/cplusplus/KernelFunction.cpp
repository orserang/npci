template <typename DOUBLEVAL, typename OBJECTVAL>
DOUBLEVAL KernelFunction<DOUBLEVAL, OBJECTVAL>::squared_dist(const OBJECTVAL & x1, const OBJECTVAL & x2) const
{
  OBJECTVAL unsquared = OBJECTVAL(x2 - x1);
  return DOUBLEVAL(unsquared * unsquared);
}

// template <typename DOUBLEVAL, std::size_t N>
// DOUBLEVAL KernelFunction<DOUBLEVAL, std::array<double, N> >::squared_dist(const std::array<double, N> & x1, const std::array<double, N> & x2) const
// {
//   DOUBLEVAL result = DOUBLEVAL(0.0);
//   for (int k=0; k<N; ++k) {
//     DOUBLEVAL temp(x1[k] - x2[k]);
//     result += temp*temp;
//   }
//   return result;
// }

