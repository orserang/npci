#ifndef _PYTHONMAPOPERATIONS_H
#define _PYTHONMAPOPERATIONS_H

// to behave as python does
template <typename MAP>
std::list<typename MAP::key_type> keys(const MAP & a_map) {
  std::list<typename MAP::key_type> result;
  for (const auto & a_pair : a_map)
    result.push_back(a_pair.first);
  return result;
}

template <typename MAP>
std::list<typename MAP::mapped_type> values(const MAP & a_map) {
  std::list<typename MAP::mapped_type> result;
  for (const auto & a_pair : a_map)
    result.push_back(a_pair.second);
  return result;
}

template <typename MAP>
std::list<typename MAP::value_type> items(const MAP & a_map) {
  std::list<typename MAP::value_type> result;
  for (const auto & a_pair : a_map)
    result.push_back(a_pair);
  return result;
}

#endif

