#ifndef _PLAINTEXTSETTARGETDECOY_H
#define _PLAINTEXTSETTARGETDECOY_H

#include <iostream>
#include <fstream>

#include "SetTargetDecoy.hpp"
#include "StandardExceptions.hpp"
#include "StringOperations.hpp"

// uses inheritance rather than a factory because text format requires SetTargetDecoy
template <typename K>
class PlainTextSetTargetDecoy : public SetTargetDecoy<K>
{
public:
  // K must have has >> and << operators defined
  PlainTextSetTargetDecoy(const std::string & filename);
};

#include "PlainTextSetTargetDecoy.cpp"

#endif
