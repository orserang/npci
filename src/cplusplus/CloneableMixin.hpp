#ifndef _CLONEABLEMIXIN_H
#define _CLONEABLEMIXIN_H

template <typename BASE, typename TYPE>
class CloneableMixin {
public:
  BASE*clone() const {
    return new TYPE(dynamic_cast<const TYPE &>(*this));
  }
};

#endif
