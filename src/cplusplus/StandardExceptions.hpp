#ifndef _STANDARDEXCEPTIONS_H
#define _STANDARDEXCEPTIONS_H

#include <string>

class StandardException : public std::exception {
private:
  std::string my_error_msg;
public:
  StandardException(const std::string & str) throw() {
    my_error_msg = str;
  }
  virtual ~StandardException() throw() { }
  virtual const char* what() const throw() {
    return my_error_msg.c_str();
  }
};

struct FormatException : public StandardException {
public:
  FormatException(const std::string & str) throw():
    StandardException(str) { }
};

struct ValueNotFoundException : public StandardException {
  ValueNotFoundException(const std::string & str) throw():
    StandardException(str) { }
};

struct NumericalStabilityException : public StandardException {
  NumericalStabilityException(const std::string & str) throw():
    StandardException(str) { }
};

struct OutOfBoundsException : public StandardException {
  OutOfBoundsException(const std::string & str) throw():
    StandardException(str)
  { }
};

// should possibly go into npCI/NullProjection
struct NegativeCountException : public StandardException {
  NegativeCountException(const std::string & str) throw():
    StandardException(str) { }
};

class SizeMismatchException : public StandardException {
public:
  SizeMismatchException(const std::string & str) throw():
    StandardException(str) { }
};

class NotImplementedException : public StandardException {
public:
  NotImplementedException(const std::string & str) throw():
    StandardException(str) { }
};

#endif
