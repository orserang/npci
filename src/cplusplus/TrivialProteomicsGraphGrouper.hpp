#ifndef _TRIVIALPROTEOMICSGRAPHGROUPER_H
#define _TRIVIALPROTEOMICSGRAPHGROUPER_H

#include "ProteomicsGraphGrouper.hpp"

template <typename PROTEOMICSGRAPH>
class TrivialProteomicsGraphGrouper: public ProteomicsGraphGrouper<PROTEOMICSGRAPH> {
public:
  virtual std::list<std::set<typename PROTEOMICSGRAPH::ProteinNodePtr> > get_protein_groups(const PROTEOMICSGRAPH & pg) const;
};

#include "TrivialProteomicsGraphGrouper.cpp"

#endif
