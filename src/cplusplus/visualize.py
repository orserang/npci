from lxml import etree
import numpy as np
import pylab as P
import itertools
P.ion()

def flatten(listOfLists):
    "Flatten one level of nesting"
    return itertools.chain.from_iterable(listOfLists)

import time

def getchar():
    print ''
    Var = raw_input("press a key")

#xml_tree = etree.parse('output.xml')
#xml_tree = etree.parse('output-no-ratios.xml')
xml_tree = etree.parse('output-with-dists.xml')

#axis_labels_subtree = xml_tree.find('.//axis_labels')
#axis_labels = [ ax.attrib.get('label') for ax in axis_labels_subtree.findall('.//axis') ]
#axis_labels = [ 'Log fold chg', 'Average intensity' ]
axis_labels = [ 'Log Intensity A', 'Log Intensity B' ]

# fixme: change to findall and 0, 1

# finds first only
def get_x_and_ys_for_all_replicates():
    result = []

    all_density_subtrees = xml_tree.findall('.//density_series_collection')

    for density_subtree in all_density_subtrees:
        x_vals = [ [ float(x) for x in line.split(' ') ] for line in density_subtree.find('pdf_support').text.split('\n')[:-1] ]

        y_vals = []
        for distribution_pair in list(density_subtree.find('density_series').findall('distribution_pair')):
          single_y_val_pair = [ None , None ]
          for log_pdf in distribution_pair.findall('log_pdf'):
            index = int(log_pdf.attrib.get('column_number'))
            single_y_val_pair[index] = np.exp([ float(y) for y in log_pdf.text.split('\n')[:-1] ])
          y_vals.append( single_y_val_pair )

        result.append( (x_vals, y_vals) )

    return result

rep_to_x_ys = get_x_and_ys_for_all_replicates();

#P.figure(1)

# for 1D
# P.figure(1)
# P.clf()

# size = len(x_vals)/2
# x_vals = [ x_val[0] for x_val in x_vals ][:size]
# print 'x', x_vals
# for yA, yB in y_vals[::203]:
#   yA = yA[:size]
#   yB = yB[:size]
#   P.clf()
#   print yA
#   P.plot(x_vals, yA, color="red")
#   P.plot(x_vals, yB, color="blue")
#   P.draw()
#   print 'dist', sum( abs(np.array(yA) - np.array(yB)) )
#   time.sleep(0.01)
#   getchar()


# for true 2D

def init_row_col_size(x_vals):
    global row_size, col_size
    row_times_col_size = len(x_vals)
    for row_size, row in enumerate(x_vals):
        if row_size == 0:
            continue
        if row[0] == x_vals[0][0]:
            break;

    col_size = row_times_col_size / row_size


#print row_size, col_size

# to jump ahead (for output)
#for yA, yB in y_vals[::203]:

# find the min and max for each plot (to get colors)
# min_max = [[1000, -1000], [1000, -1000], [1000, -1000]]

# for yA, yB in y_vals:
#   matA = np.zeros((row_size,col_size))
#   matB = np.zeros((row_size,col_size))

#   for row in range(row_size):
#     for col in range(col_size):
#       matA[row,col] = yA[row*col_size+col]
#       matB[row,col] = yB[row*col_size+col]

#   min_max[0][0] = min( min_max[0][0], min(flatten(matA)) )
#   min_max[0][1] = max( min_max[0][1], max(flatten(matA)) )

#   min_max[1][0] = min( min_max[1][0], min(flatten(matB)) )
#   min_max[1][1] = max( min_max[1][1], max(flatten(matB)) )

#   min_max[2][0] = min( min_max[2][0], min(flatten(matA/matB)) )
#   min_max[2][1] = max( min_max[2][1], max(flatten(matA/matB)) )

#print min_max

def scatter_to_matrix(yA):
    matA = np.zeros((row_size,col_size))
    for row in range(row_size):
        for col in range(col_size):
            # use row_size-1-row to invert y-axis
            matA[row_size-1-row,col] = yA[row*col_size+col]
    return matA

def differential_matrix_at_index(y_vals, index):
    yA, yB = y_vals[index]
    matA = scatter_to_matrix(yA)
    matB = scatter_to_matrix(yB)
    return matA / matB

# note: assumes only one ranking is used
index_to_num_targets = dict([ ( int(point.attrib.get('index')), int(point.attrib.get('target_groups')) ) for point in xml_tree.find('.//roc_series').findall('point') ])

def get_ML_index():
    # note: assumes only one ranking is used
    likelihood_subtree = xml_tree.find('.//likelihood_series')
    ML_index = int(likelihood_subtree.attrib.get('maximum_likelihood_index'))
    return ML_index

ML_index = get_ML_index()
print ML_index

# start at 0, max at 53 targets, upper bound at 300 targets
indices = [ 0 , ML_index+4 , ML_index*6+15 ]

for ind in indices:
    print index_to_num_targets[ind]

# get the maximum likelihood index for each technical replicate


from mpl_toolkits.axes_grid1 import ImageGrid

min_val, max_val = -5, 5

# only show the first heatmaps

for row in range(2):
    fig = P.figure(1)
    grid = ImageGrid(fig, 111,
                     nrows_ncols = (1, 3),
                     axes_pad = 0.2,
                     add_all = True,
                     label_mode = "L")

    x_vals, y_vals = rep_to_x_ys[row]
    init_row_col_size(x_vals)

    # for y axis, use max, min instead of min, max (imshow starts at top and goes down, inverting y axis)
    extent = [ min( [ xv[0] for xv in x_vals ] ), max( [ xv[0] for xv in x_vals ] ), min( [ xv[1] for xv in x_vals ] ), max( [ xv[1] for xv in x_vals ] ) ]
    
    print 'extent', extent

    for num, ind in enumerate(indices):
        ax = grid[ num ]

        im = ax.imshow(np.log(differential_matrix_at_index(y_vals, ind)), extent = extent, vmin = -4, vmax = 4, interpolation = None)
        P.title('Stuff')
#        P.imshow(np.log(differential_matrix_at_index(y_vals, ind)), extent = extent, vmin = -4, vmax = 4)
        P.xlabel(axis_labels[0]);
        P.ylabel(axis_labels[1]);
        
    P.savefig('rep_' + str(row) + '.pdf')
    P.clf()

decoy_mat = scatter_to_matrix(rep_to_x_ys[0][1][0][1])
im = P.imshow(decoy_mat, extent = extent, interpolation = None)
P.colorbar()
P.savefig('null.pdf')

P.draw()

#fig.subplots_adjust(right=0.8)
#P.colorbar(im)

KL = lambda a, b: sum( np.array(a) * (np.log(np.array(a)) - np.log(np.array(b))) )
  
for ind in indices:
    print 'KL', KL(y_vals[ind][0], y_vals[ind][1]) + KL(y_vals[ind][1], y_vals[ind][0])

time.sleep(0.01)
getchar()
