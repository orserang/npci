#ifndef _PLAINTEXTRANKING_H
#define _PLAINTEXTRANKING_H

#include <fstream>
#include <sstream>
#include <string>

#include "Ranking.hpp"
#include "StandardExceptions.hpp"

// requires the >> operator for K
template <typename K>
class PlainTextRanking : public Ranking<K>
{
public:
  PlainTextRanking(const std::string & filename);
};

#include "PlainTextRanking.cpp"

#endif
