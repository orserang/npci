template <typename GRAPHTYPE>
ConsistentlyGroupedGraphCollection<GRAPHTYPE>::ConsistentlyGroupedGraphCollection(const std::list<GRAPHTYPE> & all_graphs_param, const std::string & grouping_name):
  all_graphs(all_graphs_param)
{
  // create the union of all graphs
  for (const GRAPHTYPE & g : all_graphs)
    union_graph.copy_from_rhs(g);

  const ProteomicsGraphGrouper & pgg = *Singleton<ProteomicsGraphGrouperRegistry>::get_instance().lookup_name(grouping_name);
    
  std::list<PtrSet<ProteinNode>::Type > union_groups = pgg.get_protein_groups(union_graph);
  pgg.group_proteins_from_defined_sets(union_graph, union_groups);

  // apply the grouping to all_graphs
  for (GRAPHTYPE & g : all_graphs)
    {
      pgg.group_proteins_from_defined_sets(g, union_groups);
      std::cout << g.get_file_and_format().get_file() << " " << g.get_protein_node_ptr_set().size() << " " << g.get_peptide_node_ptr_set().size() << std::endl;
    }
}
