#ifndef _SUBTYPE_H
#define _SUBTYPE_H

#include <iostream>
#include <vector>
#include <typeinfo>
#include <tuple>

#include "TupleToArray.hpp"

template <typename BASE, typename T>
std::vector<const T*> of_subtype(const std::vector<const BASE*> & par) {
  std::vector<const T*> result;
  for (const BASE * base_ptr : par) {
    try {
      const T* t_ptr = & dynamic_cast<const T &>(*base_ptr);
      result.push_back( t_ptr );
    }
    catch (const std::bad_cast & bc_e) {
    }
  }
  return result;
}

template <typename BASE, typename ...T>
std::tuple<std::vector<const T*>...> separate_subtypes(const std::vector<const BASE*> & par) {
  return std::tuple<std::vector<const T*>...>( of_subtype<BASE, T>(par)... );
}

template <typename T>
std::vector<typename T::ScoreType> get_score( const std::vector<const T*> & evidence_containers ) {
  std::vector<typename T::ScoreType > result;
  for ( const T* ptr : evidence_containers )
    result.push_back(ptr->get_score());
  return result;
}

template <typename BASE, typename ...T>
std::tuple<std::vector<typename T::ScoreType >...> separate_subtype_scores(const std::vector<const BASE*> & par) {
  return std::tuple<std::vector<typename T::ScoreType >...>( get_score( of_subtype<BASE, T>(par) )... );
}


#endif

