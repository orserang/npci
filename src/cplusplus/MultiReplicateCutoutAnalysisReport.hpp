#ifndef _MULTIREPLICATECUTOUTANALYSISREPORT_H
#define _MULTIREPLICATECUTOUTANALYSISREPORT_H

#include <limits>

#include "BaseReport.hpp"

template <typename K>
class MultiReplicateCutoutAnalysisReport : public BaseReport {
public:
  MultiReplicateCutoutAnalysisReport(const MultiReplicateCutoutAnalysis<K> & mrca):
    BaseReport("MultiReplicateCutoutAnalysisReport")
  {
    write_labeled_value("label", mrca.get_label());
    write_all_rankings(mrca.get_group_ranking_ptr_list());
    write_all_roc_series(mrca.get_target_decoy_ptr(), mrca.get_group_ranking_ptr_list());
    write_all_likelihoods(mrca.get_all_likelihoods());
    write_target_decoy_column_labeles();

    for (std::shared_ptr<BaseCutoutAnalysis<K> > replicate_analysis_ptr : mrca.all_replicate_analyses_)
      // casts as BaseReport, but doesn't matter, since
      // the report is already created and only needs its root node
      // ptr from this point
      write_sub_report( replicate_analysis_ptr->generate_report() );
  }
  
private:
  void write_all_rankings(const std::list<std::shared_ptr<GroupRanking<K> > > & gr_ptr_list) {
    rapidxml::xml_node<> * rankings = create_node_ptr("ranking_collection");
    int ranking_number = 0;
    for (std::shared_ptr<GroupRanking<K> > gr_ptr : gr_ptr_list) {
      rapidxml::xml_node<> * ranking_subtree = create_subtree_for_ranking(gr_ptr);
      ranking_subtree->append_attribute( BaseReport::create_attribute_ptr("ranking_number", ranking_number) );
      ranking_subtree->append_attribute( BaseReport::create_attribute_ptr("label", gr_ptr->get_label()) );
      rankings->append_node(ranking_subtree);
      ++ranking_number;
    }
    BaseReport::root_->append_node(rankings);
  }

  void write_target_decoy( std::shared_ptr<TargetDecoy<K> > td_ptr ) {
    // fixme:
  }

  void write_all_roc_series(const std::shared_ptr<TargetDecoy<K> > td_ptr, const std::list<std::shared_ptr<GroupRanking<K> > > & group_ranking_ptr_list) {
    rapidxml::xml_node<> *roc_tree = BaseReport::create_node_ptr("roc_series_collection");
    
    std::size_t ranking_number = 0;
    for (std::shared_ptr<GroupRanking<K> > group_ranking_ptr : group_ranking_ptr_list) {
      rapidxml::xml_node<> *single_roc = create_subtree_for_roc( td_ptr->roc(*group_ranking_ptr), td_ptr->q_value_vs_targets(*group_ranking_ptr) );
      single_roc->append_attribute( create_attribute_ptr("ranking_number", ranking_number) );
      roc_tree->append_node( single_roc );

      ++ranking_number;
    }
    BaseReport::root_->append_node(roc_tree);
  }
  
  rapidxml::xml_node<> * create_subtree_for_ranking(std::shared_ptr<GroupRanking<K> > gr_ptr) {
    rapidxml::xml_node<> *ranking_subtree = BaseReport::create_node_ptr("ranking");
    rapidxml::xml_node<> * initial_point_ptr = create_point_subtree(0, std::numeric_limits<double>::infinity(), std::set< std::set<K > >() );
    ranking_subtree->append_node(initial_point_ptr);

    // start the ranking at 1 because of the first point above
    std::size_t i = 1;
    // iterate over in reverse so that higher scores are first
    const auto & double_to_set_of_groups = gr_ptr->get_double_to_scored_object_set_map();
    for ( auto iter = double_to_set_of_groups.rbegin(); iter != double_to_set_of_groups.rend(); ++iter, ++i ) {
      const std::pair<double, std::set<std::set<K> > > & score_and_equivalent_key_groups = *iter;

      rapidxml::xml_node<> * point_ptr = create_point_subtree(i, score_and_equivalent_key_groups.first, score_and_equivalent_key_groups.second);
      ranking_subtree->append_node(point_ptr);
    }
    return ranking_subtree;
  }

  rapidxml::xml_node<> * create_point_subtree(int index, double score, const std::set< std::set<K> > & set_of_groups) {
    rapidxml::xml_node<> * point_ptr = BaseReport::create_node_ptr("point");
    point_ptr->append_attribute( BaseReport::create_attribute_ptr("index", index) );
    point_ptr->append_attribute( BaseReport::create_attribute_ptr("at_or_above_score", score) );

    // for every equally scoring group, output the group
    for (const std::set<K> & group : set_of_groups) {
      rapidxml::xml_node<> * group_ptr = BaseReport::create_node_ptr("group");
      group_ptr->append_attribute( BaseReport::create_attribute_ptr("size", group.size()) );
      for (const K & key : group) {
	rapidxml::xml_node<> * key_ptr = BaseReport::create_node_ptr("key");
	key_ptr->append_attribute( BaseReport::create_attribute_ptr("key", key) );
	group_ptr->append_node( key_ptr );
      }
      point_ptr->append_node( group_ptr );
    }
    return point_ptr;
  }

  rapidxml::xml_node<> * create_subtree_for_roc(const std::vector<std::pair<int, int> > & roc_series, const std::vector<std::pair<double, int> > & q_value_vs_targets) {
    rapidxml::xml_node<> *roc_ranking_subtree = BaseReport::create_node_ptr("roc_series");
    
    for (std::size_t i = 0; i<roc_series.size(); ++i) {
      std::pair<int, int> roc_point = roc_series[i];
      std::pair<double, int> q_value_roc_point = q_value_vs_targets[i];
      
      rapidxml::xml_node<> * point_ptr = BaseReport::create_node_ptr("point");
      point_ptr->append_attribute( BaseReport::create_attribute_ptr("index", i ) );

      point_ptr->append_attribute( BaseReport::create_attribute_ptr("target_groups", roc_point.first ) );
      point_ptr->append_attribute( BaseReport::create_attribute_ptr("decoy_groups", roc_point.second ) );
      point_ptr->append_attribute( BaseReport::create_attribute_ptr("q_value", q_value_roc_point.first ) );

      roc_ranking_subtree->append_node(point_ptr);
    }

    return roc_ranking_subtree;
  }

  void write_target_decoy_column_labeles() {
    rapidxml::xml_node<> *column_label_subtree = BaseReport::create_node_ptr("both_sample_labels");
    
    rapidxml::xml_node<> * first_column = BaseReport::create_node_ptr("sample");
    first_column->append_attribute( BaseReport::create_attribute_ptr("sample_number", 0 ) );
    first_column->append_attribute( BaseReport::create_attribute_ptr("label", "target" ) );

    rapidxml::xml_node<> * second_column = BaseReport::create_node_ptr("sample");
    second_column->append_attribute( BaseReport::create_attribute_ptr("sample_number", 1 ) );
    second_column->append_attribute( BaseReport::create_attribute_ptr("label", "decoy" ) );

    column_label_subtree->append_node(first_column);
    column_label_subtree->append_node(second_column);

    BaseReport::root_->append_node(column_label_subtree);
  }
};

#endif

