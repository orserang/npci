#ifndef _KRONECKERHISTOGRAM_H
#define _KRONECKERHISTOGRAM_H

#include <unordered_map>
#include <vector>
#include <iostream>
#include <algorithm>

#include "StandardExceptions.hpp"
#include "KeyComparable.hpp"
#include "NumericRange.hpp"

// note: rvalue references will improve the runtime when the cost of
// copying T is large

namespace std {
  template<typename T, size_t N>
  struct hash<array<T, N> > {
    typedef array<T, N> argument_type;
    typedef size_t result_type;
    
    result_type operator()(const argument_type& a) const {
      hash<T> hasher;
      result_type h = 0;
      for (result_type i = 0; i < N; ++i)
	h = h * 31 + hasher(a[i]);
      return h;
    }
  };
}

template <typename T>
class KroneckerHistogram: public std::unordered_map<T, int>
{
public:
  KroneckerHistogram(): n_(0) { }
  KroneckerHistogram(const std::vector<T> & values);

  void add_values(const std::vector<T> & values);
  void remove_values(const std::vector<T> & values);

  void add_value(T val, int count_change=1);
  void remove_value(T val, int count_change=1);

  int get_n() const {
    return n_;
  }

  int number_of_unique_keys() const {
    return std::unordered_map<T, int>::size();
  }

  bool contains_key(const T & key) const {
    return std::unordered_map<T, int>::count(key) > 0;
  }

  // note: using const & for rhs assumes that & rhs != this
  const KroneckerHistogram<T> & operator +=(const KroneckerHistogram<T> & rhs) {
    for (const std::pair<T, int> & item_and_count : rhs)
      add_value(item_and_count.first, item_and_count.second);
    return *this;
  }
  const KroneckerHistogram<T> & operator -=(const KroneckerHistogram<T> & rhs) {
    for (const std::pair<T, int> & item_and_count : rhs)
      remove_value(item_and_count.first, item_and_count.second);
    return *this;
  }
  KroneckerHistogram<T> operator -() const {
    KroneckerHistogram<T> result = *this;
    for (std::pair<const T, int> & point : result) {
      point.second = -point.second;
    }
    result.n_ = -result.n_;
    return result;
  }

  T get_lower_bound() const {
    if (n_ <= 0)
      throw OutOfBoundsException("Cannot establish lower bound on empty KroneckerHistogram");
    return get_lower_from_collection( keys(*this) );
  }
  T get_upper_bound() const {
    if (n_ <= 0)
      throw OutOfBoundsException("Cannot establish lower bound on empty KroneckerHistogram");
    return get_upper_from_collection( keys(*this) );
  }

  void print() const;

private:
  int n_;
};

// used for construction with automatic type determination
template <typename T>
KroneckerHistogram<T> make_kronicker(const std::vector<T> & data) {
  return KroneckerHistogram<T>(data);
}

template <typename T>
KroneckerHistogram<T> operator -(const KroneckerHistogram<T> & lhs, const KroneckerHistogram<T> & rhs) {
  KroneckerHistogram<T> result = lhs;
  result -= rhs;
  return result;
}

#include "KroneckerHistogram.cpp"

#endif
