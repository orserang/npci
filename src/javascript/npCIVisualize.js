// Load the Visualization API and the piechart package.
google.load('visualization', '1', {'packages':['corechart', 'table']});

//document.write("<script type=\"text/javascript\" src=\"tools.js\"></script>");

function XMLAttributeTable(subtree, point_name, arr_of_attributes) {
    // note: assumes numeric attribute values
    var attributes_for_each_point = [];
    jQuery(subtree).find(point_name).each(function() {
	row = jQuery(this);
	var point = {};
	arr_of_attributes.forEach( function(attr_name) { 
	    point[attr_name] = parseFloatPlus(row.attr(attr_name));
	});
	attributes_for_each_point.push(point);
    });
    
    return attributes_for_each_point;
}

function XMLAttributeTableCollection(subtree, instance_name, point_name, arr_of_attributes) {
    // note: assumes the series are in order
    var tables_for_each_instance = [];
    jQuery(subtree).find(instance_name).each( function() {
	tables_for_each_instance.push( XMLAttributeTable( this, "point", arr_of_attributes ) );
    });
    return tables_for_each_instance;
}

function getROCSeriesForEachRanking(roc_subtree) {
    return XMLAttributeTableCollection(roc_subtree, 'roc_series', 'point', ['index', 'target_groups', 'decoy_groups', 'q_value']);
}

function getLikelihoodSeriesForEachRanking(likelihood_subtree) {
    return XMLAttributeTableCollection(likelihood_subtree, 'likelihood_series', 'point', ['index', 'likelihood']);
}

function createMatrixTemplate(rowVals, colVals) {
    // create the matrix_template
    matrix_template = [ ];
    // fill the corner with 'string'
    header = ['string'];
    colVals.forEach( function(val) {
	header.push( val.toString() );
    });
    
    matrix_template.push(header);
    rowVals.forEach( function(val) {
	matrix_template.push( [ val.toString() ] );
    });
    return matrix_template;
};

function createColorbarMatrix(min_and_max, num_buckets) {
    // guarantee that num_buckets is odd so that zero is the middle
    // (min and max should be symmetric negatives)
    if (num_buckets % 2 == 0)
	num_buckets++;
    var row_vals = [];
    for (var bucket = 0; bucket < num_buckets; ++bucket) {
	// note: javascripts Number type should divide properly (not threshold to int)
	var unrounded_val = min_and_max.min_val + bucket / (num_buckets-1) * (min_and_max.max_val - min_and_max.min_val);
	// scale to three significant figures:
	// 1) scale so that the highest value is between 0.1 and 1
	// 2) round to the nearest 0.001
	// 3) return to the original scale
	var dynamic_range_precision = Math.ceil(Math.log(min_and_max.max_val) / Math.log(10.0));
	var rounded_val = Math.round(unrounded_val / Math.pow(10.0, dynamic_range_precision)* 1000) / 1000.0;
	row_vals.push( rounded_val );
    }
    row_vals.reverse();
    var col_vals = [ 0 ];

    // fill the colorbar with the row values
    result = createMatrixTemplate(row_vals, col_vals);
    row_vals.forEach( function(val, ind){
	result[ind+1].push(val);
    });
    // remove the meaningless col label
    result[0][1] = "";
    return result;
}
    
function supportAndAllLogPDFsToAllFoldChangeMatricesForAllRankings(support_and_all_log_pdf_pairs_for_all_rankings) {
    var support = support_and_all_log_pdf_pairs_for_all_rankings.support;
    var all_log_pdf_pairs_for_all_rankings = support_and_all_log_pdf_pairs_for_all_rankings.all_density_pairs_for_each_ranking;

    getSupportColumn = function(col) {
	var result = [];
	support.forEach( function(supportPoint) {
	    result.push(supportPoint[col]);
	});
	return result;
    };
    
    rowVals = nsorted(getUnique(getSupportColumn(0)));
    // display smaller rows at bottom to match paper
    rowVals.reverse();
    colVals = nsorted(getUnique(getSupportColumn(1)));
    
    // build a table of value to column for each
    rowVals_to_index = createInverseDictionary(rowVals);
    colVals_to_index = createInverseDictionary(colVals);
    
    createLogPDFMatrix = function(log_pdf) {
	log_pdf_matrix = createMatrixTemplate(rowVals, colVals);
	log_pdf.forEach( function(log_pdf_val, ind) {
	    support_value = support[ind];
	    
	    // get the matrix indices. add 1 to account for the row
	    // and column labels
	    rowInd = rowVals_to_index[support_value[0]][0]+1;
	    colInd = colVals_to_index[support_value[1]][0]+1;
	    
	    log_pdf_matrix[rowInd][colInd] = log_pdf_val;
	});
	return log_pdf_matrix;
    };

    var result = [];

    all_log_pdf_pairs_for_all_rankings.forEach( function(all_log_pdf_pairs) {
	result_for_ranking = [];
	all_log_pdf_pairs.forEach( function(log_pdf_pair) {

	    // subtraction is division in log space
	    log_difference = function(a, b) {
	    	var diff_result = [];
	    	a.forEach( function(val, ind) {
	    	    diff_result.push(val - b[ind]);
	    	});
		return diff_result;
	    }(log_pdf_pair.target, log_pdf_pair.decoy);
	    target_vs_decoy_matrix = createLogPDFMatrix(log_difference);

	    result_for_ranking.push( target_vs_decoy_matrix );
	});
	result.push(result_for_ranking);
    });

    return result;
}

function Ranking(label, ranking_table) {
    this.label = label;
    this.ranking_table = ranking_table;
}

function getRankings(subtree) {
    result = []
    jQuery(subtree).find("ranking").each( function() {
	var ranking_number = parseInt(jQuery(this).attr("ranking_number"));
	var filename = jQuery(this).attr("label");

	var ranking_table = [ ];
	jQuery(this).find("point").each(function() {
	    var key_groups = [ [] ];
	    jQuery(this).find("group").each(function() {
		var keys = [];
		jQuery(this).find("key").each(function() {
		    keys.push( jQuery(this).attr("key") );
		});
		key_groups.push(keys);
	    });
	    ranking_table.push( { 'index' : parseFloatPlus(jQuery(this).attr("index")),
				  'at_or_above_score' : parseFloatPlus(jQuery(this).attr("at_or_above_score")),
				  'key_groups' : key_groups } );
	});
	result[ ranking_number ] = new Ranking(filename, ranking_table);
    });
    return result;
}

function appendColumn(mat, new_col) {
    for ( var index = 0; index < new_col.length; ++index ) {
	if ( mat[index] === undefined )
	    mat[index] = [];
	mat[index].push( new_col[index] );
    }
}

function appendAllColumns(mat, rhs_mat) {
    if ( mat.length == 0 )
	return;

    var num_cols_to_add = rhs_mat[0].length;

    for ( var rhs_col = 0; rhs_col < num_cols_to_add; ++rhs_col)
	for ( var index = 0; index < mat.length; ++index )
	    mat[index].push( rhs_mat[index][rhs_col] );
}

function XYSeries(x_ser, y_ser) {
    this.x = x_ser;
    this.y = y_ser;
}

function extractAllXYSeries(all_attribute_tables, x_name, y_name) {
    var result = [];
    for ( var series_number = 0; series_number < all_attribute_tables.length; ++series_number ) {
	var series = all_attribute_tables[series_number];
	var x_for_series = [];
	var y_for_series = [];
	for ( var index = 0; index < series.length; ++index ) {
	    var xval = series[index][x_name];
	    var yval = series[index][y_name];
	    
	    x_for_series.push(xval);
	    y_for_series.push(yval);
	}
	result.push( new XYSeries(x_for_series, y_for_series) );
    }
    return result;
}

// fixme: split into allXYSeriesToGoogleTable and addInverseTablesToGoogleTable
function allXYSeriesToGoogleTable(all_xy_series, titles) {
    pre_result = []

    // get all unique x-values (sorted)
    var all_x = []
    for ( var ranking_number = 0; ranking_number < all_xy_series.length; ++ranking_number ) {
	for ( var index = 0; index < all_xy_series[ranking_number].x.length; ++index )
	    all_x.push( all_xy_series[ranking_number].x[index] );
    }
    var unique_x = nsorted(getUnique(all_x));

    // create a map of x to all y (in order of index) for each xy series
    var column_to_x_to_ys_for_column = [];
    for ( var series_number = 0; series_number < all_xy_series.length; ++series_number ) {
	// for each x value:
	// if it's in the series for this ranking, put the y-value in
	// the series (if not, put in null)

	// create and add the column from this ranking (add the header)
	var xy_series = all_xy_series[series_number];

	var series = all_xy_series[series_number];
	
	var x_to_ys_for_column = {}
	for ( var index = 0; index < series.x.length; ++index ) {
	    var xval = series.x[index];
	    var yval = series.y[index];

	    if ( ! x_to_ys_for_column.hasOwnProperty(xval) )
		x_to_ys_for_column[ xval ] = [];
	    x_to_ys_for_column[ xval ].push(yval);
	}
	column_to_x_to_ys_for_column.push( x_to_ys_for_column );
    }

    // turn unique_x into an array that includes duplicates
    var x_to_num_x = {};
    unique_x.forEach( function(xval) {
	var num_x = 0;
	// for each x value, compute the number of rows needed (it's
	// the maximum number of y values over all series). note that
	// the y values don't need to align (each column in the final
	// table is handled on its own).
	for ( var series_number = 0; series_number < all_xy_series.length; ++series_number ) {
	    if ( column_to_x_to_ys_for_column[series_number].hasOwnProperty(xval) )
		num_x = Math.max( num_x, column_to_x_to_ys_for_column[series_number][xval].length );
	}
	x_to_num_x[xval] = num_x;
    } );

    // add the column with the x values to the pre_result
    var new_x_column = [];
    unique_x.forEach( function(xval) {
	var num_x = x_to_num_x[xval];
	for ( var k = 0; k < num_x; ++k )
	    new_x_column.push( xval ); 
    } );
    appendColumn(pre_result, new_x_column);

    // to allow lookup by column below
    var all_new_columns = [];

    // initialized below: anytime you do a push on the new column,
    // also add a link back from merged index to index for that point
    series_number_to_merged_index_to_index = [];

    // add new columns
    for ( var series_number = 0; series_number < all_xy_series.length; ++series_number ) {
	var x_to_ys_for_column = column_to_x_to_ys_for_column[series_number];
	var merged_index_to_index_for_column = [];

	var new_column = []
	var original_index = 0;
	for ( var index = 0; index < unique_x.length; ++index ) {
	    var xval = unique_x[index];
	    var num_x = x_to_num_x[xval];

	    var num_x_in_this_column = 0;
	    if ( x_to_ys_for_column.hasOwnProperty(xval) ) {
		for ( var k = 0; k < x_to_ys_for_column[xval].length; ++k ) {
		    merged_index_to_index_for_column.push(original_index);
		    new_column.push(x_to_ys_for_column[xval][k]);
		    ++original_index;
		}

		num_x_in_this_column += x_to_ys_for_column[xval].length;
	    }

	    for ( var k = 0; k < (num_x - num_x_in_this_column); ++k ) {
		// if the index doesn't exist, use the last valid one
		// (if no index has occurred yet because this series
		// is missing the first x value, use 0). this
		// guarantees that the index will always exist in the
		// series (if you were to use the next index instead
		// of the previous, then a missing x value at the end
		// would result in an attempted lookup of [length].).
		merged_index_to_index_for_column.push( Math.max(original_index-1, 0) );
		new_column.push(null);
	    }
	}
 	all_new_columns.push(new_column);
	appendColumn(pre_result, new_column);

	series_number_to_merged_index_to_index[series_number] = merged_index_to_index_for_column;
    }

    if ( titles.length != pre_result[0].length-1 )
	alert('Error: cannot plot data with titles of length ' + titles.length + ' and ' + (pre_result[0].length-1) + ' columns');

    var result = google.visualization.arrayToDataTable( [ ['index'].concat(titles) ].concat(pre_result) );

    // add the table mapping merged indices to indices
    result.series_number_to_merged_index_to_index = series_number_to_merged_index_to_index;

    // make inverse tables of x and all y values (i.e. a map to indices for each series)
    result.inverse_table_for_each_column = [ createInverseDictionary(new_x_column) ];
    for ( var series_number = 0; series_number < all_xy_series.length; ++series_number )
	result.inverse_table_for_each_column.push(createInverseDictionary(all_new_columns[series_number]));
    
    return result;
}

function addAndDrawChart(parent_div_id, chart_div_id, all_xy_series, ranking_titles, x_axis_lbl, y_axis_lbl, other_options) {
    var chart = addChart(parent_div_id, chart_div_id);
    drawChart(chart, all_xy_series, ranking_titles, x_axis_lbl, y_axis_lbl, other_options);

    // silence the legend mouseover events that crash due to duplicate
    // identical rows in Google charts; alternatively, you could
    // remove duplicate rows from the DataTable before creating the
    // inverse dictionaries.
    
    // fixme: clicking the legend may cause a crash when the data
    // table contains any identical rows; however, allowing click
    // events is necessary, because it allows navigating through the
    // legend when there are too many series to display at once.
    jQuery('#' + parent_div_id + '_' + chart_div_id + ' > div > div > svg > g:first-of-type').bind('mouseover mousemove mousedown mouseup dblclick', function(e) { e.stopPropagation(); });

    return chart;
}

function addChart(parent_div_id, chart_div_id) {
    var chart_id = parent_div_id + "_" + chart_div_id;
    addDiv(parent_div_id, chart_id);

    var chart = new google.visualization.LineChart(document.getElementById(chart_id));
    return chart;
}

function addHeatmapChartAndColorbarChart(parent_div_id, chart_div_id, title) {
    var container_div_id = parent_div_id + "_container";
    addDiv(parent_div_id, container_div_id);

    jQuery("#" + container_div_id).append("<p>" + title + "</p>");

    var colorbar_id = container_div_id + '_colorbar';
    addDivFloat(container_div_id, colorbar_id, 'left');
    jQuery("#" + colorbar_id).attr('position', 'relative');
    var heatmap_id = container_div_id + "_" + chart_div_id;
    addDivFloat(container_div_id, heatmap_id, 'right');
    jQuery("#" + heatmap_id).attr('position', 'relative');

    // fix the problem with overflow : hidden
    jQuery("#" + container_div_id).append("<div style=\"clear:both\"></div>")

    var heatmap = new org.systemsbiology.visualization.BioHeatMap(document.getElementById(heatmap_id));
    var colorbar = new org.systemsbiology.visualization.BioHeatMap(document.getElementById(colorbar_id));
    return {heatmap_chart : heatmap, colorbar_chart : colorbar, heatmap_chart_id : heatmap_id};
}

function drawChart(chart, all_xy_series, ranking_titles, x_axis_lbl, y_axis_lbl, other_options) {
    var google_table = allXYSeriesToGoogleTable(all_xy_series, ranking_titles);

    var options = {'chartArea':{width:"60%"}, vAxis: {'title': y_axis_lbl}, 'hAxis': {'title': x_axis_lbl}, 'interpolateNulls': true };
    update(options, other_options);

    chart.draw( google_table, options );

    // add the data table to the chart
    chart.google_table = google_table;
}

function updateLikelihoodPointsWithROCPoints(likelihood_series_for_each_ranking, roc_series_for_each_ranking, ranking_titles) {
    // First, add the roc point data to the likelihood series points

    for ( var ranking_number = 0; ranking_number < likelihood_series_for_each_ranking.length; ++ranking_number )
	updateAll(likelihood_series_for_each_ranking[ranking_number], roc_series_for_each_ranking[ranking_number]);
}

function toFloats(arr) {
    var result = [];
    for ( var k = 0; k < arr.length; ++k )
	result.push( parseFloatPlus(arr[k]) );
    return result;
}

function newlineVals(str) {
    var result = [];
    var newline_array_vals = str.split('\n');

    // use length-1 as the bound to avoid final newline (results in
    // empty string, which becomes a NaN value)
    for ( var k = 0; k < newline_array_vals.length-1; ++k )
	result.push( newline_array_vals[k] );
    return result;
}

function getDensitySeriesCollection(subtree) {
    var density_series_collection_subtree = jQuery(subtree).find("density_series_collection")[0];
    var pdf_support_subtree = jQuery(density_series_collection_subtree).find("pdf_support")[0];
    var support_series_text = jQuery(pdf_support_subtree).context.textContent;
    var support_series_lines = newlineVals(support_series_text);

    var support_series = [];
    for ( var index = 0; index < support_series_lines.length; ++index ) {
	var line_strings = support_series_lines[index].split(' ');
	support_series.push( toFloats(line_strings) );
    }

    var all_density_pairs_for_each_ranking = [];
    jQuery(density_series_collection_subtree).find("density_series").each(function() {
	var ranking_number = parseInt(jQuery(this).attr("ranking_number"));
	all_density_pairs_for_ranking = []

	jQuery(this).find("distribution_pair").each(function() {
	    var index = parseInt(jQuery(this).attr("index"));
	    
	    log_pdf_pair = []
	    jQuery(this).find("log_pdf").each(function() {
		var column_number = parseInt(jQuery(this).attr("column_number"));

		var log_pdf_values = toFloats(newlineVals( jQuery(this).context.textContent ));
		log_pdf_pair[column_number] = log_pdf_values;
	    });
	    // note that the decoy series is always column 1 in the npCI
	    log_pdf_object = { 'target' : log_pdf_pair[0],
			       'decoy' : log_pdf_pair[1] };

	    all_density_pairs_for_ranking[index] = log_pdf_object;
	});
	all_density_pairs_for_each_ranking[ranking_number] = all_density_pairs_for_ranking;
    });

    return { 'support' : support_series,
	     'all_density_pairs_for_each_ranking' : all_density_pairs_for_each_ranking };
}

function getSingleReplicateResults(xmlData) {
    var result = []

    console.log("Loading replicates")
    jQuery(xmlData).find("SingleReplicateCutoutAnalysisReport").each(function() {
	
	var sample_sizes = [ ];
	jQuery(jQuery(this).find("both_sample_sizes")[0]).find("sample").each(function() {
	    var i = parseInt(jQuery(this).attr("sample_number"));
	    var n = parseFloatPlus(jQuery(this).attr("size"));
	    sample_sizes[i] = n;
	});

	var smoothed_likelihood_similarity_report = jQuery(this).find("SmoothedLikelihoodSimilarityReport")[0];
	var normalized_sigma = jQuery(smoothed_likelihood_similarity_report).attr("normalized_sigma");
	var dimensions = jQuery(smoothed_likelihood_similarity_report).attr("dimensions");
	
	single_result = { 'likelihood_series_for_each_ranking' : getLikelihoodSeriesForEachRanking(jQuery(this)),
			  'label': jQuery(this).attr("label"),
			  'dimensions' : dimensions,
			  'sample_sizes' : sample_sizes,
			  'normalized_sigma' : normalized_sigma};
	
//	if (dimensions == 1) {
	if (dimensions == 1 || dimensions == 2) {
	    single_result.support_and_all_density_pairs_for_each_ranking = getDensitySeriesCollection(jQuery(this));
	}
	
	result.push( single_result );
    });

    return result;
}

function getAggregateResults(xmlData) {
    console.log("Loading aggregate");
    var multiReport = jQuery(xmlData).find("MultiReplicateCutoutAnalysisReport")[0];
    // narrow the likelihood subtree to
    // likelihood_series_collection. otherwise, the single replicate
    // series will also be found.
    var result = { "likelihood_series_for_each_ranking" : getLikelihoodSeriesForEachRanking(jQuery(multiReport).find("likelihood_series_collection")[0]),
			      "roc_series_for_each_ranking" : getROCSeriesForEachRanking(multiReport),
			      "label": jQuery(multiReport).attr("label"),
			      "rankings" : getRankings(multiReport) };

    updateLikelihoodPointsWithROCPoints(result.likelihood_series_for_each_ranking, result.roc_series_for_each_ranking);

    return result;
}

function renderRankingTables(aggregate_results, parent_div_id, chart_div_id_prefix) {
    var rankings = aggregate_results.rankings;
    var likelihood_series_for_each_ranking = aggregate_results.likelihood_series_for_each_ranking;
    var roc_series_for_each_ranking = aggregate_results.roc_series_for_each_ranking;

    var panel_div_id = chart_div_id_prefix + '_panels';
    addDiv(parent_div_id, panel_div_id);

    panel_div = jQuery("#" + panel_div_id);
    // add the tab links to the div
    var ul_id = panel_div_id + '_ul';
    panel_div.append('<ul id="' + ul_id + '">');
    for (var ranking_number = 0; ranking_number < rankings.length; ++ranking_number) {
	var chart_div_id = chart_div_id_prefix + "_" + ranking_number;
	var label = rankings[ranking_number].label;
	jQuery("#" + ul_id).append('<li><a href="#' + chart_div_id + '">' + label + '</a></li>');
    }

    // add the tab divs
    for (var ranking_number = 0; ranking_number < rankings.length; ++ranking_number) {
	var single_ranking = rankings[ranking_number];
	var likelihood_series = likelihood_series_for_each_ranking[ranking_number];
	var roc_series = roc_series_for_each_ranking[ranking_number];

	// render an individual chart in a tabbed div
	var data = new google.visualization.DataTable();
        data.addColumn('number', 'Rank');
	data.addColumn('number', 'Ranking score');
	data.addColumn('number', 'Target group identifications (cumulative)');
	data.addColumn('number', 'Decoy group identifications (cumulative)');
        data.addColumn('number', 'q-value');
        data.addColumn('number', 'npCI (to include this rank and all superior)');
        data.addColumn('string', 'Keys');

	for (var index = 0; index < single_ranking.ranking_table.length; ++index) {
	    var num_target_groups = roc_series[index].target_groups;
	    var num_decoy_groups = roc_series[index].decoy_groups;

	    var ranking_score = single_ranking.ranking_table[index].at_or_above_score;
	    var qval = roc_series[index].q_value;
	    var key_groups = single_ranking.ranking_table[index].key_groups;
	    all_key_groups_string = "";
	    for (var key_group_ind = 0; key_group_ind < key_groups.length; ++key_group_ind) {
		var key_group_string = ""
		for (var key_ind = 0; key_ind < key_groups[key_group_ind].length; ++key_ind)
		    key_group_string += key_groups[key_group_ind][key_ind] + " ";

		// if the group is not of trivial size, add parentheses
		if (key_groups[key_group_ind].length > 1)
		    key_group_string = "( " + key_group_string + " ) ";

		all_key_groups_string += key_group_string;
	    }
//	    console.log([ index, ranking_score, num_target_groups, num_decoy_groups, qval, likelihood_series[index].likelihood, all_key_groups_string ]);
	    data.addRow( [ index, ranking_score, num_target_groups, num_decoy_groups, qval, likelihood_series[index].likelihood, all_key_groups_string ] );
	}

	var chart_div_id = chart_div_id_prefix + "_" + ranking_number;
	addDiv(panel_div_id, chart_div_id);
	var table = new google.visualization.Table(document.getElementById(chart_div_id));
        table.draw(data, {showRowNumber: false});
    }

    panel_div.tabs();
}

function getBestRankingNumberAndBestQValAndHighestQVal(aggregate_results) {
    var likelihood_series_for_each_ranking = aggregate_results.likelihood_series_for_each_ranking;
    var roc_series_for_each_ranking = aggregate_results.roc_series_for_each_ranking;
    
    var best_likelihood = 0.0;
    var best_qval = 0.0;
    var highest_qval = 0.0;
    var best_ranking_number = 0;

    for (var ranking_number = 0; ranking_number < likelihood_series_for_each_ranking.length; ++ranking_number) {
	var likelihood_series = likelihood_series_for_each_ranking[ranking_number];
	var roc_series = roc_series_for_each_ranking[ranking_number];

	for (var index = 0; index < likelihood_series.length; ++index) {
	    var likelihood = likelihood_series[index].likelihood;
	    var qval = roc_series[index].q_value;
	    
	    if (best_likelihood < likelihood) {
		best_likelihood = likelihood;
		best_qval = qval;
		best_ranking_number = ranking_number;
	    }

	    highest_qval = Math.max(highest_qval, qval);
	}
    }
    return { 'best_ranking_number' : best_ranking_number,
	     'best_qval' : best_qval,
	     'highest_qval' : highest_qval };
}

function renderAggregateResults(aggregate_results, ranking_titles) {
    var aggregate_id = "aggregate";
    addDiv("results", aggregate_id);

    // Write the heading title
    jQuery("#" + aggregate_id).append("<p>" + "Aggregate: " + aggregate_results.label + "</p>");

    var best_ranking_number_and_qval = getBestRankingNumberAndBestQValAndHighestQVal(aggregate_results);
    var best_ranking_number = best_ranking_number_and_qval.best_ranking_number;
    var best_qval = best_ranking_number_and_qval.best_qval;
    var highest_qval = best_ranking_number_and_qval.highest_qval;

    var numeric_summary_id = aggregate_id + "_numeric_summary";
    addDiv(aggregate_id, numeric_summary_id);
    // fixme: align div
    jQuery("#" + numeric_summary_id).attr('align', 'left');

    jQuery("#" + numeric_summary_id).append("<p>" + "Best ranking: " + ranking_titles[best_ranking_number] + "</p>");
    jQuery("#" + numeric_summary_id).append("<p>" + "Best q-value: " + best_qval + "</p>");
    if ( best_qval > 0.2 )
	jQuery("#" + numeric_summary_id).append("<p style='color:red'>" + "Warning: best q-value is large (it may be that the ranking provided does not result in high-quality inferences)" + "</p>");

    // Plot the likelihoods
    var likelihood_chart = addAndDrawChart(aggregate_id, "likelihood_chart", extractAllXYSeries(aggregate_results.likelihood_series_for_each_ranking, "target_groups", "likelihood"), ranking_titles, 'Target group identifications', 'npCI');

    var max_qval_for_plot = Math.max( best_qval+0.015, highest_qval);

    // Plot the ROCs
    
    // note: add the q-value label again (it's lost)
    var roc_chart = addAndDrawChart(aggregate_id, "roc_chart", extractAllXYSeries(aggregate_results.roc_series_for_each_ranking, 'q_value', 'target_groups'), ranking_titles, 'q-value', 'Target group identifications',
				    // to set the min and max x values
				    { 'hAxis' : {title:'q-value', viewWindowMode:'explicit', viewWindow: { 'min': 0, 'max': max_qval_for_plot }} } );

    bindSelectionOnMouseOver(likelihood_chart, roc_chart);
    renderRankingTables(aggregate_results, aggregate_id, 'aggregate_table_id');

    jQuery("#" + aggregate_id).append("<hr>");
    return { 'likelihood_chart' : likelihood_chart,
	     'roc_chart' : roc_chart };
}

function renderReplicateResults(replicate_results, aggregate_results, ranking_titles) {
    var num_replicates = replicate_results.length;

    replicate_to_likelihood_chart = [];
    for ( var rep_num = 0; rep_num < num_replicates; ++rep_num ) {
	single_rep = replicate_results[rep_num];
	var rep_id = "replicate_" + rep_num;
	
	addDiv("results", rep_id);
	jQuery("#" + rep_id).append("<p>" + single_rep.label + "</p>");

	// add the numeric summary for the replicate
	var numeric_rep_id = rep_id + '_numeric_rep_id';
	addDiv(rep_id, numeric_rep_id);
	// fixme: align div
	jQuery("#" + numeric_rep_id).attr('align', 'left');

	jQuery("#" + numeric_rep_id).append("<p>" + "Normalized sigma: " + single_rep.normalized_sigma + "</p>");
	jQuery("#" + numeric_rep_id).append("<p>" + "Sample size: " + single_rep.sample_sizes[0] + " target, " + single_rep.sample_sizes[1] + " decoy" + "</p>");


	// plot replicates
	updateLikelihoodPointsWithROCPoints(single_rep.likelihood_series_for_each_ranking, aggregate_results.roc_series_for_each_ranking);

	var likelihood_chart = addAndDrawChart(rep_id, "likelihood_chart", extractAllXYSeries(single_rep.likelihood_series_for_each_ranking, 'target_groups', 'likelihood'), ranking_titles, 'Target group identifications', 'npCI');

	replicate_to_likelihood_chart.push(likelihood_chart);

	if (single_rep.dimensions == 1) {
	    // add the density chart
	    var pdf_chart = addChart(rep_id, "pdf_chart");

	    // draw with empty data
	    var options = {'chartArea':{width:"60%"}, vAxis: {'title': 'Log PDF'}, 'hAxis': {'title': 'Evidence score'}, 'interpolateNulls':true };
	    pdf_chart.draw( google.visualization.arrayToDataTable( [['x', 'y'], [0, 0] ]), options );
	    bindDistributionUpdateMouseover(likelihood_chart, pdf_chart, single_rep.support_and_all_density_pairs_for_each_ranking, ranking_titles);
	}
	else if (single_rep.dimensions == 2) {
	    all_relative_matrices_for_every_ranking = supportAndAllLogPDFsToAllFoldChangeMatricesForAllRankings(single_rep.support_and_all_density_pairs_for_each_ranking);

	    var all_heatmap_cell_vals = function() {
		var res = [];
		all_relative_matrices_for_every_ranking.forEach(
		    function(all_matrices) {
			all_matrices.forEach( function(mat, ind) {
			    res.push.apply(res, flattenMatrixAndRemoveTitles(mat));
			});
		    });
		return res;
	    }();
	    // use 1%, 99% percentile instead of min and max to remove
	    // outliers from visualization
	    var min_and_max = percentileVals(all_heatmap_cell_vals, 0.0);

	    // make symmetric bounds so that middle is zero
	    min_and_max.min_val = Math.min(min_and_max.min_val, -min_and_max.max_val);
	    min_and_max.max_val = Math.max(-min_and_max.min_val, min_and_max.max_val);

	    // add relative log density heatmaps for each ranking
	    single_rep.support_and_all_density_pairs_for_each_ranking.all_density_pairs_for_each_ranking.forEach( function(all_density_pairs, ranking_index) {
		heatmap_chart_and_colorbar_chart = addHeatmapChartAndColorbarChart(rep_id, "relative_pdf_heatmap_chart", ranking_titles[ranking_index]);
		heatmap_chart = heatmap_chart_and_colorbar_chart.heatmap_chart;
		colorbar_chart = heatmap_chart_and_colorbar_chart.colorbar_chart;
		var heatmap_div_id = heatmap_chart_and_colorbar_chart.heatmap_chart_id;

		// draw with empty data
		var red_color = {r: 150, g: 0, b: 0, a: 1 };
		var blue_color = {r: 0, g: 0, b: 150, a: 1 };
		var options = { startColor : red_color , endColor : blue_color , cellWidth : 12, cellHeight : 12 , drawBorder : false, passThroughWhite : true};

		// note: drawing here initializes the min/max values used for coloring.
		// first draw, then hide, then set to show on mouseover
		placeholder_matrix = [['string', 'col1'], ['row1', min_and_max.min_val], ['row2', min_and_max.max_val] ];
		heatmap_chart.draw( google.visualization.arrayToDataTable( placeholder_matrix ), options );
		var num_matrix_rows = all_relative_matrices_for_every_ranking[ranking_index][0].length;
		colorbar_chart.draw( google.visualization.arrayToDataTable( createColorbarMatrix(min_and_max, num_matrix_rows) ), options );
		// now hide the drawn dummy plots
		jQuery('#' + heatmap_div_id).hide(0);

		bindHeatmapUpdateMouseover(likelihood_chart, heatmap_chart, colorbar_chart, all_relative_matrices_for_every_ranking[ranking_index], ranking_index, heatmap_div_id, options);
	    });
	}
    }

    return { 'replicate_to_likelihood_chart' : replicate_to_likelihood_chart };
}

// fixme: use to replace current bind 
/*
function bindOnMouseOverSamePointX(from_chart, to_chart, to_row_or_col) {
    google.visualization.events.addListener(from_chart, 'onmouseover', function(e) {
	if (e.row != null && e.column != null) {
	    from_chart.setSelection( [e] );
	    // get the shared value (use 0 as the column to get the x value, >0 to select a column with y values)

	    assert(from_row_or_col == 'row' || from_row_or_col == 'column');
	    if (from_row_or_col == 'row')
		var shared_variable = from_chart.google_table.getValue(e.row, 0);
	    else
		var shared_variable = from_chart.google_table.getValue(e.row, e.column);

	    // convert the shared_variable value to indices in the
	    // to_chart that have that value (note: use e.column-1 to
	    // remove the x column in the google chart)
	    to_indices_matching_shared_variable_value = to_chart.google_table.inverse_table_for_each_column[ e.column ][ shared_variable ];

	    // select those points in to_chart
	    to_points_with_number_targets = [];
	    for (var index = 0; index < to_indices_with_number_targets.length; ++index)
		to_points_with_number_targets[index] = { 'row' : to_indices_with_number_targets[index],
							  'column' : e.column };
	    to_chart.setSelection( to_points_with_number_targets );
	}
    });

    google.visualization.events.addListener(roc_chart, 'onmouseover', function(e) {
	if (e.row != null && e.column != null) {
	    roc_chart.setSelection( [e] );
	    // get the y value (use e.column as the column to get the y value)
	    var targets = roc_chart.google_table.getValue(e.row, e.column);

	    // convert the y value to indices in the likelihood chart that have that value
	    likelihood_indices_with_number_targets = likelihood_chart.google_table.x_to_merged_indices[ targets ];

	    // select those points in the likelihood chart
	    likelihood_points_with_number_targets = [];
	    for (var index = 0; index < likelihood_indices_with_number_targets.length; ++index)
		likelihood_points_with_number_targets[index] = { 'row': likelihood_indices_with_number_targets[index],
							  'column' : e.column };
	    likelihood_chart.setSelection( likelihood_points_with_number_targets );
	}
    });
}
*/

function bindSelectionOnMouseOver(likelihood_chart, roc_chart)
{
    google.visualization.events.addListener(likelihood_chart, 'onmouseover', function(e) {
	if (e.row != null && e.column != null) {
	    likelihood_chart.setSelection( [e] );
	    // get the x value (use 0 as the column to get the x value)
	    var targets = likelihood_chart.google_table.getValue(e.row, 0);

	    // convert the x value to indices in the roc chart that
	    // have that value
	    roc_indices_with_number_targets = roc_chart.google_table.inverse_table_for_each_column[ e.column ][ targets ];

	    // select those points in the roc chart
	    roc_points_with_number_targets = [];
	    for (var index = 0; index < roc_indices_with_number_targets.length; ++index)
		roc_points_with_number_targets[index] = { 'row': roc_indices_with_number_targets[index],
							  'column' : e.column };
	    roc_chart.setSelection( roc_points_with_number_targets );
	}
    });

    google.visualization.events.addListener(roc_chart, 'onmouseover', function(e) {
	if (e.row != null && e.column != null) {
	    roc_chart.setSelection( [e] );
	    // get the y value (use e.column as the column to get the y value)
	    var targets = roc_chart.google_table.getValue(e.row, e.column);

	    // convert the y value to indices in the likelihood chart that have that value
	    likelihood_indices_with_number_targets = likelihood_chart.google_table.inverse_table_for_each_column[0][ targets ];

	    // select those points in the likelihood chart
	    likelihood_points_with_number_targets = [];
	    for (var index = 0; index < likelihood_indices_with_number_targets.length; ++index)
		likelihood_points_with_number_targets[index] = { 'row': likelihood_indices_with_number_targets[index],
							  'column' : e.column };
	    likelihood_chart.setSelection( likelihood_points_with_number_targets );
	}
    });
}

function bindDistributionUpdateMouseover(likelihood_chart, pdf_chart, support_and_all_density_pairs_for_each_ranking, ranking_titles, plotDifference = true) {
    google.visualization.events.addListener(likelihood_chart, 'onmouseover', function(e) {
	if (e.row != null && e.column != null) {
	    likelihood_chart.setSelection( [e] );

	    // fixme: make function like ...ToAllMatrices and call
	    // outside addListener
	    var support = support_and_all_density_pairs_for_each_ranking.support;
	    var all_density_pairs_for_each_ranking = support_and_all_density_pairs_for_each_ranking.all_density_pairs_for_each_ranking;
	    var merged_index = e.row;

	    var raw_data = [ ];

	    for (var support_index = 0; support_index < support.length; ++support_index) {
		// push the x value
		var raw_data_row = [ support[support_index][0] ];
		for (var series_number = 0; series_number < all_density_pairs_for_each_ranking.length; ++series_number) {
		    var raw_index_for_series = likelihood_chart.google_table.series_number_to_merged_index_to_index[series_number][merged_index];

		    if ( ! plotDifference )
			// plot the logs
			raw_data_row.push( all_density_pairs_for_each_ranking[series_number][raw_index_for_series].target[support_index] );
		    else {
			var raw_index_0 = likelihood_chart.google_table.series_number_to_merged_index_to_index[0][merged_index];

			raw_data_row.push( all_density_pairs_for_each_ranking[series_number][raw_index_for_series].target[support_index] - all_density_pairs_for_each_ranking[0][raw_index_0].decoy[support_index] );
		    }
		}

		// Add the point from the decoy density series. Use
		// the fact that the decoys series is identical
		// regardless of which ranking it comes from (note:
		// this is not the case for general
		// SmoothedLikelihoodSimilarity, but is the case for
		// the graphical npCI). For this reason, the decoy
		// series are identical regardless of which ranking is
		// used, so use arbitrary series_number = 0.

		if ( ! plotDifference ) {
		    // plot the log decoys only when not plotting differences
		    var raw_index_0 = likelihood_chart.google_table.series_number_to_merged_index_to_index[0][merged_index];
		    raw_data_row.push( all_density_pairs_for_each_ranking[0][raw_index_0].decoy[support_index] );
		}

		raw_data.push(raw_data_row);
	    }

	    if ( ! plotDifference ) {
		var options = {'chartArea':{width:"60%"}, vAxis: {'title': 'Log PDF'}, 'hAxis': {'title': 'Evidence score'}, 'interpolateNulls':true };
		pdf_chart.draw(google.visualization.arrayToDataTable( [ [ 'string' ].concat(ranking_titles.concat( [ 'Decoy' ])) ].concat(raw_data) ), options );
	    }
	    else {
		var options = {'chartArea':{width:"60%"}, vAxis: {'title': 'Log PDF (Relative to Decoy)'}, 'hAxis': {'title': 'Evidence score'}, 'interpolateNulls':true };

		pdf_chart.draw(google.visualization.arrayToDataTable( [ [ 'string' ].concat(ranking_titles ) ].concat(raw_data) ), options );
	    }

	}
    });
}

function bindHeatmapUpdateMouseover(likelihood_chart, heatmap_chart, colorbar_chart, all_matrices_for_ranking, ranking_number, hidden_div_id, options) {
    google.visualization.events.addListener(likelihood_chart, 'onmouseover', function(e) {
	if (e.row != null && e.column != null) {
	    likelihood_chart.setSelection( [e] );

	    var merged_index = e.row;
	    var raw_index_for_series = likelihood_chart.google_table.series_number_to_merged_index_to_index[ranking_number][merged_index];

	    // undo the hide
	    jQuery('#' + hidden_div_id).show(0);
	    heatmap_chart.draw(google.visualization.arrayToDataTable( all_matrices_for_ranking[raw_index_for_series] ), options);
//	    colorbar_chart.draw( google.visualization.arrayToDataTable( createColorbarMatrix(all_matrices_for_ranking[raw_index_for_series])), options);
	}
    });
}

function renderAllCharts(xmlData) {
    var replicate_results = getSingleReplicateResults(xmlData);
    var aggregate_results = getAggregateResults(xmlData);

    var num_rankings = aggregate_results.likelihood_series_for_each_ranking.length;

    // create the ranking titles (for plotting)
    // start with a title for the independent variable (not used by the plot)
    ranking_titles = [];
    for ( var ranking_number = 0; ranking_number < num_rankings; ++ranking_number )
	ranking_titles.push( aggregate_results.rankings[ranking_number].label);

    var aggregate_charts = renderAggregateResults(aggregate_results, ranking_titles);
    var all_replicate_charts = renderReplicateResults(replicate_results, aggregate_results, ranking_titles);
}

function reformatErrorReportText(errorReportID) {
    // change newline to html breaks in the error report
    jQuery(document).ready( function() {
    var str = jQuery("#" + errorReportID).html();
    str = str.replace(/\n/g, '<br />');
    str = str.replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
    jQuery("#" + errorReportID).html(str);
    });
}

