function ajaxWithProgressBarAndSuccessFunction(xmlFilename, loadingID, successFunction) {
    // if connected to network, show progress bar to indicate loading has begun
    if (navigator.onLine)
	jQuery( "#" + loadingID ).show();

    console.log("loading");
    // load the xml file with ajax
    jQuery.ajax({
	url: xmlFilename,
	dataType:"xml",
	async: true,

	// update the progress bar as the loading progresses
	xhr: function() {
	    var xhr = new window.XMLHttpRequest();
	    xhr.addEventListener("progress", function(evt){
		if (evt.lengthComputable) {
		    var percentComplete = evt.loaded / evt.total;
		    //Do something with download progress
		    jQuery( "#" + loadingID + " > #progressbar" ).progressbar({
	     		value: percentComplete*100
		    });
		}
	    }, false);
	    return xhr;
	},

	// after loading succeeds, get data from XML report
	success : function(xmlData) {
	    console.log("AJAX success");
	    // hide the loading progress bar
	    jQuery( "#" + loadingID ).hide(100);

	    successFunction(xmlData);
    	}
    });
}

function parseFloatPlus(str) {
    // fixme: how small is small?
    if (str == '-inf') return -1E100;
    if (str == 'inf') return 1E100;
    return parseFloat(str)
}


function getUnique(arr) {
    var u = {}, a = [];
    for(var i = 0, l = arr.length; i < l; ++i){
	if(u.hasOwnProperty(arr[i])) {
            continue;
	}
	a.push(arr[i]);
	u[arr[i]] = 1;
    }
    return a;
}

function nsort(arr) {
    arr.sort( function(a,b) {return a-b;} );
}

function nsorted(arr) {
    arr_copy = arr.clone();
    nsort(arr_copy);
    return arr_copy;
}

function createInverseDictionary(column) {
    var val_to_indices = {};
 
    for (var index = 0; index < column.length; ++index) {
	var val = column[index];
	if ( val == null )
	    continue;
	
	if ( ! val_to_indices.hasOwnProperty(val) )
	    val_to_indices[val] = [];
	
	val_to_indices[val].push(index);
    }
    return val_to_indices;
}

function update(a, b) {
    // merges dictionaries as python does
    
    // note: overwrites fields of b that are also in a
    for ( x in b )
	a[x] = b[x];
}

function updateAll(a_series, b_series) {
    for (var index = 0; index < a_series.length; ++index)
	update( a_series[index], b_series[index] );
}

function addDiv(parent_id, div_id) {
    jQuery("#" + parent_id).append("<div id=\"" + div_id + "\" class=\"left_right_container\"></div>");
}

function addDivFloat(parent_id, div_id, float_left_right) {
    jQuery("#" + parent_id).append("<div id=\"" + div_id + "\" class=\"" + float_left_right + "\"></div>");
}

function minAndMaxVals(arr) {
    var sorted_vals = nsorted(arr);
    return {min_val : sorted_vals[0],
	    max_val : sorted_vals[sorted_vals.length-1]};
}

function percentileVals(arr, percent) {
    // round to nearest int
    var change_for_percent = Math.round(percent * arr.length);
    var sorted_vals = nsorted(arr);
    return {min_val : sorted_vals[0+change_for_percent],
	    max_val : sorted_vals[sorted_vals.length-1-change_for_percent]};
}

function flattenMatrixAndRemoveTitles(mat) {
    // flatten the matrix and strip out the strings from row 0 and col 0
    var result = [];
    for (var row = 1; row < mat.length; ++row)
	for (var col = 1; col < mat[row].length; ++col)
	    result.push(mat[row][col]);

    return result;
}
