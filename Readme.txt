How to use npCI:

npCI [arguments] > output.xml

Then move output.xml into src/javascript and open
src/javascript/index.shtml in Firefox (chrome will only work if it is
opened with a command line option to permit access of local files).

If you are connected to the internet (this is necessary), it will
produce animated charts and tables of results.

-------------------------------

Examples (all called from the directory src/cplusplus/:

Protein identification:

./npci-protein-id ../../data/sigma/sigma49.pivdo ../../data/sigma/target_is_human_decoy_is_reversed.txt ../../data/sigma/1_1.rlists > ../javascript/output.xml 



Differential protein quantification with TMT:

./npci-tmt ../../data/hela-quant-tmt/spectrum_report_no_missing_no_cRAP.tsv "Quant 6 Raw" "Quant 1 Raw" "Quant 2 Raw" true > ../javascript/output.xml 

